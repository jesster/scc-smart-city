within ;
class Version_1_1_0 "Version 1.1.0"
  extends Modelica.Icons.ReleaseNotes;
    annotation (Documentation(info="<html>
<p>Version 1.1.0 is fully compatible with version 1.0.0. It updates the case studies and the tutorial models used to show the implementation of those models. </p>
<p>The following <b><span style=\"color: #0000ff;\">existing components</span></b> have been <b><span style=\"color: #0000ff;\">improved</span></b> in a <b><span style=\"color: #0000ff;\">backward compatible</span></b> way: </p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\"><tr>
<td colspan=\"2\"><p><b>MultiInfrastructure.CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses</b> </p></td>
</tr>
<tr>
<td valign=\"top\"><p>MultiInfrastructure.CoupledSystem.EnergyAndTransportationDelay.BaseClasses.RoadVariableDelay </p></td>
<td valign=\"top\"><p>Changed connection of the output of <span style=\"font-family: monospace;\">qOut to int2. Also changed the connection between varDel and output qOut. </span></p></td>
</tr>
</table>
<p><br>The following <b><span style=\"color: #0000ff;\">existing components</span></b> have been <b><span style=\"color: #0000ff;\">improved</span></b> in a <b><span style=\"color: #0000ff;\">non-backward compatible</span></b> way: </p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\"><tr>
<td colspan=\"2\"><p><b>MultiInfrastructure.CaseStudy</b> </p></td>
</tr>
<tr>
<td valign=\"top\"><p>MultiInfrastructure.CaseStudy.CaseIEnergySystem </p></td>
<td valign=\"top\"><p>Renamed the package to <span style=\"font-family: monospace;\">MultiInfrastructure.CaseStudy.EnergySystem</span>. </p></td>
</tr>
<tr>
<td valign=\"top\"><p>MultiInfrastructure.CaseStudy.CaseIIEnergyTransportation </p></td>
<td valign=\"top\"><p>Renamed the package to <span style=\"font-family: monospace;\">MultiInfrastructure.CaseStudy.EnergyTransportationSystem</span>. </p></td>
</tr>
<tr>
<td valign=\"top\"><p>MultiInfrastructure.CaseStudy.CaseIIIThreeCoupled </p></td>
<td valign=\"top\"><p>Renamed the package to <span style=\"font-family: monospace;\">MultiInfrastructure.CaseStudy.EnergyTransportationCommunicationSystem</span>. </p></td>
</tr>
</table>
<p><br>The following <b><span style=\"color: #0000ff;\">new models</span></b> have been added to <b><span style=\"color: #0000ff;\">existing</span></b> libraries: </p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\"><tr>
<td colspan=\"2\"><p><b>MultiInfrastructure.CaseStudy</b> </p></td>
</tr>
<tr>
<td valign=\"top\"><p>MultiInfrastructure.CaseStudy.EnergySystem.Tutorial </p></td>
<td valign=\"top\"><p>Added tutorial to showcase <span style=\"font-family: monospace;\">MultiInfrastructure.CaseStudy.EnergySystem</span>. </p></td>
</tr>
<tr>
<td valign=\"top\"><p>MultiInfrastructure.CaseStudy.EnergyTransporationSystem.Tutorial </p></td>
<td valign=\"top\"><p>Added tutorial to showcase <span style=\"font-family: monospace;\">MultiInfrastructure.CaseStudy.EnergyTransportationSystem</span>. </p></td>
</tr>
</table>
</html>"));
end Version_1_1_0;
