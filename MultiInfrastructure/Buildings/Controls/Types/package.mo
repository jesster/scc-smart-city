within MultiInfrastructure.Buildings.Controls;
package Types "Package with type definitions"
  constant Integer nDayTypes = 3 "Integer with the number of day types";

annotation (Documentation(info="<html>
This package contains type definitions.
</html>"));
end Types;
