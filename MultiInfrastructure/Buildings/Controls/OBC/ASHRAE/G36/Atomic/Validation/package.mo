within MultiInfrastructure.Buildings.Controls.OBC.ASHRAE.G36.Atomic;
package Validation "Package with validation models"
  extends Modelica.Icons.ExamplesPackage;

 annotation (Documentation(info="<html>
 <p>
 This package contains models that validate the control sequences.
 The examples plot various outputs, which have been verified against
 analytical solutions. These model outputs are stored as reference data to
 allow continuous validation whenever models in the library change.
 </p>
</html>"));
end Validation;
