within MultiInfrastructure.IndividualSystem.Energy.Examples;
model Energy2 "Example that demonstrates the use of Energy.Energy2"
  import MultiInfrastructure;
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";

  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false, filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-50,30})));
  MultiInfrastructure.IndividualSystem.Energy.Energy2 ene(
    lat=weaDat.lat,
    V_nominal=10000,
    PBb_nominal=-1500000,
    PEv_nominal=-1500000,
    PCt_nominal=-70000,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    A=60000,
    num=1,
    betCha=1,
    betDis=0.35,
    thrCha=0.6e5,
    SOC_start=0.5,
    thrDis=-6e5)
             "energy model in the block"
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));
  Modelica.Blocks.Sources.CombiTimeTable nev(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,100; 2,80; 4,60; 6,50; 8,30; 10,8; 12,10; 14,20; 16,30; 18,40;
        20,60; 22,80; 24,100])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-40},{-50,-30}})));
  Modelica.Blocks.Sources.CombiTimeTable ns(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0; 2,0; 4,0; 6,40; 8,100; 10,80; 12,60; 14,40; 16,30; 18,20; 20,
        10; 22,0; 24,0])
    "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-60,-60},{-50,-50}})));
  Modelica.Blocks.Sources.CombiTimeTable pow(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000;
        12,-200000; 14,-300000; 16,-700000; 18,-800000; 20,-1000000; 22,-600000;
        24,-600000]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-20},{-50,-10}})));
equation
  connect(ene.weaBus, weaDat.weaBus) annotation (Line(
      points={{39,9.8},{0,9.8},{0,24},{0,70},{-40,70}},
      color={255,204,51},
      thickness=0.5));
  connect(ene.term_p, gri.terminal) annotation (Line(
      points={{39,0},{-50,0},{-50,20}},
      color={0,120,120},
      thickness=0.5));
  connect(nev.y[1], ene.numEV) annotation (Line(points={{-49.5,-35},{24,-35},{
          24,-5},{38,-5}},
                        color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow.y[1], ene.PBui) annotation (Line(points={{-49.5,-15},{18,-15},{18,
          -3},{38,-3}},
                    color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ns.y[1], ene.numSenPac[1]) annotation (Line(
      points={{-49.5,-55},{30,-55},{30,-7.2},{38,-7.2}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (
    Documentation(info="<html>
    <p>This example shows how to use an energy model in a residential block. If the
    supply renewable energy is insufficient, the grid will provide the power to the system.</p>
</html>"),
      experiment(
      StartTime=0,
      StopTime=86400,
      Tolerance=1e-06),
    __Dymola_Commands(file=
          "Resources/Scripts/IndividualSystem/Energy/Examples/Energy2.mos"
        "Simulate and Plot", file=
          "Resources/Scripts/IndividualSystem/Energy/Examples/EnergyStorage2.mos"
        "Simulate and Plot (Energy storage)"));
end Energy2;
