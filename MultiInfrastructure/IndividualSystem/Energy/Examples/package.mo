within MultiInfrastructure.IndividualSystem.Energy;
package Examples "Collection of models that demonstrate the use of models in MultiInfrastructure.IndividualSystem.Energy"

  extends Modelica.Icons.ExamplesPackage;


  annotation (Documentation(info="<html>
<p>This package contains examples for the use of models in the energy package.</p>
</html>"));
end Examples;
