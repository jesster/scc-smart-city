within MultiInfrastructure.IndividualSystem.Energy.Examples;
model Control2
  import MultiInfrastructure;
  extends Modelica.Icons.Example;
  Modelica.Blocks.Sources.Sine SOC(
    freqHz=1/36000,
    offset=0,
    amplitude=1.2)
              "SOC"
    annotation (Placement(transformation(extent={{-40,-50},{-20,-30}})));
  Modelica.Blocks.Sources.Pulse Psup(
    period=15000,
    amplitude=200,
    offset=-50) "Power supply"
    annotation (Placement(transformation(extent={{-40,30},{-20,50}})));
  MultiInfrastructure.IndividualSystem.Energy.BaseClasses.Control2 batCon(
    tWai=10,
    deaBan=0.01,
    thrDis=-100,
    thrCha=80)
    annotation (Placement(transformation(extent={{20,-12},{40,8}})));
  Modelica.Blocks.Sources.Pulse Pdem(
    amplitude=200,
    width=40,
    period=25000,
    offset=-200) "Power demand"
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
equation
  connect(Psup.y, batCon.Psup)
    annotation (Line(points={{-19,40},{0,40},{0,3},{18,3}}, color={0,0,127}));
  connect(Pdem.y, batCon.Pdem)
    annotation (Line(points={{-19,0},{0,0},{0,-2},{18,-2}},
                                              color={0,0,127}));
  connect(SOC.y, batCon.SOC) annotation (Line(points={{-19,-40},{0,-40},{0,-7},
          {18,-7}},color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file=
          "Resources/Scripts/IndividualSystem/Energy/Examples/Control2.mos"
        "Simulate and Plot"));
end Control2;
