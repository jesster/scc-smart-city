within MultiInfrastructure.IndividualSystem.Energy.Examples;
model Control1
  import MultiInfrastructure;
  extends Modelica.Icons.Example;
  MultiInfrastructure.IndividualSystem.Energy.BaseClasses.Control1 batCon(tWai=
        10, threshold=-100,
    deaBan=0.01)
    annotation (Placement(transformation(extent={{12,-10},{32,10}})));
  Modelica.Blocks.Sources.Sine SOC(
    freqHz=1/36000,
    amplitude=1.5,
    offset=0) "SOC"
    annotation (Placement(transformation(extent={{-28,-40},{-8,-20}})));
  Modelica.Blocks.Sources.Pulse P(
    amplitude=500,
    offset=-200,
    period=15000) "Power deviation"
    annotation (Placement(transformation(extent={{-28,12},{-8,32}})));
equation
  connect(P.y, batCon.P)
    annotation (Line(points={{-7,22},{0,22},{0,4},{10,4}}, color={0,0,127}));
  connect(SOC.y, batCon.SOC) annotation (Line(points={{-7,-30},{0,-30},{0,-4},{
          10,-4}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Control1;
