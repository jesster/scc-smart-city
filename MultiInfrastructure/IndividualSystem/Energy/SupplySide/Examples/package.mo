within MultiInfrastructure.IndividualSystem.Energy.SupplySide;
package Examples "Collection of models that demonstrate the use of models in MultiInfrastructure.IndividualSystem.Energy.Supply"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>This package contains examples for the use of models in the energy supply side.</p>
</html>"));
end Examples;
