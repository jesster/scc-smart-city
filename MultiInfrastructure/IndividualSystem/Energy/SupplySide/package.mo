within MultiInfrastructure.IndividualSystem.Energy;
package SupplySide "Packages that contain supply side components"
annotation (Documentation(info="<html>
<p>This package contains models for the energy supply side in the block.</p>
</html>"));
end SupplySide;
