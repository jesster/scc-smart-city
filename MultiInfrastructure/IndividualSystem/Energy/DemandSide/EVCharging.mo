within MultiInfrastructure.IndividualSystem.Energy.DemandSide;
model EVCharging
  "Model of EV charging power demand"
  parameter Modelica.SIunits.Power PEv_nominal = -1500000
  "Nominal power of EV charging (negative if consumed, positive if generated)";
  parameter Modelica.SIunits.Power PChar = -5000
  "Charging power per EV (negative if consumed, positive if generated)";
  parameter Real ncharg = PEv_nominal/PChar "Maximum charging spaces";

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_n term_p
    annotation (Placement(transformation(extent={{-100,50},{-120,70}}),
        iconTransformation(extent={{-110,50},{-90,70}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive loaEV(
    final mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_P_input,V_nominal(start=10000))
    "Electrical load for the EV charging"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}})));
  Modelica.Blocks.Interfaces.RealInput numEV
    "Number of EV charging in the block"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Sources.RealExpression PEV(y=numEV*PChar)
    "Power consumed by the EV charging"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  Modelica.Blocks.Interfaces.RealOutput P(
    quantity="Power",
    unit="W",
    max=0) "Value of Real output"
    annotation (Placement(transformation(extent={{100,-50},{120,-30}})));
equation
  connect(loaEV.terminal, term_p)
    annotation (Line(points={{10,0},{10,0},{60,0},{60,60},{-110,60}},
                                                color={0,120,120},
      thickness=0.5));
  connect(PEV.y,loaEV. Pow)
    annotation (Line(points={{-59,0},{-59,0},{-10,0}},color={0,0,127}));
  connect(PEV.y, P) annotation (Line(points={{-59,0},{-40,0},{-40,-28},{-40,-40},
          {110,-40}},
                 color={0,0,127}));
  connect(term_p, term_p)
    annotation (Line(points={{-110,60},{-110,60}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-92,92},{94,-94}},
          pattern=LinePattern.None,
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-38,62},{28,-58}},
          pattern=LinePattern.None,
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=3),
        Rectangle(
          extent={{-50,-54},{40,-62}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Line(points={{-126,44}}, color={0,0,0}),
        Rectangle(
          extent={{20,26},{38,22}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Rectangle(
          extent={{36,26},{40,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=4),
        Rectangle(
          extent={{36,-32},{54,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=4),
        Rectangle(
          extent={{50,4},{54,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=5),
        Ellipse(
          extent={{44,12},{60,-6}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{44,20},{60,6}},
          pattern=LinePattern.None,
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-26,50},{16,18}},
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          radius=2),
        Rectangle(
          extent={{48,14},{50,0}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Rectangle(
          extent={{54,14},{56,0}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name"),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0})}),
                                       Diagram(coordinateSystem(
          preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the power demand from the EV charging in the block, which is calculated using the number of charging EVs multiplied by the charging power load per EV.</p>
</html>"));
end EVCharging;
