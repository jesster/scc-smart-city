within MultiInfrastructure.IndividualSystem.Energy.DemandSide;
package Examples "Collection of models that demonstrate the use of models in MultiInfrastructure.IndividualSystem.Energy.Demand"
  extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>This package contains examples for the use of models in the energy demand side.</p>
</html>"));
end Examples;
