within MultiInfrastructure.IndividualSystem.Energy.DemandSide.Examples;
model DistributionSystemIEEE16
  "This example tests the model of the distribution system (IEEE16)."
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal=13600
    "Nominal grid voltage";
  parameter Modelica.SIunits.Power PLoa_nominal=350000
    "Nominal power of a load";
  parameter Modelica.SIunits.Frequency f(start=60)=60
    "Frequency of the source";
  MultiInfrastructure.IndividualSystem.Energy.DemandSide.DistributionSystemIEEE16 disSys(V_nominal=
       13600, PLoa_nominal=350000)
   "Model of a distribution system"
    annotation (Placement(transformation(extent={{0,-12},{30,12}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid
    gri(V=V_nominal, phiSou=0,
    f=f) "Model of a power grid"
    annotation (Placement(transformation(extent={{-60,20},{-40,40}})));
equation
  connect(gri.terminal, disSys.ter1) annotation (Line(points={{-50,20},{-50,10},
          {-0.9375,10}}, color={0,120,120}));
  connect(gri.terminal, disSys.ter2) annotation (Line(points={{-50,20},{-50,0},{
          -0.9375,0}}, color={0,120,120}));
  connect(gri.terminal, disSys.ter3) annotation (Line(points={{-50,20},{-50,-10},
          {-0.9375,-10}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This example tests the distribution system model with IEEE16 topology.</p>
</html>"),experiment(StartTime=0, StopTime=86400),
    __Dymola_Commands(file=
          "Resources/Scripts/IndividualSystem/Energy/DemandSide/Examples/DistributionSystemIEEE16.mos"
        "Simulate and plot"));
end DistributionSystemIEEE16;
