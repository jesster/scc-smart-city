within MultiInfrastructure.IndividualSystem.Energy.DemandSide.Examples;
model DistributionSystem
  "Examples that demonstrate the demand side models"
  import MultiInfrastructure;
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";

  Modelica.Blocks.Sources.CombiTimeTable nev(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,100; 2,80; 4,60; 6,50; 8,30; 10,8; 12,10; 14,20; 16,30; 18,40; 20,
        60; 22,80; 24,100])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-40},{-50,-30}})));
  Modelica.Blocks.Sources.CombiTimeTable ns(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,230; 2,184; 4,138; 6,115; 8,69; 10,20; 12,23; 14,46; 16,69; 18,92;
        20,138; 22,184; 24,230])
    "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-60,-56},{-50,-46}})));
  Modelica.Blocks.Sources.CombiTimeTable pow1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-37800; 2,-27300; 4,-21600; 6,-22300; 8,-22200; 10,-15300; 12,-7500;
        14,-15300; 16,-37800; 18,-52200; 20,-67840; 22,-52300; 24,-37900])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-24},{-50,-14}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid
    gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
  MultiInfrastructure.IndividualSystem.Energy.DemandSide.DistributionSystem
    disSys(
    num=1,
    n=12,
    V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{10,18},{42,42}})));
  Modelica.Blocks.Sources.CombiTimeTable pow2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-32513; 2,-34300; 4,-25720; 6,-34138; 8,-30238; 10,-17400; 12,-11063;
        14,-17400; 16,-49413; 18,-58188; 20,-76063; 22,-37388; 24,-37713])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,-8},{-50,2}})));
  Modelica.Blocks.Sources.CombiTimeTable pow3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-66000; 2,-41800; 4,-44000; 6,-46200; 8,-39600; 10,-38830; 12,-26730;
        14,-34100; 16,-66550; 18,-81400; 20,-88000; 22,-84040; 24,-66220])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,8},{-50,18}})));
  Modelica.Blocks.Sources.CombiTimeTable pow4(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-84720; 2,-74880; 4,-70080; 6,-55920; 8,-76320; 10,-45120; 12,-40320;
        14,-54720; 16,-102720; 18,-147120; 20,-167520; 22,-141120; 24,-102720])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-60,22},{-50,32}})));
equation
  connect(nev.y[1],disSys. numEV) annotation (Line(points={{-49.5,-35},{0,-35},
          {0,27.9429},{11.6842,27.9429}},
                             color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ns.y[1],disSys. numSenPac[1]) annotation (Line(points={{-49.5,-51},{2,
          -51},{2,24.6857},{11.6842,24.6857}},
                               color={0,0,127},
      pattern=LinePattern.Dot));
  connect(gri.terminal,disSys. ter1) annotation (Line(points={{-50,60},{-50,
          39.0857},{12.5263,39.0857}},                  color={0,120,120}));
  connect(gri.terminal,disSys. ter2) annotation (Line(points={{-50,60},{-50,
          36.8571},{12.5263,36.8571}},
                     color={0,120,120}));
  connect(gri.terminal,disSys. ter3) annotation (Line(points={{-50,60},{-50,
          34.6286},{12.5263,34.6286}},
                     color={0,120,120}));
  connect(pow1.y[1],disSys. PBui[10]) annotation (Line(
      points={{-49.5,-19},{-2,-19},{-2,32.5429},{11.6842,32.5429}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow1.y[1],disSys. PBui[11]) annotation (Line(
      points={{-49.5,-19},{-2,-19},{-2,32.8286},{11.6842,32.8286}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow1.y[1],disSys. PBui[12]) annotation (Line(
      points={{-49.5,-19},{-2,-19},{-2,33.1143},{11.6842,33.1143}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow1.y[1],disSys. PBui[1]) annotation (Line(
      points={{-49.5,-19},{-2,-19},{-2,29.9714},{11.6842,29.9714}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],disSys. PBui[2]) annotation (Line(
      points={{-49.5,-3},{-2,-3},{-2,30.2571},{11.6842,30.2571}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],disSys. PBui[3]) annotation (Line(
      points={{-49.5,-3},{-2,-3},{-2,30.5429},{11.6842,30.5429}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],disSys. PBui[6]) annotation (Line(
      points={{-49.5,-3},{-2,-3},{-2,31.4},{11.6842,31.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],disSys. PBui[7]) annotation (Line(
      points={{-49.5,-3},{-2,-3},{-2,31.6857},{11.6842,31.6857}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow2.y[1],disSys. PBui[8]) annotation (Line(
      points={{-49.5,-3},{-2,-3},{-2,31.9714},{11.6842,31.9714}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow3.y[1],disSys. PBui[5]) annotation (Line(
      points={{-49.5,13},{-2,13},{-2,31.1143},{11.6842,31.1143}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow4.y[1],disSys. PBui[4]) annotation (Line(
      points={{-49.5,27},{-2,27},{-2,30.8286},{11.6842,30.8286}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(pow4.y[1],disSys. PBui[9]) annotation (Line(
      points={{-49.5,27},{-2,27},{-2,32.2571},{11.6842,32.2571}},
      color={0,0,127},
      pattern=LinePattern.Dot));

  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -60},{100,100}})),                                   Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-60},{100,100}})),
    Documentation(info="<html>
<p>This example illustrates how to use the models from the energy demand side in a residential block. </p>
</html>"),
experiment(
      StartTime=0,
      StopTime=864000,
      Tolerance=1e-06),
    __Dymola_Commands(file="Resources/Scripts/IndividualSystem/Energy/DemandSide/Examples/DistributionSystem.mos"
        "Simulate and Plot"));
end DistributionSystem;
