within MultiInfrastructure.IndividualSystem.Energy.DemandSide;
model Demand
    "Model of power demand"
  parameter Modelica.SIunits.Power PBb_nominal
    "Nominal power of building blocks (negative if consumed, positive if generated)"
    annotation(Dialog(tab="Building",group="Parameters"));
  parameter Modelica.SIunits.Power PEv_nominal=-1500000
    "Nominal power of EV charging (negative if consumed, positive if generated)"
    annotation(Dialog(tab="EV",group="Parameters"));
  parameter Modelica.SIunits.Power PChar=-5000
  "Charging power per EV(negative if consumed, positive if generated)"
    annotation(Dialog(tab="EV",group="Parameters"));
  parameter Modelica.SIunits.Power PCt_nominal=-70000
    "Nominal power of communication towers (negative if consumed, positive if generated)"
    annotation(Dialog(tab="Communication tower",group="Parameters"));
  parameter Modelica.SIunits.Length d=5000
    "Length of the transimission distance"
    annotation(Dialog(tab="Communication tower",group="Parameters"));
  parameter Modelica.SIunits.EnergyDensity ect=2.6*10^(-9)
    "Transmission energy for a package"
    annotation(Dialog(tab="Communication tower",group="Parameters"));
  parameter Modelica.SIunits.Length l1 "Length of the demand line1"
    annotation (Dialog(tab="Line",group="Parameters"));
  parameter Modelica.SIunits.Length l2 "Length of the demand line2"
    annotation (Dialog(tab="Line",group="Parameters"));
  parameter Modelica.SIunits.Length l3 "Length of the demand line3"
    annotation (Dialog(tab="Line",group="Parameters"));
  parameter Modelica.SIunits.Voltage V_nominal=10000 "Nominal line voltage in the demand side";
  parameter Integer num "Number of ports in the communication block"
    annotation (Dialog(tab="Communication tower"));
  replaceable parameter
    MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable
    constrainedby
    MultiInfrastructure.Buildings.Electrical.Transmission.BaseClasses.BaseCable
    "Commercial cables options"
    annotation (
    Evaluate=true,Dialog(
    tab="Line",group="Manual mode",
    enable=mode == MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial),
    choicesAllMatching=true);

  Modelica.Blocks.Interfaces.RealInput numEV "Number of charging EVs"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}}),
        iconTransformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealInput PBui(
    quantity="Power",
    unit="W",
    min=0)
    "Power comsumed by building appliances"
    annotation (Placement(
        transformation(extent={{-140,30},{-100,70}}),
                                                    iconTransformation(extent={{-140,30},
            {-100,70}})));
  Modelica.Blocks.Interfaces.RealOutput PDem(
    quantity="Power",
    unit="W",
    min=0) "real power for the demand side"
    annotation (Placement(transformation(extent={{100,-90},{120,-70}})));

  MultiInfrastructure.IndividualSystem.Energy.DemandSide.EVCharging loaEV(
    PEv_nominal=PEv_nominal,
    PChar=PChar,
    final ncharg=PEv_nominal/PChar) "Electrical load for EV"
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  MultiInfrastructure.IndividualSystem.Energy.DemandSide.CommunicationTower loaComTow(
    PCt_nominal=PCt_nominal,
    d=d,
    ect=ect,
    num=num) "Electrical load for communication towers"
    annotation (Placement(transformation(extent={{0,-60},{20,-40}})));
  Modelica.Blocks.Sources.RealExpression PDemTot(y=loaComTow.P + loaEV.P + PBui -
        lin1.cableTemp.port.Q_flow - lin2.cableTemp.port.Q_flow - lin3.cableTemp.port.Q_flow)
    "Demand power calculation"
    annotation (Placement(transformation(extent={{60,-90},{80,-70}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_n term_p
    annotation (Placement(transformation(extent={{-118,70},{-98,90}}),
        iconTransformation(extent={{-118,70},{-98,90}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    P_nominal=PEv_nominal,
    l=l2,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    V_nominal=V_nominal,
    redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable)
          "Line to EV charging"
    annotation (Placement(transformation(extent={{-40,-4},{-20,16}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin3(
    P_nominal=PCt_nominal,
    l=l3,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    V_nominal=V_nominal,
    redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable)
          "Line to EV charging"
    annotation (Placement(transformation(extent={{-40,-54},{-20,-34}})));

  Modelica.Blocks.Sources.RealExpression PLinTot(y=-lin1.cableTemp.port.Q_flow
         - lin2.cableTemp.port.Q_flow - lin3.cableTemp.port.Q_flow)
    "Demand power calculation"
    annotation (Placement(transformation(extent={{60,-70},{80,-50}})));
  Modelica.Blocks.Interfaces.RealOutput PLin(
    quantity="Power",
    unit="W",
    min=0) "real power for the demand side"
    annotation (Placement(transformation(extent={{100,-70},{120,-50}})));
  BuildBlock loaBui(PBb_nominal=PBb_nominal) "Electrical load for buildings"
    annotation (Placement(transformation(extent={{0,40},{20,60}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    P_nominal=PBb_nominal,
    l=l1,
    V_nominal=V_nominal,
    mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable)
          "Line to Building"
    annotation (Placement(transformation(extent={{-40,46},{-20,66}})));

  Modelica.Blocks.Interfaces.RealInput numSenPac[num] "Number of packages sent"
    annotation (Placement(transformation(extent={{-140,-70},{-100,-30}})));
equation
  connect(numEV, loaEV.numEV)
    annotation (Line(points={{-120,0},{-120,0},{-2,0}},
                                                     color={0,0,127}));
  connect(PDemTot.y, PDem)
    annotation (Line(points={{81,-80},{81,-80},{110,-80}}, color={0,0,127}));
  connect(lin1.terminal_n, term_p) annotation (Line(points={{-40,56},{-60,56},{
          -60,80},{-108,80}},
                          color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_p, loaEV.term_p)
    annotation (Line(points={{-20,6},{0,6}},           color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_n, term_p) annotation (Line(points={{-40,6},{-60,6},{
          -60,80},{-108,80}},
                          color={0,120,120},
      thickness=0.5));
  connect(lin3.terminal_p, loaComTow.term_p) annotation (Line(points={{-20,-44},
          {-20,-44},{-3,-44}},  color={0,120,120},
      thickness=0.5));
  connect(lin3.terminal_n, term_p) annotation (Line(points={{-40,-44},{-60,-44},
          {-60,80},{-108,80}}, color={0,120,120},
      thickness=0.5));
  connect(PLinTot.y, PLin)
    annotation (Line(points={{81,-60},{92,-60},{110,-60}},  color={0,0,127}));
  connect(lin1.terminal_p, loaBui.term_p)
    annotation (Line(points={{-20,56},{-1,56},{-1,55.2}}, color={0,120,120},
      thickness=0.5));
  connect(PBui, loaBui.PBui)
    annotation (Line(points={{-120,50},{-62,50},{-2,50}},
                                                 color={0,0,127}));
  connect(loaComTow.numSenPac, numSenPac)
    annotation (Line(points={{-2,-50},{-120,-50}}, color={0,0,127}));
    annotation (Dialog(tab="Line",group="Parameters"),
                Dialog(tab="Lines"),
    Icon(coordinateSystem(preserveAspectRatio=false), graphics={
      Rectangle(
        extent={{-48,58},{16,-2}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Polygon(
        points={{-16,82},{-58,58},{26,58},{-16,82}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
      Rectangle(
        extent={{-36,34},{-22,48}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-10,34},{4,48}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-36,12},{-22,26}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-10,12},{4,26}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{10,24},{58,-60}},
          pattern=LinePattern.None,
          lineColor={0,0,0},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          radius=3),
        Rectangle(
          extent={{64,-58},{4,-64}},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{58,10},{68,6}},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{66,10},{70,-44}},
          pattern=LinePattern.None,
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          radius=4,
          lineColor={0,0,0}),
        Rectangle(
          extent={{66,-40},{80,-44}},
          pattern=LinePattern.None,
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          radius=4,
          lineColor={0,0,0}),
        Rectangle(
          extent={{76,-14},{80,-44}},
          pattern=LinePattern.None,
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          radius=5,
          lineColor={0,0,0}),
        Ellipse(
          extent={{72,-8},{84,-24}},
          pattern=LinePattern.None,
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{72,-4},{84,-14}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{16,16},{52,-8}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          radius=2,
          lineColor={0,0,0}),
        Rectangle(
          extent={{74,-8},{76,-14}},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{80,-8},{82,-14}},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Ellipse(
          extent={{-72.5,8.5},{-0.5,-63.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-69.5,5.5},{-3.5,-60.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-57.5,-6.5},{-15.5,-48.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-54.5,-9.5},{-18.5,-45.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-48.5,-15.5},{-24.5,-39.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-38,-26},{-62,-88},{-60,-88},{-38,-30},{-38,-26}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-44,-42},{-30,-44}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-48,-52},{-26,-54}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-54,-68},{-20,-70}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-41.5,-22.5},{-32,-32}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{-36,-26},{-12,-88},{-14,-88},{-36,-30},{-36,-26}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the total power load in the blocks from the three components : building blocks. EV charging stations, and communication towers. </p>
<p><img src=\"modelica://MultiInfrastructure/Resources/Images/IndividualSystem/Energy/DemandSide/Demand.png\"/></p>
</html>"),
    __Dymola_Commands);
end Demand;
