within MultiInfrastructure.IndividualSystem.Energy;
package DemandSide "Packages that contain demand side models"


annotation (Documentation(info="<html>
<p>This package contains models for the energy demand side in the block.</p>
</html>"));
end DemandSide;
