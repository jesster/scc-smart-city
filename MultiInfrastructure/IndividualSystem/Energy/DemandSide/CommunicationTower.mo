within MultiInfrastructure.IndividualSystem.Energy.DemandSide;
model CommunicationTower
  "Model of power demand from communication tower"
  parameter Modelica.SIunits.Power PCt_nominal = 65000
  "Nominal power of Communication Tower(negative if consumed, positive if generated)";
  parameter Modelica.SIunits.Length d=5000 "Length of the transimission distance";
  parameter Modelica.SIunits.EnergyDensity ect=2.6*10^(-9) "Transmission energy for a package";
  parameter Integer num(min=1) "Number of ports in the communication block";
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive loaComTow(
     mode=MultiInfrastructure.Buildings.Electrical.Types.Load.VariableZ_P_input,V_nominal(start=10000))
    "Electrical load for the communication tower"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}})));

  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_n term_p
    annotation (Placement(transformation(extent={{-100,50},{-120,70}}),
        iconTransformation(extent={{-140,50},{-120,70}})));
  Modelica.Blocks.Interfaces.RealInput numSenPac[num] "Number of packages sent"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Sources.RealExpression PComTow(y=-sum.y*ect*(d^3))
    "Power consumed by the communication tower"
    annotation (Placement(transformation(extent={{-52,-10},{-32,10}})));
  Modelica.Blocks.Interfaces.RealOutput P "Value of Real output"
    annotation (Placement(transformation(extent={{100,-50},{120,-30}})));
  Modelica.Blocks.Math.Sum sum(nin=num)
    annotation (Placement(transformation(extent={{-86,-10},{-66,10}})));
equation
  connect(loaComTow.terminal, term_p)
    annotation (Line(points={{10,0},{10,0},{60,0},{60,60},{-110,60}},
                                                color={0,120,120},
      thickness=0.5));
  connect(PComTow.y, loaComTow.Pow)
    annotation (Line(points={{-31,0},{-10,0}},color={0,0,127}));
  connect(PComTow.y, P) annotation (Line(points={{-31,0},{-20,0},{-20,-40},{110,
          -40}},      color={0,0,127}));
  connect(numSenPac, sum.u)
    annotation (Line(points={{-120,0},{-104,0},{-88,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-57,87},{60,-30}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-53,83},{56,-26}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-34.5,64.5},{37.5,-7.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-31.5,61.5},{34.5,-4.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-19.5,49.5},{22.5,7.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-16.5,46.5},{19.5,10.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-10.5,40.5},{13.5,16.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-7.5,37.5},{10.5,19.5}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{6,26},{50,-92},{42,-92},{0,26},{6,26}},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Rectangle(
          extent={{-24,-32},{26,-36}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-14,-8},{18,-12}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-2,26},{-46,-92},{-38,-92},{4,26},{-2,26}},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Rectangle(
          extent={{-30,-58},{34,-62}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name"),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0})}),
                                            Diagram(coordinateSystem(
          preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the power load from the communication towers in the block, whose input comes from the number of the sent packets. </p>
</html>"));
end CommunicationTower;
