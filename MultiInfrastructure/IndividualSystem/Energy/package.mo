within MultiInfrastructure.IndividualSystem;
package Energy "Collection of models that describes the energy system"





annotation (Documentation(info="<html>
<p>
This package contains models for energy systems.
</p>
</html>"));
end Energy;
