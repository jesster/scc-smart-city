within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data;
record CClassRoad30
  extends
    MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.Generic(
  a1=1.5,
  a2=1.88,
  a3=7,
  uf=8.33,
  q_nominal=700);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end CClassRoad30;
