within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses;
package Data "Package with traffic data for different road types"
  extends Modelica.Icons.MaterialPropertiesPackage;

annotation (Documentation(info="<html>
<p>
  This record declares the data used to specify different road types.
</p>

</html>"));
end Data;
