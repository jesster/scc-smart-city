within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data;
record BClassRoad80
  extends
    MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.Generic(
  a1=0.95,
  a2=1.88,
  a3=6.97,
  uf=22.22,
  q_nominal=1400);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end BClassRoad80;
