within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data;
record Generic
  extends Modelica.Icons.Record;
  parameter Modelica.SIunits.Velocity uf "Traffic speed at the free flow";
  parameter
    MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Types.TrafficFlow
    q_nominal "Nominal capacity at the road";
  parameter Real a1 "Regression coefficient of the road velocity function";
  parameter Real a2 "Regression coefficient of the road velocity function";
  parameter Real a3 "Regression coefficient of the road velocity function";
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Generic;
