within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data;
record Expressway60
  extends
    MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.Generic(
  a1=1.2,
  a2=1.88,
  a3=4.88,
  uf=16.67,
  q_nominal=1800);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Expressway60;
