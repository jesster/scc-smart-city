within MultiInfrastructure.IndividualSystem.Transportation;
package TrafficTheory
annotation (Documentation(info="<html>
<p>In this package, the transportation system is modeled using basic traffic theory (the relationship between the road velocity, flow rate, and density).</p>
</html>"));
end TrafficTheory;
