within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.Examples;
model RoadFixedDelay
    "Example that tests the Transporation.TrafficTheory.RoadFixedDelay"
  extends Modelica.Icons.Example;
  MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.RoadFixedDelay
    roaFixDel annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Sources.CombiTimeTable qIn(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,
        30; 12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,
        10; 22,5; 23,0; 24,5]) "Traffic flow output for a certain block"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
equation
  connect(qIn.y[1], roaFixDel.qIn)
    annotation (Line(points={{-59,0},{-12,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This example shows the use of RoadFixedDelay model.</p>
</html>"),
    __Dymola_Commands(file=
          "Resources/Scripts/IndividualSystem/Transportation/TrafficTheory/Examples/RoadFixedDelay.mos"
        "Simulate and Plot"));
end RoadFixedDelay;
