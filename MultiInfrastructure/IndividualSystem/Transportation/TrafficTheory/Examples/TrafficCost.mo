within MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.Examples;
model TrafficCost
    "Example that tests the Transporation.TrafficTheory.TrafficCost"
  extends Modelica.Icons.Example;
  MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.TrafficCost
    traCos(        redeclare BaseClasses.Data.DClassRoad40 roaTyp, l=1000)
              annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Sources.CombiTimeTable qIn(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,25; 1,35; 2,10; 3,15; 4,40; 5,50; 6,400; 7,500; 8,600; 9,400; 10,
        250; 11,150; 12,100; 13,100; 14,150; 15,150; 16,100; 17,150; 18,150; 19,
        100; 20,150; 21,50; 22,25; 23,0; 24,25])
    "Number of traffic inflow for a block"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
equation
  connect(qIn.y[1], traCos.qIn)
    annotation (Line(points={{-59,0},{-12,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This example shows the use of TrafficCost model.</p>
</html>"),
    __Dymola_Commands(file=
          "Resources/Scripts/IndividualSystem/Transportation/TrafficTheory/Examples/TrafficCost.mos"
        "Simulate and Plot"));
end TrafficCost;
