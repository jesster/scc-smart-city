within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory;
package Examples "Examples that demonstrate the use of models in FlowTheory"
  extends Modelica.Icons.ExamplesPackage;





annotation (Documentation(info="<html>
<p>Examples to demonstrate the usage of transportation  models based on flow theory.</p>
</html>"));
end Examples;
