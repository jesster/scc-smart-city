within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Examples;
model FourPortNetwork
  "Examples that demonstrate the Transporation.FlowTheory.FourPortBlock"
  extends Modelica.Icons.Example;

  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.FourPortBlock com1(
    qb1_nominal=600,
    qb2_nominal=600,
    Ub1_nominal=800,
    Ub2_nominal=800,
    numEV(start=100, fixed=true)) "Commercial Block 1"
    annotation (Placement(transformation(extent={{40,40},{60,60}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.FourPortBlock com2(
    qb1_nominal=600,
    qb2_nominal=600,
    Ub1_nominal=800,
    Ub2_nominal=800,
    numEV(fixed=true, start=150)) "Commercial Block 2"
    annotation (Placement(transformation(extent={{40,-60},{60,-40}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa1(q_nominal=
       1200, U_nominal=1000)
    annotation (Placement(transformation(extent={{-10,46},{10,66}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa2(q_nominal=
       600, U_nominal=800)
    annotation (Placement(transformation(extent={{10,54},{-10,34}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa6(q_nominal=
       600, U_nominal=800) annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={80,0})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa5(q_nominal=
       600, U_nominal=800) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={70,0})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa3(q_nominal=
       1200, U_nominal=1000) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-80,0})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa4(q_nominal=
       1200, U_nominal=1000) annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-66,0})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa7(q_nominal=
       1200, U_nominal=1000)
    annotation (Placement(transformation(extent={{-10,-54},{10,-34}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road roa8(q_nominal=
       600, U_nominal=800)
    annotation (Placement(transformation(extent={{10,-46},{-10,-66}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.FourPortBlock res1(
    qb1_nominal=1200,
    qb2_nominal=1200,
    Ub1_nominal=1000,
    Ub2_nominal=1000,
    numEV(start=550, fixed=true)) "Residential Block1"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Sources.CombiTimeTable resBlo1(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,7; 2,5; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,
        30; 12,20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,
        10; 22,5; 23,0; 24,0])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-110,80},{-100,90}})));
  Modelica.Blocks.Sources.CombiTimeTable comBlo3(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,43;
        22,10; 23,15; 24,0])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{-110,-70},{-100,-60}})));
  Modelica.Blocks.Sources.CombiTimeTable resBlo2(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,8; 2,4; 3,5; 4,6; 5,12; 6,75; 7,108; 8,116; 9,75; 10,38; 11,
        45; 12,23; 13,26; 14,38; 15,40; 16,15; 17,25; 18,35; 19,40; 20,42; 21,
        20; 22,10; 23,15; 24,0])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-110,60},{-100,70}})));
  Modelica.Blocks.Sources.CombiTimeTable comBlo4(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30;
        12,35; 13,25; 14,30; 15,50; 16,76; 17,80; 18,120; 19,90; 20,60; 21,40;
        22,27; 23,25; 24,0])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{-110,-90},{-100,-80}})));
  Modelica.Blocks.Sources.CombiTimeTable resBlo3(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,0; 2,4; 3,0; 4,8; 5,10; 6,20; 7,15; 8,30; 9,30; 10,15; 11,30;
        12,33; 13,25; 14,30; 15,25; 16,25; 17,40; 18,33; 19,30; 20,50; 21,35;
        22,15; 23,15; 24,0])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-110,32},{-100,42}})));
  Modelica.Blocks.Sources.CombiTimeTable resBlo4(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,8; 2,4; 3,0; 4,8; 5,10; 6,15; 7,30; 8,35; 9,25; 10,38; 11,45;
        12,23; 13,26; 14,38; 15,40; 16,15; 17,25; 18,33; 19,40; 20,20; 21,20;
        22,10; 23,10; 24,0])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-110,10},{-100,20}})));
  Modelica.Blocks.Sources.CombiTimeTable comBlo1(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,0; 2,4; 3,0; 4,8; 5,10; 6,15; 7,18; 8,30; 9,42; 10,35; 11,30;
        12,21; 13,25; 14,28; 15,24; 16,26; 17,34; 18,41; 19,30; 20,50; 21,35;
        22,15; 23,15; 24,0])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{-110,-20},{-100,-10}})));
  Modelica.Blocks.Sources.CombiTimeTable comBlo2(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0; 1,9; 2,5; 3,1; 4,9; 5,11; 6,16; 7,21; 8,25; 9,26; 10,39; 11,30;
        12,33; 13,25; 14,30; 15,41; 16,24; 17,26; 18,37; 19,41; 20,21; 21,21;
        22,20; 23,25; 24,0])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{-110,-40},{-100,-30}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.FourPortBlock res2(
    qb1_nominal=1200,
    qb2_nominal=1200,
    Ub1_nominal=1000,
    Ub2_nominal=1000,
    numEV(fixed=true, start=600)) "Residential Block 2"
    annotation (Placement(transformation(extent={{-60,-60},{-40,-40}})));
equation
  connect(res1.port_b1, roa1.port_a) annotation (Line(
      points={{-40,56},{-24,56},{-10,56}},
      color={0,127,255},
      thickness=1));
  connect(roa1.port_b, com1.port_a1) annotation (Line(
      points={{10,56},{26,56},{40,56}},
      color={0,127,255},
      thickness=1));
  connect(com1.port_b2, roa2.port_a) annotation (Line(
      points={{40,44},{26,44},{10,44}},
      color={0,127,255},
      thickness=1));
  connect(res1.port_a2, roa2.port_b) annotation (Line(
      points={{-40,44},{-10,44}},
      color={0,127,255},
      thickness=1));
  connect(res2.port_b1, roa7.port_a) annotation (Line(
      points={{-40,-44},{-25,-44},{-10,-44}},
      color={0,127,255},
      thickness=1));
  connect(roa7.port_b, com2.port_a1) annotation (Line(
      points={{10,-44},{18,-44},{40,-44}},
      color={0,127,255},
      thickness=1));
  connect(res2.port_a2, roa8.port_b) annotation (Line(
      points={{-40,-56},{-26,-56},{-10,-56}},
      color={0,127,255},
      thickness=1));
  connect(roa8.port_a, com2.port_b2) annotation (Line(
      points={{10,-56},{26,-56},{40,-56}},
      color={0,127,255},
      thickness=1));
  connect(res1.port_a1, roa3.port_b) annotation (Line(
      points={{-60,56},{-70,56},{-80,56},{-80,10}},
      color={0,127,255},
      thickness=1));
  connect(roa3.port_a, res2.port_b2) annotation (Line(
      points={{-80,-10},{-80,-10},{-80,-56},{-60,-56}},
      color={0,127,255},
      thickness=1));
  connect(res1.port_b2, roa4.port_a) annotation (Line(
      points={{-60,44},{-64,44},{-66,44},{-66,10}},
      color={0,127,255},
      thickness=1));
  connect(roa4.port_b, res2.port_a1) annotation (Line(
      points={{-66,-10},{-66,-10},{-66,-44},{-60,-44}},
      color={0,127,255},
      thickness=1));
  connect(com1.port_a2, roa5.port_b) annotation (Line(
      points={{60,44},{70,44},{70,10}},
      color={0,127,255},
      thickness=1));
  connect(roa5.port_a, com2.port_b1) annotation (Line(
      points={{70,-10},{70,-44},{60,-44}},
      color={0,127,255},
      thickness=1));
  connect(com1.port_b1, roa6.port_a) annotation (Line(
      points={{60,56},{80,56},{80,10}},
      color={0,127,255},
      thickness=1));
  connect(roa6.port_b, com2.port_a2) annotation (Line(
      points={{80,-10},{80,-10},{80,-56},{60,-56}},
      color={0,127,255},
      thickness=1));
  connect(resBlo1.y[1], res1.qb1) annotation (Line(
      points={{-99.5,85},{-82,85},{-82,53},{-62,53}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo3.y[1], com1.qb2) annotation (Line(
      points={{-99.5,-65},{20,-65},{20,47},{38,47}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo4.y[1], com2.qb2) annotation (Line(
      points={{-99.5,-85},{20,-85},{20,-53},{38,-53}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo2.y[1], res2.qb1) annotation (Line(
      points={{-99.5,65},{-82,65},{-82,-46},{-72,-46},{-62,-46},{-62,-47}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo3.y[1], res1.qb2) annotation (Line(
      points={{-99.5,37},{-82,37},{-82,47},{-62,47}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo4.y[1], res2.qb2) annotation (Line(
      points={{-99.5,15},{-82,15},{-82,-53},{-62,-53}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo1.y[1], com1.qb1) annotation (Line(
      points={{-99.5,-15},{20,-15},{20,53},{38,53}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo2.y[1], com2.qb1) annotation (Line(
      points={{-99.5,-35},{-30,-35},{-30,-47},{38,-47}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
            -100},{100,100}})),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{100,
            100}})),
    __Dymola_Commands(file=
          "Resources/Scripts/IndividualSystem/Transportation/FlowTheory/Examples/FourPortNetwork.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
<p>This example demonstrates the traffic interaction between the four blocks. </p>
<p>See model <a href=\"modelica://MultiInfrastructure.IndividualSystem.Transportation.Examples.TwoPortNetwork\">MultiInfrastructure.IndividualSystem.Transportation.Examples.TwoPortNetwork</a> for more information. </p>
</html>"));
end FourPortNetwork;
