within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory;
model FourPortBlock "Model of a four-port transportation block"
 extends
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.PartialFourPort;
  parameter
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types.TrafficFlow
    qb1_nominal "Nominal capacity at the road linked with port_b1";
  parameter
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types.TrafficFlow
    qb2_nominal "Nominal capacity at the road linked with port_b2";
  parameter Modelica.SIunits.Time Ub1_nominal
    "Reference travel cost at the free flow road linked with port_b1";
  parameter Modelica.SIunits.Time Ub2_nominal
    "Reference travel cost at the free flow road linked with port_b2";
  Real numEV(start = 300) "Number of charging EV in the block";
  Modelica.Blocks.Interfaces.RealInput qb1(quantity="TrafficFlow", unit="1/h")
    "Traffic outflow at the port_b"
    annotation (Placement(transformation(extent={{-140,10},{-100,50}})));
  Modelica.Blocks.Sources.RealExpression num(y=numEV)
    "number of charging EV in the block"
    annotation (Placement(transformation(extent={{70,-90},{90,-70}})));
  Modelica.Blocks.Interfaces.RealOutput numEVCha "Numer of EVs being charged"
    annotation (Placement(transformation(extent={{100,-90},{120,-70}})));

  Modelica.Blocks.Interfaces.RealInput qb2(
    quantity="TrafficFlow",
    unit="1/h") "Traffic outflow at the port_b2"
    annotation (Placement(transformation(extent={{-140,-50},{-100,-10}})));
equation
  port_b1.q =-qb1;
  port_b2.q =-qb2;
  port_b1.U=0.15*Ub1_nominal*(port_b1.q/qb1_nominal)^5;
  port_b2.U=0.15*Ub2_nominal*(port_b2.q/qb2_nominal)^5;
  der(numEV)= (port_a1.q + port_b1.q + port_a2.q + port_b2.q)/3600;

  connect(num.y, numEVCha)
    annotation (Line(points={{91,-80},{110,-80}}, color={0,0,127}));
  annotation (Icon(graphics={
        Ellipse(
          extent={{-100,100},{100,-100}},
          pattern=LinePattern.None,
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-38,62},{28,-58}},
          pattern=LinePattern.None,
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=3),
        Rectangle(
          extent={{-50,-54},{40,-62}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{20,26},{38,22}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{36,26},{40,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=4,
          lineColor={0,0,0}),
        Rectangle(
          extent={{36,-32},{54,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=4,
          lineColor={0,0,0}),
        Rectangle(
          extent={{50,4},{54,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=5,
          lineColor={0,0,0}),
        Ellipse(
          extent={{44,12},{60,-6}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{44,20},{60,6}},
          pattern=LinePattern.None,
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-26,50},{16,18}},
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          radius=2,
          lineColor={0,0,0}),
        Rectangle(
          extent={{48,14},{50,0}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{54,14},{56,0}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<p>This model shows a four port transporation block. 
The number of the EVs in the block is calculated using the balance of the traffic flow.</p>
<p>The traffic flow calculation uses the basic electric circuit theory. 
See model <a href=\"modelica://MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road\">
MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.Road</a> for more information.</p>

</html>"));
end FourPortBlock;
