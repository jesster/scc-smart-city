within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses;
connector TrafficPort
 "Interface for traffic flow in a road network "
 Modelica.SIunits.Time U "Travel cost at the node";
  flow Types.TrafficFlow q "Vehicles flowing into the port"
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
  annotation (Documentation(info="<html>
<p>TrafficPort is the basic traffic connector. It includes the travel cost which consists between the TrafficPort and the ground node. The ground node is the node of (any) ground device. Furthermore, the TrafficPort includes the traffic flow, which is considered to be positive if it is flowing at the port into the block.</p>
</html>"));
end TrafficPort;
