within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses;
connector TrafficPort_a "Generic traffic connector at design inlet"
  extends TrafficPort;
  annotation (defaultComponentName="port_a",
              Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Ellipse(
          extent={{-40,40},{40,-40}},
          lineColor={0,0,0},
          fillColor={0,127,255},
          fillPattern=FillPattern.Solid), Text(extent={{-150,110},{150,50}},
            textString="%name")}),
       Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{
            100,100}}), graphics={Ellipse(
          extent={{-100,100},{100,-100}},
          lineColor={0,127,255},
          fillColor={0,127,255},
          fillPattern=FillPattern.Solid), Ellipse(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillColor={0,127,255},
          fillPattern=FillPattern.Solid)}),
    Documentation(info="<html>
<p>Connectors TrafficPort_a and TrafficPort_b are nearly identical. The only difference is that the icons are different in order to identify more easily the port of the component. Usually, connector TrafficPort_a  is used for the positive and connector TrafficPort_b for the negative port of a traffic component.</p>
</html>"));
end TrafficPort_a;
