within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory;
package BaseClasses










  annotation (Documentation(info="<html>
  <p>This package contains base classes that are used to construct the models in 
  <a href=\"modelica://MultiInfrastructure.IndividualSystem.Transportation.FlowTheory\">Transportation.FlowTheory</a>  </p>
</html>"));
end BaseClasses;
