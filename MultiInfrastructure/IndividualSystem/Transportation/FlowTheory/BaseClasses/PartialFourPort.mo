within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses;
partial model PartialFourPort
 "Partial component with Four ports"
   import Modelica.Constants;
   parameter Boolean allowFlowReversal = true
    "= false to simplify equations, assuming, but not enforcing, no flow reversal for traffic flow"
    annotation(Dialog(tab="Assumptions"), Evaluate=true);

  TrafficPort_a port_a1( q(min=if allowFlowReversal then -Constants.inf else 0))
    "Traffic connector a1 (positive design flow direction is from port_a1 to port_b1)"
     annotation (Placement(transformation(extent={{-110,50},{-90,70}})));
  TrafficPort_b port_b1( q(max=if allowFlowReversal then +Constants.inf else 0))
    "Traffic connector b1 (positive design flow direction is from port_a1 to port_b1)"
     annotation (Placement(transformation(extent={{110,50},{90,70}})));
  TrafficPort_a port_a2( q(min=if allowFlowReversal then -Constants.inf else 0))
    "Traffic connector a2 (positive design flow direction is from port_a2 to port_b2)"
     annotation (Placement(transformation(extent={{90,-70},{110,-50}})));
  TrafficPort_b port_b2( q(max=if allowFlowReversal then +Constants.inf else 0))
    "Traffic connector b2 (positive design flow direction is from port_a2 to port_b2)"
     annotation (Placement(transformation(extent={{-90,-70},{-110,-50}})));

   annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This partial model defines an interface for components with four ports. </p>
</html>"));
end PartialFourPort;
