within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types;
type TrafficFlow = Real (final quantity = "TrafficFlow", final unit="1/h",min=0.0)
  "Type for traffic flow";
