within MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types;
type TrafficImpedance = Real (final quantity = "TrafficImpedance", final unit="s.h5",min=0.0);
