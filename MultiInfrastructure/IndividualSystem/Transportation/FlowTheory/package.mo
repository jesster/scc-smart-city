within MultiInfrastructure.IndividualSystem.Transportation;
package FlowTheory
annotation (Documentation(info="<html>
<p>In this package, the transportation system mimics the fluid system. The traffic flow resembles the water flow, and the road resembles the pipe flow.</p>
</html>"));
end FlowTheory;
