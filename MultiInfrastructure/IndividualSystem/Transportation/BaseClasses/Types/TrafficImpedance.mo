within MultiInfrastructure.IndividualSystem.Transportation.BaseClasses.Types;
type TrafficImpedance = Real (final quantity = "TrafficImpedance", final unit="s.h5",min=0.0);
