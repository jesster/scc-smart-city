within MultiInfrastructure.IndividualSystem.Transportation.BaseClasses;
partial model PartialTwoPort "Partial component with two ports"
 import Modelica.Constants;

TrafficPort_a port_a(q(min=0))
    "Traffic connector a (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
TrafficPort_b port_b(q(max=0))
    "Traffic connector b (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{110,-10},{90,10}}), iconTransformation(extent={{110,-10},
            {90,10}})));

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This partial model defines an interface for components with two ports. </p>
</html>"));
end PartialTwoPort;
