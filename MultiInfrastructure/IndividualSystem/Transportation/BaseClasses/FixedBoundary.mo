within MultiInfrastructure.IndividualSystem.Transportation.BaseClasses;
model FixedBoundary
  "Ideal source that produces a prescribe traffic flow rate"
  parameter Integer nPorts=0 "Number of ports" annotation(Dialog(connectorSizing=true));
  MultiInfrastructure.IndividualSystem.Transportation.BaseClasses.TrafficPorts_b ports[nPorts]
    annotation (Placement(transformation(extent={{84,-40},{104,40}})));

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillPattern=FillPattern.Sphere,
          fillColor={0,127,255}),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),    Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end FixedBoundary;
