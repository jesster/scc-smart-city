within MultiInfrastructure.IndividualSystem;
package Transportation "Collection of models that describes the transportation system"
annotation (Documentation(info="<html>
<p>
This package contains models for transportation system.
</p>
</html>"));
end Transportation;
