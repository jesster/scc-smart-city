within MultiInfrastructure.IndividualSystem.Communication;
model nPortBlock "Model of a communication block with n ports"

  parameter Integer num(min=1)=1 "Number of ports in the communication block";
  Modelica.Blocks.Interfaces.RealOutput numSenPac[num] "Number of sent packets"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Interfaces.RealInput numRecPac[num]
    "Number of received packets" annotation (Placement(transformation(extent={{-140,
            -30},{-100,10}}), iconTransformation(extent={{-120,-10},{-100,10}})));
equation
   for i in 1:num loop
  numSenPac[i]=numRecPac[i];
   end for
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-57,87},{60,-30}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-53,83},{56,-26}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-34.5,64.5},{37.5,-7.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-31.5,61.5},{34.5,-4.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-19.5,49.5},{22.5,7.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-16.5,46.5},{19.5,10.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-10.5,40.5},{13.5,16.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-7.5,37.5},{10.5,19.5}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{6,26},{50,-92},{42,-92},{0,26},{6,26}},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Rectangle(
          extent={{-24,-32},{26,-36}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-14,-8},{18,-12}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-2,26},{-46,-92},{-38,-92},{4,26},{-2,26}},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Rectangle(
          extent={{-30,-58},{34,-62}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),            Diagram(coordinateSystem(
          preserveAspectRatio=false)),
    Documentation(info="<html>
    <p>This model shows a n-port communication block. Its input is the number of the 
    received packets and its output is the number of the sent packets, 
    which is determined by the traffic outflow. The number of the ports can be set by the users.</p>
</html>"));

end nPortBlock;
