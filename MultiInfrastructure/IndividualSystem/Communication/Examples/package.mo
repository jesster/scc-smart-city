within MultiInfrastructure.IndividualSystem.Communication;
package Examples "Examples that demonstrate the use of models in Communication"
  extends Modelica.Icons.ExamplesPackage;

  model TwoPortBlock
      "Examples that demonstrate the Communication.TwoPortBlock"
    extends Modelica.Icons.Example;

  MultiInfrastructure.IndividualSystem.Communication.nPortBlock res(num=2)
    annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
  MultiInfrastructure.IndividualSystem.Communication.nPortBlock com(num=2)
    annotation (Placement(transformation(extent={{28,-10},{48,10}})));
    Modelica.Blocks.Sources.CombiTimeTable numPac2(
      tableName="tab1",
      fileName="Resources/Inputs/Transportation/nevRes.txt",
      tableOnFile=false,
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
      timeScale=3600,
      table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,30; 12,
          20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,10; 22,5; 23,
          0; 24,5]) "Number of vehicle for a residential building block"
      annotation (Placement(transformation(extent={{-100,40},{-80,60}})));
    Modelica.Blocks.Sources.CombiTimeTable numPac1(
      tableName="tab1",
      fileName="Resources/Inputs/Transportation/nevRes.txt",
      tableOnFile=false,
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
      timeScale=3600,
      table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30; 12,
          20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40; 22,10;
          23,15; 24,5]) "Number of vehicles for a commercial building block"
      annotation (Placement(transformation(extent={{-100,72},{-80,92}})));
  Transmission tra1(
    num=2,
    kc={0.03,0.03},
    cc={105,100})
    annotation (Placement(transformation(extent={{-12,-10},{8,10}})));
  Transmission tra2(
    num=2,
    cc={100,105},
    kc={0.03,0.05})
    annotation (Placement(transformation(extent={{8,-10},{-12,-30}})));
  equation

  connect(numPac2.y[1], com.numRecPac[2]) annotation (Line(points={{-79,50},{20,
          50},{20,0.5},{27,0.5}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(res.numSenPac, tra1.numSenPac)
    annotation (Line(points={{-29,0},{-13,0}}, color={0,0,127}));
  connect(com.numSenPac, tra2.numSenPac) annotation (Line(points={{49,0},{60,0},
          {60,-20},{9,-20}}, color={0,0,127}));
  connect(numPac1.y[1], res.numRecPac[1]) annotation (Line(points={{-79,82},{
          -70,82},{-70,0},{-52,0},{-52,-0.5},{-51,-0.5}},
                                          color={0,0,127},
      pattern=LinePattern.Dot));
  connect(tra1.numRecPac[1], com.numRecPac[1]) annotation (Line(points={{9,-0.5},
          {8,-0.5},{8,0},{26,0},{26,0},{26,0},{26,-0.5},{27,-0.5}}, color={0,0,
          127}));
  connect(tra2.numRecPac[2], res.numRecPac[2]) annotation (Line(points={{-13,
          -20.5},{-66,-20.5},{-66,0},{-52,0},{-52,0},{-52,0},{-52,0.5},{-51,0.5}},
                                                   color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)),
      __Dymola_Commands(file=
          "Resources/Scripts/IndividualSystem/Communication/Examples/TwoPortBlock.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
  <p>This example demonstrates the communication transmission between two blocks.
  The results show that there exists some packets losses when the number of the packet 
  to be sent rises more than a certain threshold.</p>
</html>"));
  end TwoPortBlock;

annotation (Documentation(info="<html>
<p>Examples to demonstrate the usage of communication models.</p>
</html>"));
end Examples;
