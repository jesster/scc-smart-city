within MultiInfrastructure.CaseStudy.EnergyTransportationSystem.BaseClasses;
model EnergyTransportation
  "Model that connects the energy and transportation system"
  parameter Modelica.SIunits.Angle lat "Latitude"
  annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Real numEV "Number of initial EV in the block"
    annotation(Dialog(tab="Transportation",group="Parameters"));
  parameter Modelica.SIunits.Frequency f=60 "Nominal grid frequency"
    annotation(Dialog(tab="Energy",group="Parameters"));
 final parameter Integer n=12 "Number of buildings in the community";
  parameter Modelica.SIunits.Voltage V_nominal=10000
    "Nominal voltage(V_nominal>=0)"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Area A=600000 "Net surface area of PV"
    annotation (Dialog(tab="Energy",group="Renewable Generation"));
  parameter Modelica.SIunits.Voltage VWin_nominal=480
    "Nominal voltage of wind turbine (VWin_nominal >= 0)"
    annotation (Dialog(tab="Energy",group="Renewable Generation"));
  parameter Modelica.SIunits.Power PWin_nominal=-ene.PLoa_nominal
    "Nominal power pf the wind turbine"
    annotation (Dialog(tab="Energy",group="Renewable Generation"));
  parameter Real SOC_start=0.5 "Initial charge"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Modelica.SIunits.Energy EMax=2e10 "Maximum available charge"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Real betDis=0.7 "Discharging velocity coefficient"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Real betCha=0.7 "Charging velocity coefficient"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Modelica.SIunits.Power thrDis=-1.2e6 "Discharging power threshold"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Modelica.SIunits.Power thrCha=-7e5 "Charging power threshold"
    annotation (Dialog(tab="Energy",group="Battery"));
  parameter Integer num=2 "Number of ports for transportation and communication system"
   annotation (Dialog(tab="Transportation"));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
    annotation (Placement(transformation(extent={{-120,52},{-100,72}}),
        iconTransformation(extent={{-120,52},{-100,72}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-120,76},{-100,96}}), iconTransformation(extent=
           {{-120,76},{-100,96}})));

  Modelica.Blocks.Interfaces.RealInput numSenPac[num] "Number of packages sent"
    annotation (Placement(transformation(extent={{-140,-40},{-100,0}})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.EnergyDistribution
    ene(
    V_nominal=V_nominal,
    f=f,
    lat=lat,
    num=num,
    A=A,
    VWin_nominal=VWin_nominal,
    PWin_nominal=PWin_nominal,
    SOC_start=SOC_start,
    EMax=EMax,
    betDis=betDis,
    betCha=betCha,
    thrDis=thrDis,
    thrCha=thrCha,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)
    annotation (Placement(transformation(extent={{26,50},{52,70}})));

  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.TransportationBlock
    tra(numEV(start=numEV, fixed=true), num=num)
    annotation (Placement(transformation(extent={{20,-40},{40,-20}})));
  Modelica.Blocks.Interfaces.RealInput qOutSet[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,-80},{-100,-40}})));
  Modelica.Blocks.Interfaces.RealInput qIn[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=90,
        origin={-52,-120}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={-60,-120})));
  Modelica.Blocks.Interfaces.RealInput proEV
    "Probability of single EV to be charged"
    annotation (Placement(transformation(extent={{-140,-114},{-100,-74}})));
  Modelica.Blocks.Interfaces.RealOutput qOut [num] "Connector of Real output signal"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={30,-120}),
        iconTransformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={60,-120})));
  Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui [n](
    quantity="Power",
    unit="W",
    max=0)
    "Building power load(negative means consumption, positive means generation)"
    annotation (Placement(transformation(extent={{-140,0},{-100,40}}),
        iconTransformation(extent={{-140,0},{-100,40}})));

equation
  connect(term_p, term_p) annotation (Line(points={{-110,62},{-110,62}},
                      color={0,120,120}));
  connect(ene.weaBus, weaBus) annotation (Line(
      points={{25,69.8},{0,69.8},{0,86},{-110,86}},
      color={255,204,51},
      thickness=0.5));
  connect(ene.term_p, term_p) annotation (Line(points={{25,63},{-20,63},{-20,62},
          {-110,62}}, color={0,120,120}));
  connect(numSenPac, ene.numSenPac) annotation (Line(points={{-120,-20},{16,-20},
          {16,51.4},{24,51.4}},       color={0,0,127},
      pattern=LinePattern.Dot));
  connect(tra.qOut, qOut) annotation (Line(points={{41,-30},{56,-30},{56,-84},{
          30,-84},{30,-120}},
                     color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOutSet, tra.qOutSet) annotation (Line(points={{-120,-60},{0,-60},{0,
          -25},{18,-25}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qIn, tra.qIn) annotation (Line(points={{-52,-120},{-52,-120},{-52,-96},
          {-52,-96},{-52,-30},{18,-30}},
                 color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proEV, tra.proEV) annotation (Line(points={{-120,-94},{12,-94},{12,-38},
          {18,-38}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(tra.numEVCha, ene.numEV) annotation (Line(points={{41,-38},{56,-38},{
          56,0},{8,0},{8,55.4},{24,55.4}},
                                       color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[1], PBui[1]) annotation (Line(
      points={{24,57.5667},{0,57.5667},{0,1.66667},{-120,1.66667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[2], PBui[2]) annotation (Line(
      points={{24,57.9},{0,57.9},{0,5},{-120,5}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[3], PBui[3]) annotation (Line(
      points={{24,58.2333},{0,58.2333},{0,8.33333},{-120,8.33333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[4], PBui[4]) annotation (Line(
      points={{24,58.5667},{0,58.5667},{0,11.6667},{-120,11.6667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[5], PBui[5]) annotation (Line(
      points={{24,58.9},{0,58.9},{0,15},{-120,15}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[6], PBui[6]) annotation (Line(
      points={{24,59.2333},{0,59.2333},{0,18.3333},{-120,18.3333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[7], PBui[7]) annotation (Line(
      points={{24,59.5667},{0,59.5667},{0,21.6667},{-120,21.6667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[8], PBui[8]) annotation (Line(
      points={{24,59.9},{0,59.9},{0,25},{-120,25}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[9], PBui[9]) annotation (Line(
      points={{24,60.2333},{0,60.2333},{0,28.3333},{-120,28.3333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[10], PBui[10]) annotation (Line(
      points={{24,60.5667},{0,60.5667},{0,31.6667},{-120,31.6667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[11], PBui[11]) annotation (Line(
      points={{24,60.9},{0,60.9},{0,35},{-120,35}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[12], ene.PBui[12]) annotation (Line(
      points={{-120,38.3333},{0,38.3333},{0,61.2333},{24,61.2333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-74,-20},{76,-64}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={61,-20},
          rotation=90),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={61,16},
          rotation=90),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
        Line(points={{-58,22},{-32,22}},   color={0,0,0}),
        Line(points={{-46,22},{-46,-8}},    color={0,0,0}),
        Line(points={{-58,22},{-58,20}},   color={0,0,0}),
        Line(points={{-32,22},{-32,20}},   color={0,0,0}),
        Line(
          points={{-68,-8},{-66,-6},{-62,-2},{-58,12},{-58,20}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-68,-8},{-56,-6},{-50,-4},{-34,8},{-32,20}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{-22,32},{4,32}},     color={0,0,0}),
        Line(points={{-22,32},{-22,30}},   color={0,0,0}),
        Line(points={{4,32},{4,30}},       color={0,0,0}),
        Line(
          points={{-58,20},{-42,24},{-32,26},{-26,28},{-22,30}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-32,20},{-20,22},{-10,24},{-2,26},{4,30}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-22,30},{-6,34},{4,36},{10,38},{14,40}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{4,30},{16,32},{26,34},{34,36},{40,40}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{14,42},{40,42}},   color={0,0,0}),
        Line(points={{-10,32},{-10,2}},    color={0,0,0}),
        Line(points={{26,42},{26,10}},   color={0,0,0}),
        Line(points={{14,42},{14,40}},     color={0,0,0}),
        Line(points={{40,42},{40,40}},     color={0,0,0}),
        Text(
          extent={{-70,94},{68,48}},
          lineColor={28,108,200},
          pattern=LinePattern.Dash,
          lineThickness=0.5,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid,
          textString="E+T"),
        Rectangle(
          extent={{50,20},{72,-16}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{56,20},{66,10}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{56,2},{66,-8}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{56,-14},{66,-24}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid), Line(
          points={{-74,-42},{76,-42}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),
        Text(
          extent={{-150,160},{134,108}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the charging stations and charging stations in the block.The EV charging load is calculated from the transportation system.</p>
</html>"));
end EnergyTransportation;
