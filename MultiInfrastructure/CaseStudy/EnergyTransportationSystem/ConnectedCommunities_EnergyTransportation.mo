within MultiInfrastructure.CaseStudy.EnergyTransportationSystem;
model ConnectedCommunities_EnergyTransportation
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  BaseClasses.EnergyTransportation resBlo1(lat=weaDat.lat,
    A=20000,
    EMax=1.8e10,
    betDis=0.5,
    betCha=1,
    numEV=850)
    annotation (Placement(transformation(extent={{-62,40},{-42,60}})));
  BaseClasses.EnergyTransportation resBlo2(lat=weaDat.lat,
    thrCha=-6e5,
    A=30000,
    EMax=2.1e10,
    betDis=0.5,
    betCha=1,
    numEV=850)
    annotation (Placement(transformation(extent={{60,40},{40,60}})));
  BaseClasses.EnergyTransportation comBlo(lat=weaDat.lat, numEV=200,
    betDis=0.3,
    betCha=0.9,
    thrCha=-3.5e5,
    A=50000,
    EMax=2.1e10)
    annotation (Placement(transformation(extent={{-10,-50},{10,-70}})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa1(
    numIni=5,
    l=9000,
    redeclare
      IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp)         annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={0,32})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa2(
    numIni=5,
    l=9000,
    redeclare
      IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp)         annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={0,24})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa3(
    l=10000,
    numIni=5,
    redeclare
      IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.CClassRoad60
      roaTyp)   annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-46,-6})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa4(
    numIni=5,
    l=10000,
    redeclare
      IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.CClassRoad60
      roaTyp)         annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-58,-6})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa5(
    l=10000,
    numIni=5,
    redeclare
      IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.BClassRoad40
      roaTyp) annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={44,-6})));
  CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses.RoadVariableDelay
    roa6(
    numIni=5,
    l=8000,
    redeclare
      IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.BClassRoad40
      roaTyp)         annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={56,-6})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false, filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-90,70})));
  Modelica.Blocks.Sources.CombiTimeTable PBui1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-31642.24665; 1,-25737.65655; 2,-23374.2521; 3,-22685.2667; 4,-22501.96805;
        5,-22001.6213; 6,-23525.33505; 7,-28600.1871; 8,-33781.5394; 9,-29866.28095;
        10,-23544.42185; 11,-22349.86555; 12,-21847.5527; 13,-21501.7796; 14,-21735.9029;
        15,-22513.4214; 16,-24298.4994; 17,-30480.0999; 18,-41825.40685; 19,-50450.39685;
        20,-50320.518; 21,-48195.64265; 22,-44804.9175; 23,-37865.276; 24,-31642.24665])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-98,44},{-88,54}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-65489.46691; 1,-52579.71872; 2,-46793.20889; 3,-44550.76517; 4,-44086.60396;
        5,-44036.65506; 6,-46260.73703; 7,-57901.44167; 8,-72613.09332; 9,-69900.28072;
        10,-59006.96062; 11,-58092.84249; 12,-56041.65098; 13,-53937.63598; 14,-52003.87518;
        15,-51703.49376; 16,-55460.0902; 17,-69523.23761; 18,-96347.78504; 19,-112934.2919;
        20,-111558.4958; 21,-104309.0771; 22,-96400.3119; 23,-80807.46526; 24,-65489.46691])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-98,30},{-88,40}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-93547.60327; 1,-81765.20272; 2,-71653.916; 3,-67617.96395; 4,-66564.75353;
        5,-65594.14602; 6,-67298.99476; 7,-83343.95152; 8,-104325.7667; 9,-98738.47411;
        10,-79365.80039; 11,-74866.26255; 12,-72194.08975; 13,-69795.18078; 14,-68179.44484;
        15,-67469.89108; 16,-73082.1226; 17,-94165.77606; 18,-133049.0548; 19,-161614.4816;
        20,-162519.0552; 21,-151231.1099; 22,-139018.1881; 23,-116633.5399; 24,-93547.60327])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-98,16},{-88,26}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui7(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-15106.99921; 1,-15106.99921; 2,-15106.99921; 3,-15162.01319; 4,-15190.36283;
        5,-15292.58251; 6,-15288.13499; 7,-22916.16308; 8,-22530.14638; 9,-32344.27609;
        10,-48736.55298; 11,-48736.55298; 12,-48736.55298; 13,-48736.55298; 14,-48736.55298;
        15,-48736.55298; 16,-48736.55298; 17,-54479.05298; 18,-60221.55298; 19,-48376.76377;
        20,-48376.76377; 21,-43829.27609; 22,-22377.64918; 23,-15106.99921; 24,-15106.99921])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-98,-40},{-88,-30}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui8(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-39665.66037; 1,-22880.25538; 2,-14648.29608; 3,-14664.78088; 4,-14671.08278;
        5,-14817.98117; 6,-23008.873; 7,-38970.01719; 8,-43260.60256; 9,-42212.49951;
        10,-34726.26651; 11,-42235.95747; 12,-43019.25007; 13,-42320.02807; 14,-34790.53907;
        15,-34789.96052; 16,-34769.04615; 17,-44296.78038; 18,-46418.70809; 19,-46421.70322;
        20,-39060.35347; 21,-39038.64715; 22,-38966.13298; 23,-38856.78773; 24,-39665.66037])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-98,-54},{-88,-44}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui9(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-113199.5177; 1,-109803.5388; 2,-102368.0109; 3,-118330.9617; 4,-106788.0383;
        5,-148854.4245; 6,-183652.7374; 7,-232847.716; 8,-247709.1024; 9,-248902.5028;
        10,-256398.0105; 11,-255070.6781; 12,-258375.3686; 13,-248890.5716; 14,-255974.4066;
        15,-250712.9033; 16,-248438.0369; 17,-256005.5167; 18,-263049.3853; 19,-174078.2046;
        20,-172535.5662; 21,-150404.431; 22,-125633.5674; 23,-95590.35684; 24,-113199.5177])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-98,-68},{-88,-58}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui10(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-46268.5865; 1,-44616.12113; 2,-48249.87495; 3,-44890.35052; 4,-45974.81958;
        5,-48485.49996; 6,-53000.83359; 7,-71074.69613; 8,-78345.33465; 9,-97123.15792;
        10,-81180.84723; 11,-56799.73352; 12,-53500.92217; 13,-52553.03076; 14,-51871.65923;
        15,-51188.67417; 16,-50625.71801; 17,-58819.71068; 18,-71170.73161; 19,-85193.82708;
        20,-85850.21377; 21,-91994.72379; 22,-89762.32297; 23,-67564.07879; 24,-46268.5865])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-98,-82},{-88,-72}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui4(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-36856.7682; 1,-30109.66872; 2,-26911.8066; 3,-25905.7908; 4,-25780.80756;
        5,-25155.5178; 6,-27082.74654; 7,-32366.52792; 8,-39100.60374; 9,-34581.82464;
        10,-27258.65676; 11,-25785.80442; 12,-25415.8515; 13,-24931.6566; 14,-25393.53036;
        15,-26149.71714; 16,-28371.35976; 17,-35629.00254; 18,-48714.62742; 19,
        -59249.40528; 20,-59161.0404; 21,-56793.59538; 22,-52699.33704; 23,-44223.9339;
        24,-36856.7682])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{94,50},{84,60}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui5(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-73419.43823; 1,-59627.21458; 2,-52576.6988; 3,-49491.06976; 4,-49188.12081;
        5,-48884.82785; 6,-51691.14179; 7,-65106.38244; 8,-82257.84709; 9,-78533.45258;
        10,-65435.20235; 11,-63415.22063; 12,-61581.85481; 13,-59000.12924; 14,
        -57241.53931; 15,-56606.19439; 16,-61360.6668; 17,-77812.178; 18,-109067.4244;
        19,-129399.214; 20,-127940.8268; 21,-119847.2756; 22,-110192.6585; 23,-91387.73128;
        24,-73419.43823])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{94,34},{84,44}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui6(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-104099.2344; 1,-86473.71657; 2,-76022.39164; 3,-70742.03559; 4,-71216.27148;
        5,-70407.99691; 6,-73316.15983; 7,-90569.96931; 8,-116724.5089; 9,-109958.8606;
        10,-87132.76907; 11,-81241.05547; 12,-77935.20867; 13,-74881.80258; 14,
        -72740.52869; 15,-71785.08938; 16,-79316.34138; 17,-103391.7425; 18,-148729.3196;
        19,-183510.4413; 20,-184961.8902; 21,-173115.099; 22,-157626.1633; 23,-129665.1893;
        24,-104099.2344])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{94,18},{84,28}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,16; 1,14; 2,18; 3,10; 4,15; 5,43; 6,57; 7,91; 8,542; 9,124; 10,82;
        11,59; 12,55; 13,65; 14,82; 15,75; 16,77; 17,58; 18,46; 19,70; 20,58;
        21,21; 22,16; 23,15; 24,16])
    "Traffic outflow for residential block1 to commercial block"
    annotation (Placement(transformation(extent={{-98,2},{-88,12}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,12; 1,16; 2,7; 3,12; 4,27; 5,20; 6,31; 7,48; 8,58; 9,62; 10,73; 11,
        83; 12,68; 13,80; 14,96; 15,65; 16,61; 17,81; 18,105; 19,102; 20,77; 21,
        65; 22,43; 23,18; 24,12])
    "Traffic outflow for residential block1 to residential block2"
    annotation (Placement(transformation(extent={{-98,-12},{-88,-2}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes1(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.3; 1,0.28; 2,0.25; 3,0.25; 4,0.15; 5,0.1; 6,0.1; 7,0.1; 8,0.05; 9,
        0.05; 10,0.05; 11,0.05; 12,0.05; 13,0.05; 14,0.05; 15,0.05; 16,0.05; 17,
        0.05; 18,0.05; 19,0.05; 20,0.15; 21,0.15; 22,0.3; 23,0.3; 24,0.3])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-98,-26},{-88,-16}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,18; 1,10; 2,5; 3,8; 4,18; 5,54; 6,72; 7,108; 8,576; 9,109; 10,79; 11,
        80; 12,81; 13,85; 14,97; 15,72; 16,38; 17,49; 18,65; 19,74; 20,65; 21,28;
        22,15; 23,18; 24,18])
    "Traffic outflow for residential block2 to commercial block"
    annotation (Placement(transformation(extent={{94,2},{84,12}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut4(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,16; 1,14; 2,15; 3,10; 4,24; 5,28; 6,40; 7,42; 8,48; 9,56; 10,68; 11,
        76; 12,76; 13,73; 14,88; 15,59; 16,55; 17,129; 18,101; 19,95; 20,70; 21,
        59; 22,57; 23,16; 24,16])
    "Traffic outflow for residential block2 to residential block1"
    annotation (Placement(transformation(extent={{94,-12},{84,-2}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes2(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.3; 1,0.28; 2,0.25; 3,0.25; 4,0.15; 5,0.1; 6,0.1; 7,0.1; 8,0.05; 9,
        0.05; 10,0.05; 11,0.05; 12,0.05; 13,0.05; 14,0.05; 15,0.05; 16,0.05; 17,
        0.05; 18,0.05; 19,0.05; 20,0.15; 21,0.15; 22,0.3; 23,0.3; 24,0.3])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{94,-26},{84,-16}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut5(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,18; 1,13; 2,16; 3,18; 4,14; 5,19; 6,26; 7,16; 8,35; 9,41; 10,49; 11,
        55; 12,63; 13,57; 14,69; 15,79; 16,75; 17,108; 18,551; 19,124; 20,106; 21,
        58; 22,54; 23,42; 24,18]) "Traffic outflow for residential block1"
    annotation (Placement(transformation(extent={{94,-42},{84,-32}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut6(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,23; 1,17; 2,21; 3,22; 4,18; 5,14; 6,20; 7,20; 8,41; 9,48; 10,57; 11,
        66; 12,72; 13,69; 14,89; 15,71; 16,86; 17,125; 18,555; 19,165; 20,98; 21,
        55; 22,46; 23,31; 24,23]) "Traffic outflow for residential block1"
    annotation (Placement(transformation(extent={{94,-56},{84,-46}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes3(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.03; 1,0.03; 2,0.03; 3,0.03; 4,0.03; 5,0.03; 6,0.03; 7,0.03; 8,0.05;
        9,0.05; 10,0.08; 11,0.08; 12,0.08; 13,0.08; 14,0.08; 15,0.08; 16,0.08; 17,
        0.03; 18,0.03; 19,0.03; 20,0.03; 21,0.03; 22,0.03; 23,0.03; 24,0.03])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{94,-70},{84,-60}})));
  Modelica.Blocks.Sources.CombiTimeTable nev(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,200; 1,193; 2,187; 3,173; 4,151; 5,153; 6,217; 7,300; 8,463; 9,
        1505; 10,1649; 11,1704; 12,1723; 13,1723; 14,1748; 15,1768; 16,1765; 17,
        1720; 18,1594; 19,599; 20,454; 21,373; 22,309; 23,240; 24,200])
    "Number of EV charging profile for commercial"
    annotation (Placement(transformation(extent={{-56,-94},{-46,-84}})));
equation
  connect(resBlo1.qOut[1], roa1.qIn)
    annotation (Line(points={{-46,38},{-46,32},{-12,32}}, color={28,108,200},
      thickness=1));
  connect(roa1.qOut, resBlo2.qIn[1])
    annotation (Line(points={{11,32},{56,32},{56,38}}, color={28,108,200},
      thickness=1));
  connect(resBlo1.qOut[2], roa3.qIn)
    annotation (Line(points={{-46,38},{-46,6}},         color={28,108,200},
      thickness=1));
  connect(resBlo2.qOut[1], roa2.qIn)
    annotation (Line(points={{44,38},{44,24},{12,24}}, color={28,108,200},
      thickness=1));
  connect(roa2.qOut, resBlo1.qIn[1])
    annotation (Line(points={{-11,24},{-58,24},{-58,38}}, color={28,108,200},
      thickness=1));
  connect(resBlo2.qOut[2], roa5.qIn)
    annotation (Line(points={{44,38},{44,6}},        color={28,108,200},
      thickness=1));
  connect(roa5.qOut, comBlo.qIn[2]) annotation (Line(points={{44,-17},{44,-32},
          {-6,-32},{-6,-48}}, color={28,108,200},
      thickness=1));
  connect(roa3.qOut, comBlo.qIn[1]) annotation (Line(
      points={{-46,-17},{-46,-32},{-6,-32},{-6,-48}},
      color={28,108,200},
      thickness=1));
  connect(roa6.qOut, resBlo2.qIn[2]) annotation (Line(
      points={{56,5},{56,38}},
      color={28,108,200},
      thickness=1));
  connect(comBlo.qOut[1], roa4.qIn) annotation (Line(
      points={{6,-48},{6,-40},{-58,-40},{-58,-18}},
      color={28,108,200},
      thickness=1));
  connect(roa4.qOut, resBlo1.qIn[2]) annotation (Line(
      points={{-58,5},{-58,38}},
      color={28,108,200},
      thickness=1));
  connect(comBlo.qOut[2], roa6.qIn) annotation (Line(
      points={{6,-48},{6,-40},{56,-40},{56,-18}},
      color={28,108,200},
      thickness=1));
  connect(weaDat.weaBus, resBlo1.weaBus) annotation (Line(
      points={{-80,90},{-70,90},{-70,58.6},{-63,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, resBlo2.weaBus) annotation (Line(
      points={{-80,90},{80,90},{80,58.6},{61,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-80,90},{-70,90},{-70,-68.6},{-11,-68.6}},
      color={255,204,51},
      thickness=0.5));
  connect(gri.terminal, resBlo1.term_p) annotation (Line(
      points={{-90,60},{-90,56.2},{-63,56.2}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, comBlo.term_p) annotation (Line(
      points={{-90,60},{-90,56},{-74,56},{-74,-66.2},{-11,-66.2}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, resBlo2.term_p) annotation (Line(
      points={{-90,60},{-90,56},{-74,56},{-74,86},{78,86},{78,56.2},{61,56.2}},
      color={0,120,120},
      thickness=0.5));
  connect(PBui1.y[1], resBlo1.PBui[10]) annotation (Line(
      points={{-87.5,49},{-78,49},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[11]) annotation (Line(
      points={{-87.5,49},{-78,49},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[12]) annotation (Line(
      points={{-87.5,49},{-78,49},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui2.y[1], resBlo1.PBui[2]) annotation (Line(
      points={{-87.5,35},{-78,35},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui2.y[1], resBlo1.PBui[3]) annotation (Line(
      points={{-87.5,35},{-78,35},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui2.y[1], resBlo1.PBui[6]) annotation (Line(
      points={{-87.5,35},{-78,35},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui2.y[1], resBlo1.PBui[7]) annotation (Line(
      points={{-87.5,35},{-78,35},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui2.y[1], resBlo1.PBui[8]) annotation (Line(
      points={{-87.5,35},{-78,35},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui3.y[1], resBlo1.PBui[5]) annotation (Line(
      points={{-87.5,21},{-78,21},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui3.y[1], resBlo1.PBui[4]) annotation (Line(
      points={{-87.5,21},{-78,21},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui3.y[1], resBlo1.PBui[9]) annotation (Line(
      points={{-87.5,21},{-78,21},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui6.y[1], resBlo2.PBui[5]) annotation (Line(
      points={{83.5,23},{76,23},{76,52},{62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui6.y[1], resBlo2.PBui[4]) annotation (Line(
      points={{83.5,23},{76,23},{76,52},{62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui6.y[1], resBlo2.PBui[9]) annotation (Line(
      points={{83.5,23},{76,23},{76,52},{62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui7.y[1], comBlo.PBui[10]) annotation (Line(
      points={{-87.5,-35},{-62,-35},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui7.y[1], comBlo.PBui[11]) annotation (Line(
      points={{-87.5,-35},{-62,-35},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui7.y[1], comBlo.PBui[12]) annotation (Line(
      points={{-87.5,-35},{-62,-35},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui8.y[1], comBlo.PBui[1]) annotation (Line(
      points={{-87.5,-49},{-62,-49},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui8.y[1], comBlo.PBui[2]) annotation (Line(
      points={{-87.5,-49},{-62,-49},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui8.y[1], comBlo.PBui[3]) annotation (Line(
      points={{-87.5,-49},{-62,-49},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui8.y[1], comBlo.PBui[6]) annotation (Line(
      points={{-87.5,-49},{-62,-49},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui8.y[1], comBlo.PBui[7]) annotation (Line(
      points={{-87.5,-49},{-62,-49},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui8.y[1], comBlo.PBui[8]) annotation (Line(
      points={{-87.5,-49},{-62,-49},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui9.y[1], comBlo.PBui[5]) annotation (Line(
      points={{-87.5,-63},{-62,-63},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui10.y[1], comBlo.PBui[4]) annotation (Line(
      points={{-87.5,-77},{-62,-77},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui10.y[1], comBlo.PBui[9]) annotation (Line(
      points={{-87.5,-77},{-62,-77},{-62,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[1]) annotation (Line(
      points={{-87.5,49},{-78,49},{-78,52},{-64,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui4.y[1], resBlo2.PBui[1]) annotation (Line(points={{83.5,55},{76,
          55},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui4.y[1], resBlo2.PBui[12]) annotation (Line(points={{83.5,55},{76,
          55},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui4.y[1], resBlo2.PBui[11]) annotation (Line(points={{83.5,55},{76,
          55},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui4.y[1], resBlo2.PBui[10]) annotation (Line(
      points={{83.5,55},{76,55},{76,52},{62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui5.y[1], resBlo2.PBui[8]) annotation (Line(points={{83.5,39},{76,
          39},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui5.y[1], resBlo2.PBui[7]) annotation (Line(points={{83.5,39},{76,
          39},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui5.y[1], resBlo2.PBui[6]) annotation (Line(points={{83.5,39},{76,
          39},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui5.y[1], resBlo2.PBui[3]) annotation (Line(points={{83.5,39},{76,
          39},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui5.y[1], resBlo2.PBui[2]) annotation (Line(
      points={{83.5,39},{76,39},{76,52},{62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut1.y[1], resBlo1.qOutSet[2]) annotation (Line(
      points={{-87.5,7},{-78,7},{-78,44},{-64,44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut2.y[1], resBlo1.qOutSet[1]) annotation (Line(
      points={{-87.5,-7},{-78,-7},{-78,44},{-64,44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut1.y[1], resBlo1.numSenPac[2]) annotation (Line(
      points={{-87.5,7},{-78,7},{-78,48},{-64,48}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut2.y[1], resBlo1.numSenPac[1]) annotation (Line(
      points={{-87.5,-7},{-78,-7},{-78,48},{-64,48}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes1.y[1], resBlo1.proEV) annotation (Line(
      points={{-87.5,-21},{-78,-21},{-78,40.6},{-64,40.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut3.y[1], resBlo2.qOutSet[2]) annotation (Line(points={{83.5,7},{70,
          7},{70,44},{62,44}}, color={0,0,127}));
  connect(proRes2.y[1], resBlo2.proEV) annotation (Line(
      points={{83.5,-21},{70,-21},{70,40.6},{62,40.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut3.y[1], resBlo2.numSenPac[2]) annotation (Line(
      points={{83.5,7},{70,7},{70,48},{62,48}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut5.y[1], comBlo.qOutSet[1]) annotation (Line(points={{83.5,-37},{
          70,-37},{70,-84},{-30,-84},{-30,-54},{-12,-54}},
                                                        color={0,0,127}));
  connect(qOut6.y[1], comBlo.qOutSet[2]) annotation (Line(
      points={{83.5,-51},{70,-51},{70,-84},{-30,-84},{-30,-54},{-12,-54}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut5.y[1], comBlo.numSenPac[1]) annotation (Line(
      points={{83.5,-37},{70,-37},{70,-84},{-30,-84},{-30,-58},{-12,-58}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut6.y[1], comBlo.numSenPac[2]) annotation (Line(
      points={{83.5,-51},{70,-51},{70,-84},{-30,-84},{-30,-58},{-12,-58}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes3.y[1], comBlo.proEV) annotation (Line(
      points={{83.5,-65},{70,-65},{70,-84},{-30,-84},{-30,-50.6},{-12,-50.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut4.y[1], resBlo2.numSenPac[1]) annotation (Line(
      points={{83.5,-7},{70,-7},{70,48},{62,48}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut4.y[1], resBlo2.qOutSet[1]) annotation (Line(
      points={{83.5,-7},{70,-7},{70,44},{62,44}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end ConnectedCommunities_EnergyTransportation;
