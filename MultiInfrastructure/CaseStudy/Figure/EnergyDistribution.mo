within MultiInfrastructure.CaseStudy.Figure;
model EnergyDistribution "Model of power supply and demand"
  parameter Modelica.SIunits.Area A=60000 "Net surface area of PV"
    annotation(Dialog(tab="Supply",group="PVs"));
  parameter Modelica.SIunits.Voltage VPV_nominal=480
    "Nominal voltage of PV (VPV_nominal >= 0)"
    annotation (Dialog(tab="Supply",group="PVs"));
  parameter Modelica.SIunits.Voltage VWin_nominal=480
    "Nominal voltage of wind turbine (VWin_nominal >= 0)"
    annotation (Dialog(tab="Supply",group="Wind Turbines"));
  parameter Modelica.SIunits.Power PV_nominal=-PLoa_nominal "Nominal power of PV panels"
    annotation(Dialog(tab="Supply",group="PVs"));
  parameter Modelica.SIunits.Power PWin_nominal=-PLoa_nominal
    "Nominal power pf the wind turbine"
    annotation(Dialog(tab="Supply",group="Wind Turbines"));
  parameter Modelica.SIunits.Voltage V_nominal=10000 "Nominal voltage(V_nominal>=0)";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  parameter Modelica.SIunits.Power PLoa_nominal=PBb_nominal+PEv_nominal+PCt_nominal
    "Nominal power of demand load (negative if consumed, positive if generated)"
    annotation(Dialog(tab="Demand",group="Parameters"));
  parameter Modelica.SIunits.Angle lat "Latitude";
  parameter Modelica.SIunits.Power PBb_nominal=-1500000
    "Nominal power of building blocks (negative if consumed, positive if generated)"
    annotation(Dialog(tab="Demand",group="Parameters"));
  parameter Modelica.SIunits.Power PEv_nominal=-1500000
    "Nominal power of EV charging (negative if consumed, positive if generated)"
    annotation(Dialog(tab="Demand",group="Parameters"));
  parameter Modelica.SIunits.Power PCt_nominal=-70000
    "Nominal power of communication towers (negative if consumed, positive if generated)"
    annotation(Dialog(tab="Demand",group="Parameters"));
  parameter Modelica.SIunits.Length l=2000 "Length of the main line in the energy model"
    annotation (Dialog(tab="Line",group="Parameters"));
  parameter Modelica.SIunits.Length l1=2000 "Length of the line 1 in the energy model"
    annotation (Dialog(tab="Line",group="Parameters"));
  parameter Modelica.SIunits.Length l2=2000 "Length of the line 2 in the energy model"
    annotation (Dialog(tab="Line",group="Parameters"));
  parameter Integer num "Number of ports in the communication block"
    annotation (Dialog(tab="Demand"));
  parameter Integer n=12 "Number of buildings in the community";
  replaceable parameter
    MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable
    constrainedby
    MultiInfrastructure.Buildings.Electrical.Transmission.BaseClasses.BaseCable
    "Commercial cables options"
    annotation (
    Evaluate=true,Dialog(
    tab="Line",group="Manual mode",
    enable=mode == MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial),
    choicesAllMatching=true);
  parameter Real SOC_start=0.6 "Initial charge"
    annotation (Dialog(tab="Battery"));
  parameter Modelica.SIunits.Energy EMax=18000000000 "Maximum available charge"
    annotation (Dialog(tab="Battery"));
  parameter Real betDis=0.3 "Discharging velocity coefficient"
    annotation (Dialog(tab="Battery"));
  parameter Real betCha=0.9 "Charging velocity coefficient"
    annotation (Dialog(tab="Battery"));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
    annotation (Placement(transformation(extent={{-120,20},{-100,40}}),
        iconTransformation(extent={{-120,20},{-100,40}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-130,60},{-90,100}}),iconTransformation(extent={{-120,88},
            {-100,108}})));
  Modelica.Blocks.Interfaces.RealInput numEV
    "Number of EV charging in the block"
    annotation (Placement(transformation(extent={{-140,-54},{-100,-14}}),
        iconTransformation(extent={{-140,-54},{-100,-14}})));
  MultiInfrastructure.IndividualSystem.Energy.SupplySide.Supply sup(
    PV_nominal=-PLoa_nominal,
    lat=lat,
    use_C=false,
    A=A,
    l=10000,
    l1=2000,
    l2=2000,
    V_nominal=V_nominal,
    PWin_nominal=PWin_nominal,
    VABase=PLoa_nominal,
    VHigh=10000,
    VLow=480,
    XoverR=8,
    Zperc=0.03,
    VPV_nominal=VPV_nominal,
    VWin_nominal=VWin_nominal,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable)         "Supply side"
    annotation (Placement(transformation(extent={{-30,54},{-10,74}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable)
                            "Line from or to grid" annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-72,30})));

  Modelica.Blocks.Interfaces.RealInput numSenPac[num] "Number of packages sent"
    annotation (Placement(transformation(extent={{-140,-90},{-100,-50}}),
        iconTransformation(extent={{-140,-90},{-100,-50}})));
  IndividualSystem.Energy.DemandSide.DistributionSystem disSys(
    num=num,
    n=12,
    V_nominal=V_nominal)
    annotation (Placement(transformation(extent={{-36,-44},{-4,-20}})));
  Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui [n](
    quantity="Power",
    unit="W",
    max=0)
    "Building power load(negative means consumption, positive means generation)"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}}),
        iconTransformation(extent={{-140,-20},{-100,20}})));

  parameter Modelica.SIunits.Power thrDis=-3e5 "Discharging power threshold"
    annotation (Dialog(tab="Battery"));
  parameter Modelica.SIunits.Power thrCha=-2e5 "Charging power threshold"
    annotation (Dialog(tab="Battery"));
  Battery battery
    annotation (Placement(transformation(extent={{32,-10},{52,10}})));
equation
  connect(sup.weaBus, weaBus) annotation (Line(
      points={{-31,72.4},{-86,72.4},{-86,80},{-110,80}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(lin.terminal_n, term_p)
    annotation (Line(points={{-82,30},{-110,30}},
                                                color={0,120,120},
      thickness=0.5));
  connect(disSys.PBui[6], PBui[6]) annotation (Line(
      points={{-34.3158,-30.6},{-66,-30.6},{-66,0},{-116,0},{-116,-1.66667},{
          -120,-1.66667}},
      color={0,0,127}));

  connect(disSys.numEV, numEV) annotation (Line(
      points={{-34.3158,-34.0571},{-40,-34.0571},{-40,-34},{-120,-34}},
      color={0,0,127}));
  connect(numSenPac, disSys.numSenPac) annotation (Line(
      points={{-120,-70},{-46,-70},{-46,-37.3143},{-34.3158,-37.3143}},
      color={0,0,127}));

  connect(disSys.powDem, battery.PDem) annotation (Line(points={{-3.15789,
          -32.8571},{12,-32.8571},{12,-4.6},{30,-4.6}}, color={0,0,127}));
  connect(lin.terminal_p, battery.term_p) annotation (Line(
      points={{-62,30},{8,30},{8,-1.2},{30.8,-1.2}},
      color={0,120,120},
      thickness=0.5));
  connect(sup.powSup, battery.PSup) annotation (Line(points={{-9,64},{12,64},{
          12,-2.2},{31,-2.2}}, color={0,0,127}));
  connect(sup.term_p, lin.terminal_p) annotation (Line(
      points={{-31,64.2},{-62,64.2},{-62,30}},
      color={0,120,120},
      thickness=0.5));
  connect(disSys.ter1, lin.terminal_p) annotation (Line(
      points={{-33.4737,-22.9143},{-52,-22.9143},{-52,30},{-62,30}},
      color={0,120,120},
      thickness=0.5));
  connect(lin.terminal_p, disSys.ter2) annotation (Line(
      points={{-62,30},{-56,30},{-56,-25.1429},{-33.4737,-25.1429}},
      color={0,120,120},
      thickness=0.5));
  connect(disSys.ter3, lin.terminal_p) annotation (Line(
      points={{-33.4737,-27.3714},{-62,-27.3714},{-62,30}},
      color={0,120,120},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {160,100}}),                                        graphics={
        Rectangle(
          extent={{70,-64},{10,-70}},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-66,76},{-68,6}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{7,-1},{-21,-9},{-17,-13},{7,-1}},
          pattern=LinePattern.None,
          origin={-55,57},
          rotation=-90,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{-94,92},{-68,80},{-70,76},{-94,92}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-38,92},{-66,80},{-64,76},{-38,92}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{21.5,96.5},{93.5,24.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{24.5,93.5},{90.5,27.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{36.5,81.5},{78.5,39.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-28,4},{-46,-48},{2,-48},{20,4},{-28,4}},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Ellipse(
          extent={{39.5,78.5},{75.5,42.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{45.5,72.5},{69.5,48.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{56,62},{32,0},{34,0},{56,58},{56,62}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{50,46},{64,44}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52.5,65.5},{62,56}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{58,62},{82,0},{80,0},{58,58},{58,62}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-50,64},{14,4}},
        lineColor={150,150,150},
        fillPattern=FillPattern.Solid,
        fillColor={150,150,150}),
      Polygon(
        points={{-18,88},{-60,64},{24,64},{-18,88}},
        lineColor={95,95,95},
        smooth=Smooth.None,
        fillPattern=FillPattern.Solid,
        fillColor={95,95,95}),
      Rectangle(
        extent={{-38,40},{-24,54}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-12,40},{2,54}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-38,18},{-24,32}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-12,18},{2,32}},
        lineColor={255,255,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{16,16},{64,-68}},
          pattern=LinePattern.None,
          lineColor={0,0,0},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          radius=3),
        Rectangle(
          extent={{64,2},{74,-2}},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{72,2},{76,-52}},
          pattern=LinePattern.None,
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          radius=4,
          lineColor={0,0,0}),
        Rectangle(
          extent={{72,-48},{86,-52}},
          pattern=LinePattern.None,
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          radius=4,
          lineColor={0,0,0}),
        Rectangle(
          extent={{82,-22},{86,-52}},
          pattern=LinePattern.None,
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          radius=5,
          lineColor={0,0,0}),
        Ellipse(
          extent={{78,-16},{90,-32}},
          pattern=LinePattern.None,
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{78,-12},{90,-22}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{22,8},{58,-16}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          radius=2,
          lineColor={0,0,0}),
        Rectangle(
          extent={{80,-16},{82,-22}},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{86,-16},{88,-22}},
          fillColor={150,225,75},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{46,34},{68,32}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-72,10},{-90,-42},{-42,-42},{-24,10},{-72,10}},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-68,6},{-72,-6},{-62,-6},{-58,6},{-68,6}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-54,6},{-58,-6},{-48,-6},{-44,6},{-54,6}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-40,6},{-44,-6},{-34,-6},{-30,6},{-40,6}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-60,-10},{-64,-22},{-54,-22},{-50,-10},{-60,-10}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-46,-10},{-50,-22},{-40,-22},{-36,-10},{-46,-10}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-74,-10},{-78,-22},{-68,-22},{-64,-10},{-74,-10}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-66,-26},{-70,-38},{-60,-38},{-56,-26},{-66,-26}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-52,-26},{-56,-38},{-46,-38},{-42,-26},{-52,-26}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-80,-26},{-84,-38},{-74,-38},{-70,-26},{-80,-26}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-24,0},{-28,-12},{-18,-12},{-14,0},{-24,0}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-10,0},{-14,-12},{-4,-12},{0,0},{-10,0}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{4,0},{0,-12},{10,-12},{14,0},{4,0}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-16,-16},{-20,-28},{-10,-28},{-6,-16},{-16,-16}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-2,-16},{-6,-28},{4,-28},{8,-16},{-2,-16}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-30,-16},{-34,-28},{-24,-28},{-20,-16},{-30,-16}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-22,-32},{-26,-44},{-16,-44},{-12,-32},{-22,-32}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-8,-32},{-12,-44},{-2,-44},{2,-32},{-8,-32}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-36,-32},{-40,-44},{-30,-44},{-26,-32},{-36,-32}},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Ellipse(
          extent={{-70,80},{-64,74}},
          pattern=LinePattern.None,
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Line(points={{-90,-70},{-64,-70}}, color={0,0,0}),
        Line(points={{-78,-70},{-78,-100}}, color={0,0,0}),
        Line(points={{-90,-70},{-90,-72}}, color={0,0,0}),
        Line(points={{-64,-70},{-64,-72}}, color={0,0,0}),
        Line(
          points={{-100,-100},{-98,-98},{-94,-94},{-90,-80},{-90,-72}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-100,-100},{-88,-98},{-82,-96},{-66,-84},{-64,-72}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{-54,-60},{-28,-60}}, color={0,0,0}),
        Line(points={{-42,-60},{-42,-90}}, color={0,0,0}),
        Line(points={{-54,-60},{-54,-62}}, color={0,0,0}),
        Line(points={{-28,-60},{-28,-62}}, color={0,0,0}),
        Line(
          points={{-90,-72},{-74,-68},{-64,-66},{-58,-64},{-54,-62}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-64,-72},{-52,-70},{-42,-68},{-34,-66},{-28,-62}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{-18,-50},{8,-50}}, color={0,0,0}),
        Line(points={{-6,-50},{-6,-80}}, color={0,0,0}),
        Line(points={{-18,-50},{-18,-52}}, color={0,0,0}),
        Line(points={{8,-50},{8,-52}}, color={0,0,0}),
        Line(
          points={{-54,-62},{-38,-58},{-28,-56},{-22,-54},{-18,-52}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-28,-62},{-16,-60},{-6,-58},{2,-56},{8,-52}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{160,100}})),
    __Dymola_Commands,
    Documentation(info="<html>
    <p>This model shows the energy provision and use in the block. As the schematics shows, 
    the power supply from the PV and wind turbine is consumed by the building blocks, 
    EV charging stations, and the communication towers. If it is oversupply, 
    the power will flow from the block to other blocks or the grid. Conversely, 
    if the supply is not sufficient, the grid or other blocks will provide the power.</p>
<p><br><img src=\"modelica://MultiInfrastructure/Resources/Images/IndividualSystem/Energy/Energy.png\"/></p>
</html>"));
end EnergyDistribution;
