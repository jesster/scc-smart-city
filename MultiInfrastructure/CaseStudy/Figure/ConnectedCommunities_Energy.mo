within MultiInfrastructure.CaseStudy.Figure;
model ConnectedCommunities_Energy
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  EnergySystem.BaseClasses.EnergyDistribution resBlo1(
    lat=weaDat.lat,
    num=1,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_2
      commercialCable,
    A=100000,
    SOC_start=0.5,
    EMax=2e10,
    betCha=0.7,
    thrDis=-1.2e6,
    betDis=0.7,
    thrCha=-7e5)
    annotation (Placement(transformation(extent={{-30,20},{-4,40}})));
  EnergySystem.BaseClasses.EnergyDistribution comBlo(
    lat=weaDat.lat,
    num=1,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_2
      commercialCable,
    A=150000,
    SOC_start=0.5,
    EMax=2e10,
    thrDis=-1.2e6,
    thrCha=-3.5e5)
    annotation (Placement(transformation(extent={{-10,-24},{16,-4}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false, filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-90,80},{-70,100}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-80,70})));
  Modelica.Blocks.Sources.CombiTimeTable PBui1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-31642.24665; 1,-25737.65655; 2,-23374.2521; 3,-22685.2667; 4,-22501.96805;
        5,-22001.6213; 6,-23525.33505; 7,-28600.1871; 8,-33781.5394; 9,-29866.28095;
        10,-23544.42185; 11,-22349.86555; 12,-21847.5527; 13,-21501.7796; 14,-21735.9029;
        15,-22513.4214; 16,-24298.4994; 17,-30480.0999; 18,-41825.40685; 19,-50450.39685;
        20,-50320.518; 21,-48195.64265; 22,-44804.9175; 23,-37865.276; 24,-31642.24665])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-90,34},{-80,44}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-15106.99921; 1,-15106.99921; 2,-15106.99921; 3,-15162.01319; 4,-15190.36283;
        5,-15292.58251; 6,-15288.13499; 7,-22916.16308; 8,-22530.14638; 9,-32344.27609;
        10,-48736.55298; 11,-48736.55298; 12,-48736.55298; 13,-48736.55298; 14,-48736.55298;
        15,-48736.55298; 16,-48736.55298; 17,-54479.05298; 18,-60221.55298; 19,-48376.76377;
        20,-48376.76377; 21,-43829.27609; 22,-22377.64918; 23,-15106.99921; 24,-15106.99921])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-90,-36},{-80,-26}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-36856.7682; 1,-30109.66872; 2,-26911.8066; 3,-25905.7908; 4,-25780.80756;
        5,-25155.5178; 6,-27082.74654; 7,-32366.52792; 8,-39100.60374; 9,-34581.82464;
        10,-27258.65676; 11,-25785.80442; 12,-25415.8515; 13,-24931.6566; 14,-25393.53036;
        15,-26149.71714; 16,-28371.35976; 17,-35629.00254; 18,-48714.62742; 19,
        -59249.40528; 20,-59161.0404; 21,-56793.59538; 22,-52699.33704; 23,-44223.9339;
        24,-36856.7682])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{90,34},{80,44}})));
  EnergySystem.BaseClasses.EnergyDistribution resBlo2(
    lat=weaDat.lat,
    num=1,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_2
      commercialCable,
    A=100000,
    SOC_start=0.5,
    EMax=2e10,
    betCha=0.8,
    thrDis=-1.2e6,
    betDis=0.6,
    thrCha=-6e5)
    annotation (Placement(transformation(extent={{40,20},{14,40}})));
  Modelica.Blocks.Sources.CombiTimeTable nev1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,240; 1,225; 2,200; 3,202; 4,122; 5,81; 6,79; 7,77; 8,34; 9,8; 10,4;
        11,2; 12,1; 13,2; 14,1; 15,0; 16,0; 17,0; 18,5; 19,30; 20,97; 21,103;
        22,216; 23,232; 24,240])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-90,16},{-80,26}})));
  Modelica.Blocks.Sources.CombiTimeTable ns1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,28; 1,30; 2,25; 3,22; 4,42; 5,63; 6,88; 7,139; 8,600; 9,186; 10,
        155; 11,142; 12,123; 13,145; 14,178; 15,140; 16,138; 17,139; 18,151; 19,
        172; 20,135; 21,86; 22,59; 23,33; 24,28])
    "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-90,-6},{-80,4}})));
  Modelica.Blocks.Sources.CombiTimeTable nev3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,6; 1,5; 2,5; 3,5; 4,4; 5,4; 6,6; 7,8; 8,23; 9,75; 10,131; 11,136;
        12,137; 13,137; 14,139; 15,141; 16,141; 17,51; 18,47; 19,17; 20,13; 21,
        11; 22,9; 23,7; 24,6])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{-90,-54},{-80,-44}})));
  Modelica.Blocks.Sources.CombiTimeTable ns3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,41; 1,30; 2,37; 3,40; 4,32; 5,33; 6,46; 7,36; 8,76; 9,89; 10,106;
        11,121; 12,135; 13,126; 14,158; 15,150; 16,161; 17,233; 18,1106; 19,289;
        20,204; 21,113; 22,100; 23,73; 24,41])
    "Number of packages sent for a commercial building block"
    annotation (Placement(transformation(extent={{-90,-72},{-80,-62}})));
  Modelica.Blocks.Sources.CombiTimeTable nev2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,240; 1,224; 2,202; 3,204; 4,124; 5,83; 6,78; 7,72; 8,32; 9,6; 10,3;
        11,2; 12,2; 13,1; 14,0; 15,0; 16,1; 17,3; 18,5; 19,29; 20,104; 21,110;
        22,230; 23,235; 24,240])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{90,16},{80,26}})));
  Modelica.Blocks.Sources.CombiTimeTable ns2(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,34; 1,24; 2,20; 3,18; 4,42; 5,82; 6,112; 7,150; 8,624; 9,165; 10,
        147; 11,156; 12,157; 13,158; 14,184; 15,131; 16,93; 17,178; 18,166; 19,
        169; 20,135; 21,87; 22,72; 23,34; 24,34])
    "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{90,-4},{80,6}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin3(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,-9},{-10,9}},
        rotation=180,
        origin={-50,33})));

  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-8,52})));

  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-66,12})));

equation
  connect(weaDat.weaBus, resBlo1.weaBus) annotation (Line(
      points={{-70,90},{-40,90},{-40,39.8},{-31,39.8}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-70,90},{-40,90},{-40,-4.2},{-11,-4.2}},
      color={255,204,51},
      thickness=0.5));
  connect(PBui2.y[1], resBlo2.PBui[10]) annotation (Line(
      points={{79.5,39},{54,39},{54,30},{42,30},{42,29.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui3.y[1], comBlo.PBui[10]) annotation (Line(
      points={{-79.5,-31},{-44,-31},{-44,-14},{-12,-14},{-12,-14.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(weaDat.weaBus, resBlo2.weaBus) annotation (Line(
      points={{-70,90},{46,90},{46,39.8},{41,39.8}},
      color={255,204,51},
      thickness=0.5));
  connect(nev1.y[1], resBlo1.numEV) annotation (Line(
      points={{-79.5,21},{-44,21},{-44,25.4},{-32,25.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(nev3.y[1], comBlo.numEV) annotation (Line(
      points={{-79.5,-49},{-22,-49},{-22,-18.6},{-12,-18.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ns3.y[1], comBlo.numSenPac[1]) annotation (Line(
      points={{-79.5,-67},{-18,-67},{-18,-22.6},{-12,-22.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ns1.y[1], resBlo1.numSenPac[1]) annotation (Line(
      points={{-79.5,-1},{-38,-1},{-38,21.4},{-32,21.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo2.numEV, nev2.y[1]) annotation (Line(
      points={{42,25.4},{52,25.4},{52,21},{79.5,21}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ns2.y[1], resBlo2.numSenPac[1]) annotation (Line(
      points={{79.5,1},{50,1},{50,21.4},{42,21.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[1]) annotation (Line(
      points={{-79.5,39},{-72.75,39},{-72.75,29.4},{-32,29.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(lin3.terminal_p, resBlo1.term_p)
    annotation (Line(points={{-40,33},{-31,33}}, color={0,120,120}));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-80,60},{-80,52},{-18,52}},
      color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_p, resBlo2.term_p) annotation (Line(
      points={{2,52},{50,52},{50,33},{41,33}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin3.terminal_n) annotation (Line(points={{-80,60},{-80,
          52},{-66,52},{-66,33},{-60,33}}, color={0,120,120}));
  connect(gri.terminal, lin1.terminal_n) annotation (Line(
      points={{-80,60},{-80,52},{-66,52},{-66,22}},
      color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_p, comBlo.term_p) annotation (Line(
      points={{-66,2},{-66,-11},{-11,-11}},
      color={0,120,120},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file=
          "Resources/Scripts/CaseStudy/CaseIEnergySystem/ConnectedCommunitires_Energy.mos"
        "Simulate and Plot"));
end ConnectedCommunities_Energy;
