within MultiInfrastructure.CaseStudy.Figure;
model ConnectedCommunities_ThreeCoupled
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  EnergyTransportationCommunicationSystem.BaseClasses.ThreeCoupled resBlo1(
    lat=weaDat.lat,
    numEV=800,
    A=100000,
    SOC_start=0.5,
    EMax=2e10,
    betDis=0.7,
    betCha=0.7,
    thrDis=-1.2e6,
    thrCha=-7e5)
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  EnergyTransportationCommunicationSystem.BaseClasses.ThreeCoupled resBlo2(
    lat=weaDat.lat,
    numEV=800,
    betDis=0.6,
    betCha=0.8,
    thrCha=-6e5,
    A=100000,
    SOC_start=0.5,
    EMax=2e10,
    thrDis=-1.2e6)
    annotation (Placement(transformation(extent={{60,40},{40,60}})));
  EnergyTransportationCommunicationSystem.BaseClasses.ThreeCoupled comBlo(
    lat=weaDat.lat,
    numEV=200,
    betDis=0.3,
    betCha=0.9,
    thrCha=-3.5e5,
    A=150000,
    SOC_start=0.5,
    EMax=2e10,
    thrDis=-1.2e6)
    annotation (Placement(transformation(extent={{-10,-50},{10,-70}})));
  EnergyTransportationCommunicationSystem.BaseClasses.RoadVariableDelayCom roa1(
     redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=8000) annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={0,32})));
  EnergyTransportationCommunicationSystem.BaseClasses.RoadVariableDelayCom roa2(
     redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=8000) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={0,24})));
  EnergyTransportationCommunicationSystem.BaseClasses.RoadVariableDelayCom roa3(
     redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=10000) annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-44,-8})));
  EnergyTransportationCommunicationSystem.BaseClasses.RoadVariableDelayCom roa4(
     redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=10000) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-56,-8})));
  EnergyTransportationCommunicationSystem.BaseClasses.RoadVariableDelayCom roa5(
     redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=9000) annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={44,-6})));
  EnergyTransportationCommunicationSystem.BaseClasses.RoadVariableDelayCom roa6(
     redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=9000) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={56,-6})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false, filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-120,100},{-100,120}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-110,90})));
  Modelica.Blocks.Sources.CombiTimeTable PBui1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-31642.24665; 1,-25737.65655; 2,-23374.2521; 3,-22685.2667; 4,-22501.96805;
        5,-22001.6213; 6,-23525.33505; 7,-28600.1871; 8,-33781.5394; 9,-29866.28095;
        10,-23544.42185; 11,-22349.86555; 12,-21847.5527; 13,-21501.7796; 14,-21735.9029;
        15,-22513.4214; 16,-24298.4994; 17,-30480.0999; 18,-41825.40685; 19,-50450.39685;
        20,-50320.518; 21,-48195.64265; 22,-44804.9175; 23,-37865.276; 24,-31642.24665])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-120,44},{-110,54}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui7(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-15106.99921; 1,-15106.99921; 2,-15106.99921; 3,-15162.01319; 4,-15190.36283;
        5,-15292.58251; 6,-15288.13499; 7,-22916.16308; 8,-22530.14638; 9,-32344.27609;
        10,-48736.55298; 11,-48736.55298; 12,-48736.55298; 13,-48736.55298; 14,-48736.55298;
        15,-48736.55298; 16,-48736.55298; 17,-54479.05298; 18,-60221.55298; 19,-48376.76377;
        20,-48376.76377; 21,-43829.27609; 22,-22377.64918; 23,-15106.99921; 24,-15106.99921])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-120,-48},{-110,-38}})));
  Modelica.Blocks.Sources.CombiTimeTable PBui4(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-36856.7682; 1,-30109.66872; 2,-26911.8066; 3,-25905.7908; 4,-25780.80756;
        5,-25155.5178; 6,-27082.74654; 7,-32366.52792; 8,-39100.60374; 9,-34581.82464;
        10,-27258.65676; 11,-25785.80442; 12,-25415.8515; 13,-24931.6566; 14,-25393.53036;
        15,-26149.71714; 16,-28371.35976; 17,-35629.00254; 18,-48714.62742; 19,
        -59249.40528; 20,-59161.0404; 21,-56793.59538; 22,-52699.33704; 23,-44223.9339;
        24,-36856.7682])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{110,44},{100,54}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,16; 1,14; 2,18; 3,10; 4,15; 5,43; 6,57; 7,91; 8,542; 9,124; 10,82;
        11,59; 12,55; 13,65; 14,82; 15,75; 16,77; 17,58; 18,46; 19,70; 20,58; 21,
        21; 22,16; 23,15; 24,16]) "Traffic outflow for residential block1"
    annotation (Placement(transformation(extent={{-120,26},{-110,36}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes1(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.3; 1,0.28; 2,0.25; 3,0.25; 4,0.15; 5,0.1; 6,0.1; 7,0.1; 8,0.05; 9,
        0.05; 10,0.05; 11,0.05; 12,0.05; 13,0.05; 14,0.05; 15,0.05; 16,0.05; 17,
        0.05; 18,0.05; 19,0.05; 20,0.15; 21,0.15; 22,0.3; 23,0.3; 24,0.3])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-120,6},{-110,16}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut3(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,18; 1,10; 2,5; 3,8; 4,18; 5,54; 6,72; 7,108; 8,576; 9,109; 10,79; 11,
        80; 12,81; 13,85; 14,97; 15,72; 16,38; 17,49; 18,65; 19,74; 20,65; 21,28;
        22,15; 23,18; 24,18]) "Traffic outflow for residential block1"
    annotation (Placement(transformation(extent={{110,26},{100,36}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes2(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.3; 1,0.28; 2,0.25; 3,0.25; 4,0.15; 5,0.1; 6,0.1; 7,0.1; 8,0.05; 9,
        0.05; 10,0.05; 11,0.05; 12,0.05; 13,0.05; 14,0.05; 15,0.05; 16,0.05; 17,
        0.05; 18,0.05; 19,0.05; 20,0.15; 21,0.15; 22,0.3; 23,0.3; 24,0.3])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{110,6},{100,16}})));
  Modelica.Blocks.Sources.CombiTimeTable qOut5(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,18; 1,13; 2,16; 3,18; 4,14; 5,19; 6,26; 7,16; 8,35; 9,41; 10,49; 11,
        55; 12,63; 13,57; 14,69; 15,79; 16,75; 17,108; 18,551; 19,124; 20,106; 21,
        58; 22,54; 23,42; 24,18]) "Traffic outflow for residential block1"
    annotation (Placement(transformation(extent={{-120,-68},{-110,-58}})));
  Modelica.Blocks.Sources.CombiTimeTable proRes3(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.03; 1,0.03; 2,0.03; 3,0.03; 4,0.03; 5,0.03; 6,0.03; 7,0.03; 8,0.05;
        9,0.05; 10,0.08; 11,0.08; 12,0.08; 13,0.08; 14,0.08; 15,0.08; 16,0.08; 17,
        0.03; 18,0.03; 19,0.03; 20,0.03; 21,0.03; 22,0.03; 23,0.03; 24,0.03])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-120,-88},{-110,-78}})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra2(
    num=1,
    kc={0.03},
    cc={360}) annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=90,
        origin={-78,-30})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra3(kc={
        0.03}, cc={400}) annotation (Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=90,
        origin={-36,18})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra4(
    num=1,
    kc={0.03},
    cc={400}) annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=90,
        origin={-32,18})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra5(kc={
        0.03}, cc={360}) annotation (Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=90,
        origin={36,14})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra6(
    num=1,
    kc={0.03},
    cc={360}) annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=90,
        origin={32,14})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra7(kc={
        0.03}, cc={450}) annotation (Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=90,
        origin={82,-44})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra8(
    num=1,
    kc={0.03},
    cc={450}) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={78,-44})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra1(kc={
        0.03}, cc={360}) annotation (Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=90,
        origin={-82,-30})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra9(kc={
        0.03}, cc={380}) annotation (Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=0,
        origin={0,70})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra10(
    num=1,
    kc={0.03},
    cc={380}) annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=0,
        origin={0,66})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra11(kc={
        0.03}, cc={360}) annotation (Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=0,
        origin={0,8})));
  EnergyTransportationCommunicationSystem.BaseClasses.Transmission tra12(
    num=1,
    kc={0.03},
    cc={360}) annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=0,
        origin={0,4})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={0,74})));

  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-86,-10})));

  Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin3(
    V_nominal=V_nominal,
    P_nominal=PLoa_nominal,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
    l=l,
    redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic
      commercialCable=commercialCable) "Line from or to grid" annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-94,56})));

equation
  connect(resBlo1.qOut[1], roa1.qIn)
    annotation (Line(points={{-44,38},{-44,32},{-12,32}}, color={28,108,200},
      thickness=1));
  connect(roa1.qOut, resBlo2.qIn[1])
    annotation (Line(points={{11,32},{56,32},{56,38}}, color={28,108,200},
      thickness=1));
  connect(resBlo1.qOut[2], roa3.qIn)
    annotation (Line(points={{-44,38},{-44,4}},         color={28,108,200},
      thickness=1));
  connect(resBlo2.qOut[1], roa2.qIn)
    annotation (Line(points={{44,38},{44,24},{12,24}}, color={28,108,200},
      thickness=1));
  connect(roa2.qOut, resBlo1.qIn[1])
    annotation (Line(points={{-11,24},{-56,24},{-56,38}}, color={28,108,200},
      thickness=1));
  connect(resBlo2.qOut[2], roa5.qIn)
    annotation (Line(points={{44,38},{44,6}},        color={28,108,200},
      thickness=1));
  connect(roa5.qOut, comBlo.qIn[2]) annotation (Line(points={{44,-17},{44,-32},
          {-6,-32},{-6,-48}}, color={28,108,200},
      thickness=1));
  connect(roa3.qOut, comBlo.qIn[1]) annotation (Line(
      points={{-44,-19},{-44,-32},{-6,-32},{-6,-48}},
      color={28,108,200},
      thickness=1));
  connect(roa6.qOut, resBlo2.qIn[2]) annotation (Line(
      points={{56,5},{56,38}},
      color={28,108,200},
      thickness=1));
  connect(comBlo.qOut[1], roa4.qIn) annotation (Line(
      points={{6,-48},{6,-40},{-56,-40},{-56,-20}},
      color={28,108,200},
      thickness=1));
  connect(roa4.qOut, resBlo1.qIn[2]) annotation (Line(
      points={{-56,3},{-56,38}},
      color={28,108,200},
      thickness=1));
  connect(comBlo.qOut[2], roa6.qIn) annotation (Line(
      points={{6,-48},{6,-40},{56,-40},{56,-18}},
      color={28,108,200},
      thickness=1));
  connect(weaDat.weaBus, resBlo1.weaBus) annotation (Line(
      points={{-100,110},{-72,110},{-72,58.6},{-61,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, resBlo2.weaBus) annotation (Line(
      points={{-100,110},{80,110},{80,58.6},{61,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-100,110},{-72,110},{-72,-68.6},{-11,-68.6}},
      color={255,204,51},
      thickness=0.5));
  connect(PBui1.y[1], resBlo1.PBui[10]) annotation (Line(
      points={{-109.5,49},{-98,49},{-98,52},{-62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[11]) annotation (Line(
      points={{-109.5,49},{-98,49},{-98,52},{-62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[12]) annotation (Line(
      points={{-109.5,49},{-98,49},{-98,52},{-62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui7.y[1], comBlo.PBui[10]) annotation (Line(
      points={{-109.5,-43},{-82,-43},{-82,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui7.y[1], comBlo.PBui[11]) annotation (Line(
      points={{-109.5,-43},{-82,-43},{-82,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui7.y[1], comBlo.PBui[12]) annotation (Line(
      points={{-109.5,-43},{-82,-43},{-82,-62},{-12,-62}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui1.y[1], resBlo1.PBui[1]) annotation (Line(
      points={{-109.5,49},{-98,49},{-98,52},{-62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui4.y[1], resBlo2.PBui[1]) annotation (Line(points={{99.5,49},{76,
          49},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui4.y[1], resBlo2.PBui[12]) annotation (Line(points={{99.5,49},{76,
          49},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui4.y[1], resBlo2.PBui[11]) annotation (Line(points={{99.5,49},{76,
          49},{76,52},{62,52}},
                            color={0,0,127}));
  connect(PBui4.y[1], resBlo2.PBui[10]) annotation (Line(
      points={{99.5,49},{76,49},{76,52},{62,52}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut1.y[1], resBlo1.qOutSet[2]) annotation (Line(
      points={{-109.5,31},{-98,31},{-98,45.6},{-62,45.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes1.y[1], resBlo1.proEV) annotation (Line(
      points={{-109.5,11},{-98,11},{-98,40.6},{-62,40.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut3.y[1], resBlo2.qOutSet[2]) annotation (Line(points={{99.5,31},{
          70,31},{70,45.6},{62,45.6}},
                               color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes2.y[1], resBlo2.proEV) annotation (Line(
      points={{99.5,11},{70,11},{70,40.6},{62,40.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(tra3.numRecPac[1], roa3.numRecPac) annotation (Line(
      points={{-36,11.4},{-36,3}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra4.numRecPac[1], resBlo1.numRecPac[2]) annotation (Line(
      points={{-32,24.6},{-32,68},{-68,68},{-68,48},{-62,48}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(resBlo1.numSenPac[2], tra3.numSenPac[1]) annotation (Line(
      points={{-39,48},{-36,48},{-36,24.6}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa3.numSenPac, tra4.numSenPac[1]) annotation (Line(
      points={{-36,-19},{-36,-22},{-32,-22},{-32,11.4}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(qOut1.y[1], roa3.numPacSet) annotation (Line(
      points={{-109.5,31},{-47.2,31},{-47.2,3}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(roa4.numSenPac, tra1.numSenPac[1]) annotation (Line(
      points={{-64,3},{-64,6},{-82,6},{-82,-23.4}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra1.numRecPac[1], comBlo.numRecPac[1]) annotation (Line(
      points={{-82,-36.6},{-82,-58},{-12,-58}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(comBlo.numSenPac[1], tra2.numSenPac[1]) annotation (Line(
      points={{11,-58},{20,-58},{20,-48},{-78,-48},{-78,-36.6}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra2.numRecPac[1], roa4.numRecPac) annotation (Line(
      points={{-78,-23.4},{-78,-22},{-64,-22},{-64,-19}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa6.numSenPac, tra7.numSenPac[1]) annotation (Line(
      points={{64,5},{64,8},{82,8},{82,-37.4}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra7.numRecPac[1], comBlo.numRecPac[2]) annotation (Line(
      points={{82,-50.6},{82,-80},{-36,-80},{-36,-58},{-12,-58}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(comBlo.numSenPac[2], tra8.numSenPac[1]) annotation (Line(
      points={{11,-58},{78,-58},{78,-50.6}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra8.numRecPac[1], roa6.numRecPac) annotation (Line(
      points={{78,-37.4},{78,-24},{64,-24},{64,-17}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa5.numSenPac, tra6.numSenPac[1]) annotation (Line(
      points={{36,-17},{36,-22},{32,-22},{32,7.4}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra6.numRecPac[1], resBlo2.numRecPac[2]) annotation (Line(
      points={{32,20.6},{32,68},{66,68},{66,48},{62,48}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(resBlo2.numSenPac[2], tra5.numSenPac[1]) annotation (Line(
      points={{39,48},{36,48},{36,20.6}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra5.numRecPac[1], roa5.numRecPac) annotation (Line(
      points={{36,7.4},{36,5}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa1.numSenPac, tra9.numSenPac[1]) annotation (Line(
      points={{11,40},{18,40},{18,70},{6.6,70}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra9.numRecPac[1], resBlo1.numRecPac[1]) annotation (Line(
      points={{-6.6,70},{-70,70},{-70,48},{-62,48}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(resBlo1.numSenPac[1], tra10.numSenPac[1]) annotation (Line(
      points={{-39,48},{-36,48},{-36,66},{-6.6,66}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra10.numRecPac[1], roa1.numRecPac) annotation (Line(
      points={{6.6,66},{14,66},{14,44},{-18,44},{-18,40},{-11,40}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa2.numSenPac, tra12.numSenPac[1]) annotation (Line(
      points={{-11,16},{-18,16},{-18,4},{-6.6,4}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra12.numRecPac[1], resBlo2.numRecPac[1]) annotation (Line(
      points={{6.6,4},{24,4},{24,34},{66,34},{66,48},{62,48}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(resBlo2.numSenPac[1], tra11.numSenPac[1]) annotation (Line(
      points={{39,48},{22,48},{22,8},{6.6,8}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra11.numRecPac[1], roa2.numRecPac) annotation (Line(
      points={{-6.6,8},{-12,8},{-12,12},{16,12},{16,16},{11,16}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(qOut3.y[1], roa5.numPacSet) annotation (Line(
      points={{99.5,31},{47.2,31},{47.2,5}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut5.y[1], roa4.numPacSet) annotation (Line(
      points={{-109.5,-63},{-52.8,-63},{-52.8,-19}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut1.y[1], roa1.numPacSet) annotation (Line(
      points={{-109.5,31},{-98,31},{-98,28.8},{-11,28.8}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut3.y[1], roa2.numPacSet) annotation (Line(
      points={{99.5,31},{70,31},{70,27.2},{11,27.2}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut5.y[1], roa6.numPacSet) annotation (Line(
      points={{-109.5,-63},{-42,-63},{-42,-36},{52.8,-36},{52.8,-17}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes3.y[1], comBlo.proEV) annotation (Line(
      points={{-109.5,-83},{-42,-83},{-42,-50.6},{-12,-50.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOut5.y[1], comBlo.qOutSet[1]) annotation (Line(
      points={{-109.5,-63},{-32,-63},{-32,-55.6},{-12,-55.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-110,80},{-110,74},{-10,74}},
      color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_p, resBlo2.term_p) annotation (Line(
      points={{10,74},{72,74},{72,56.2},{61,56.2}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin1.terminal_n) annotation (Line(
      points={{-110,80},{-110,74},{-86,74},{-86,0}},
      color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_p, comBlo.term_p) annotation (Line(
      points={{-86,-20},{-86,-66},{-11,-66},{-11,-66.2}},
      color={0,120,120},
      thickness=0.5));
  connect(lin3.terminal_p, resBlo1.term_p) annotation (Line(
      points={{-84,56},{-84,56.2},{-61,56.2}},
      color={0,120,120},
      thickness=0.5));
  connect(lin3.terminal_n, gri.terminal) annotation (Line(
      points={{-104,56},{-110,56},{-110,80}},
      color={0,120,120},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
            -100},{120,120}})),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{120,
            120}})));
end ConnectedCommunities_ThreeCoupled;
