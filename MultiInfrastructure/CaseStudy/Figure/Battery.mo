within MultiInfrastructure.CaseStudy.Figure;
model Battery "Schematics for battery"
public
  Modelica.Blocks.Sources.BooleanExpression batMod1(y=batCon.y == 1)
    "See if it is standby (u1)"
    annotation (Placement(transformation(extent={{-74,40},{-54,60}})));
  Modelica.Blocks.Sources.Constant PSta(k=0) "Battery standby power"
    annotation (Placement(transformation(extent={{-66,62},{-56,72}})));
public
  Modelica.Blocks.Sources.BooleanExpression batMod2(y=batCon.y == 2)
    "See if it is discharging (u1)"
    annotation (Placement(transformation(extent={{-74,26},{-54,46}})));
protected
  Modelica.Blocks.Logical.Switch swi2 "Switch " annotation (Placement(
        transformation(extent={{-6,-6},{6,6}}, origin={-38,6})));
protected
  Modelica.Blocks.Logical.Switch swi1 "Switch " annotation (Placement(
        transformation(extent={{-6,-6},{6,6}}, origin={-18,26})));
public
  Buildings.Electrical.AC.ThreePhasesBalanced.Storage.Battery batBan(
    V_nominal=V_nominal,
    etaDis=1,
    etaCha=1,
    eta_DCAC=1,
    SOC_start=SOC_start,
    EMax=EMax,
    betDis=betDis,
    betCha=betCha)
                "Battery bank (Energy storage)"
    annotation (Placement(transformation(extent={{-18,-18},{2,2}})));
  IndividualSystem.Energy.BaseClasses.Control1 batCon(
    tWai=0,
    thrDis=thrDis,
    thrCha=thrCha)
    annotation (Placement(transformation(extent={{-34,-30},{-24,-20}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{-64,-24},{-54,-14}})));
  Modelica.Blocks.Sources.RealExpression PowerCha(y=add.y - batCon.thrCha)
    annotation (Placement(transformation(extent={{-88,8},{-68,28}})));
  Modelica.Blocks.Sources.RealExpression PowerDis(y=add.y - batCon.thrDis)
    annotation (Placement(transformation(extent={{-88,-12},{-68,8}})));
  Modelica.Blocks.Interfaces.RealInput PSup "Connector of Real input signal 1"
    annotation (Placement(transformation(extent={{-120,-32},{-100,-12}})));
  Modelica.Blocks.Interfaces.RealInput PDem "Connector of Real input signal 2"
    annotation (Placement(transformation(extent={{-120,-46},{-100,-26}}),
        iconTransformation(extent={{-140,-66},{-100,-26}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
    annotation (Placement(transformation(extent={{-116,-16},{-100,0}}),
        iconTransformation(extent={{-122,-22},{-102,-2}})));
equation
  connect(batMod1.y,swi1. u2)
    annotation (Line(points={{-53,50},{-38,50},{-38,26},{-25.2,26}},
                                                 color={255,0,255}));
  connect(PSta.y,swi1. u1) annotation (Line(points={{-55.5,67},{-46,67},{-46,
          30.8},{-25.2,30.8}},
                        color={0,0,127}));
  connect(swi2.u2,batMod2. y) annotation (Line(points={{-45.2,6},{-50,6},{-50,
          36},{-53,36}},
                    color={255,0,255}));
  connect(swi1.y,batBan. P)
    annotation (Line(points={{-11.4,26},{-8,26},{-8,2}}, color={0,0,127}));
  connect(batBan.SOC,batCon. SOC) annotation (Line(points={{3,-2},{10,-2},{10,
          -34},{-42,-34},{-42,-27.2},{-33.5,-27.2}},
                                            color={0,0,127}));
  connect(add.y, batCon.P) annotation (Line(points={{-53.5,-19},{-42,-19},{-42,
          -23.2},{-33.7,-23.2}}, color={0,0,127}));
  connect(swi2.y,swi1. u3) annotation (Line(points={{-31.4,6},{-30,6},{-30,21.2},
          {-25.2,21.2}}, color={0,0,127}));
  connect(PowerCha.y,swi2. u1) annotation (Line(points={{-67,18},{-58,18},{-58,
          10.8},{-45.2,10.8}},
                        color={0,0,127}));
  connect(PowerDis.y,swi2. u3) annotation (Line(points={{-67,-2},{-54,-2},{-54,
          1.2},{-45.2,1.2}},   color={0,0,127}));
  connect(add.u1, PSup) annotation (Line(points={{-65,-16},{-88,-16},{-88,-22},
          {-110,-22}}, color={0,0,127}));
  connect(add.u2, PDem) annotation (Line(points={{-65,-22},{-72,-22},{-72,-36},
          {-110,-36}}, color={0,0,127}));
  connect(batBan.terminal, term_p)
    annotation (Line(points={{-18,-8},{-108,-8}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Bitmap(extent={{-98,-68},{98,92}}, fileName=
              "modelica://MultiInfrastructure/Resources/Images/Figure/Battery.png")}),
      Diagram(coordinateSystem(preserveAspectRatio=false)));
end Battery;
