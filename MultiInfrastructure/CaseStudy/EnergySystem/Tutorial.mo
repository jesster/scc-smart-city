within MultiInfrastructure.CaseStudy.EnergySystem;
model Tutorial "Tutorial to demonstrate Energy System"
  extends Modelica.Icons.Example;
  import ModelicaServices;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
      computeWetBulbTemperature=false, filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-100,70},{-80,90}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-90,50})));

  MultiInfrastructure.CaseStudy.EnergySystem.BaseClasses.EnergyDistribution resBlo(
    lat=weaDat.lat,
    num=1,
    SOC_start=0.5,
    thrDis=-1.2e6,
    thrCha=-7e5,
    A=20000,
    betDis=0.5,
    betCha=1,
    EMax=1.8e10,
    redeclare
      Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_10
      commercialCable) "Residential block"
    annotation (Placement(transformation(extent={{-20,0},{6,20}})));
  Modelica.Blocks.Sources.CombiTimeTable nEv(
    tableOnFile=true,
    table=[0,255; 1,239; 2,213; 3,214; 4,129; 5,86; 6,84; 7,82; 8,37; 9,11; 10,
        6; 11,4; 12,4; 13,5; 14,4; 15,3; 16,3; 17,2; 18,7; 19,32; 20,105; 21,
        111; 22,231; 23,247; 24,255],
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergySystem/Tutorial/nEv.txt"),
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600)
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,-40},{-80,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable numPacSen(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergySystem/Tutorial/numPacSen.txt"),
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,28; 1,30; 2,25; 3,22; 4,42; 5,63; 6,88; 7,139; 8,600; 9,186; 10,
        155; 11,142; 12,123; 13,145; 14,178; 15,140; 16,138; 17,139; 18,151; 19,
        172; 20,135; 21,86; 22,59; 23,33; 24,28])
    "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-100,-80},{-80,-60}})));
  Modelica.Blocks.Sources.CombiTimeTable powBuiAllRes(
    tableOnFile=true,
    table=[0,-31642.24665,-65489.46691,-93547.60327; 1,-25737.65655,-52579.71872,
        -81765.20272; 2,-23374.2521,-46793.20889,-71653.916; 3,-22685.2667,-44550.76517,
        -67617.96395; 4,-22501.96805,-44086.60396,-66564.75353; 5,-22001.6213,-44036.65506,
        -65594.14602; 6,-23525.33505,-46260.73703,-67298.99476; 7,-28600.1871,-57901.44167,
        -83343.95152; 8,-33781.5394,-72613.09332,-104325.7667; 9,-29866.28095,-69900.28072,
        -98738.47411; 10,-23544.42185,-59006.96062,-79365.80039; 11,-22349.86555,
        -58092.84249,-74866.26255; 12,-21847.5527,-56041.65098,-72194.08975; 13,
        -21501.7796,-53937.63598,-69795.18078; 14,-21735.9029,-52003.87518,-68179.44484;
        15,-22513.4214,-51703.49376,-67469.89108; 16,-24298.4994,-55460.0902,-73082.1226;
        17,-30480.0999,-69523.23761,-94165.77606; 18,-41825.40685,-96347.78504,
        -133049.0548; 19,-50450.39685,-112934.2919,-161614.4816; 20,-50320.518,
        -111558.4958,-162519.0552; 21,-48195.64265,-104309.0771,-151231.1099;
        22,-44804.9175,-96400.3119,-139018.1881; 23,-37865.276,-80807.46526,-116633.5399;
        24,-31642.24665,-65489.46691,-93547.60327],
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://MultiInfrastructure/Resources/Input/CaseStudy/EnergySystem/Tutorial/powBuiAllRes.txt"),
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600)
    "Power profile for all of the buildings in the residential building block"
    annotation (Placement(transformation(extent={{-100,0},{-80,20}})));

equation
  connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,19.8},{-21,19.8}},
      color={255,204,51},
      thickness=0.5));
  connect(gri.terminal, resBlo.term_p) annotation (Line(points={{-90,40},{-90,
          28},{-70,28},{-70,14},{-46,14},{-46,13},{-21,13}},
                                             color={0,120,120}));
  connect(numPacSen.y[1], resBlo.numSenPac[1]) annotation (Line(points={{-79,-70},
          {-40,-70},{-40,1.4},{-22,1.4}},        color={0,0,127}));
  connect(nEv.y[1], resBlo.numEV) annotation (Line(points={{-79,-30},{-50,-30},
          {-50,5.4},{-22,5.4}},   color={0,0,127}));
  for i in {1,10,11,12} loop
    connect(powBuiAllRes.y[1], resBlo.PBui[i]);
  end for;
  for i in {2,3,6,7,8} loop
    connect(powBuiAllRes.y[2], resBlo.PBui[i]) annotation (Line(points={{-79,10},
            {-60,10},{-60,9.4},{-22,9.4}},         color={0,0,127}));
  end for;
  for i in {4,5,9} loop
    connect(powBuiAllRes.y[3], resBlo.PBui[i]);
  end for;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
            -100},{100,100}})),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}})),
             __Dymola_Commands(file="modelica://MultiInfrastructure/Resources/Scripts/CaseStudy/EnergySystem/Tutorial/Tutorial.mos"
        "Simulate and plot"),
    experiment(StopTime=86500,
     __Dymola_Algorithm="Cvode",
     Tolerance=1e-4));
end Tutorial;
