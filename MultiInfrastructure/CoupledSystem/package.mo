within MultiInfrastructure;
package CoupledSystem "Collection of models that couple the energy system, energy system and communication system"

annotation (Documentation(info="<html>
<p>This package contains models for coupled systems.</p>
</html>"));
end CoupledSystem;
