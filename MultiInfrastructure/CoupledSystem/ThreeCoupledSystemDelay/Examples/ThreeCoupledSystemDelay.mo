within MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.Examples;
model ThreeCoupledSystemDelay "Example that demonstrates the use of models in 
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.Block resBlo(
    V_nominal=V_nominal,
    f=f,
    lat=weaDat.lat,
    num=1,
    numEV=1500)
    annotation (Placement(transformation(extent={{-8,40},{12,60}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false,
    filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Blocks.Sources.CombiTimeTable powRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000; 12,
        -200000; 14,-300000; 16,-600000; 18,-800000; 20,-1000000; 22,-800000; 24,
        -600000]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,46},{-90,56}})));
  Modelica.Blocks.Sources.CombiTimeTable qRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,15; 1,21; 2,6; 3,9; 4,24; 5,30; 6,240; 7,360; 8,580; 9,240; 10,150;
        11,88; 12,60; 13,60; 14,90; 15,90; 16,60; 17,90; 18,90; 19,60; 20,90;
        21,30; 22,15; 23,0; 24,15])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,32},{-90,42}})));
   MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=270,
        origin={-90,0})));
   MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    l=2000,
    P_nominal=2000000,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.automatic)
    "Line from or to grid"
    annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-60,20})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    redeclare replaceable
  MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1500
      commercialCable,
    l=2000,
    P_nominal=2000000,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial)
    "Line from or to grid"
    annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-60,-20})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.Block comBlo(
    V_nominal=V_nominal,
    f=f,
    lat=weaDat.lat,
    num=1,
    numEV=200)
    annotation (Placement(transformation(extent={{10,-52},{-10,-32}})));
  Modelica.Blocks.Sources.CombiTimeTable powCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-50000; 2,-30000; 4,-50000; 6,-100000; 8,-500000; 10,-800000; 12,-1100000;
        14,-1500000; 16,-1400000; 18,-1000000; 20,-700000; 22,-300000; 24,-50000])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{90,-46},{80,-36}})));
  Modelica.Blocks.Sources.CombiTimeTable qCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,15; 1,0; 2,0; 3,0; 4,24; 5,30; 6,60; 7,60; 8,90; 9,90; 10,45; 11,
        90; 12,57; 13,61; 14,90; 15,90; 16,150; 17,275; 18,601; 19,325; 20,180;
        21,85; 22,35; 23,45; 24,15])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{90,-62},{80,-52}})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.BaseClasses.Transmission
    tra1(kc={0.03}, cc={360})
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-34,-28})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.BaseClasses.Transmission
    tra2(
    num=1,
    kc={0.03},
    cc={360})
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-26,-28})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.BaseClasses.Transmission
    tra4(
    num=1,
    kc={0.03},
    cc={400})
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={34,26})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.BaseClasses.Transmission
    tra3(kc={0.03}, cc={400})
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={26,26})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.BaseClasses.RoadVariableDelayCom roa1(l=1200, redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-18,0})));
  Modelica.Blocks.Sources.CombiTimeTable proRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.9; 1,0.9; 2,0.9; 3,0.9; 4,0.8; 5,0.7; 6,0.5; 7,0.5; 8,0.3; 9,0.4;
        10,0.35; 11,0.45; 12,0.5; 13,0.45; 14,0.4; 15,0.6; 16,0.55; 17,0.7; 18,
        0.65; 19,0.65; 20,0.75; 21,0.8; 22,0.8; 23,0.8; 24,0.9])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-100,18},{-90,28}})));
  Modelica.Blocks.Sources.CombiTimeTable proCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.6; 1,0.5; 2,0.55; 3,0.5; 4,0.5; 5,0.6; 6,0.6; 7,0.6; 8,0.7; 9,
        0.8; 10,0.8; 11,0.7; 12,0.8; 13,0.75; 14,0.8; 15,0.7; 16,0.65; 17,0.65;
        18,0.7; 19,0.7; 20,0.7; 21,0.75; 22,0.7; 23,0.65; 24,0.6])
    "Probability of charging for a single EV at different time in a commercial block"
    annotation (Placement(transformation(extent={{90,-76},{80,-66}})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay.BaseClasses.RoadVariableDelayCom roa2(l=1200, redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.CClassRoad30
      roaTyp)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={18,0})));
equation
  connect(gri.terminal, lin1.terminal_p) annotation (Line(
      points={{-80,-1.77636e-015},{-60,-1.77636e-015},{-60,10}},
      color={0,120,120},
      thickness=0.5));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-80,-1.77636e-015},{-60,-1.77636e-015},{-60,-10}},
      color={0,120,120},
      thickness=0.5));
  connect(lin1.terminal_n, resBlo.term_p) annotation (Line(
      points={{-60,30},{-60,56.2},{-9,56.2}},
      color={0,120,120},
      thickness=0.5));
  connect(comBlo.term_p, lin2.terminal_p) annotation (Line(
      points={{11,-35.8},{36,-35.8},{36,-62},{-60,-62},{-60,-30}},
      color={0,120,120},
      thickness=0.5));
  connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
      points={{-80,90},{-20,90},{-20,58.6},{-9,58.6}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-80,90},{60,90},{60,-34},{10,-34},{10,-34},{10,-34},{10,-33.4},{11,
          -33.4}},
      color={255,204,51},
      thickness=0.5));
  connect(powRes.y[1], resBlo.PBui) annotation (Line(
      points={{-89.5,51},{-10,51}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(powCom.y[1], comBlo.PBui) annotation (Line(
      points={{79.5,-41},{30,-41},{30,-41},{12,-41}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo.numSenPac[1], tra3.numSenPac[1]) annotation (Line(
      points={{13,48},{26,48},{26,37}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra3.numRecPac[1], roa2.numRecPac) annotation (Line(
      points={{26,15},{26,11}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa2.numSenPac, tra4.numSenPac[1]) annotation (Line(
      points={{26,-11},{26,-18},{34,-18},{34,15}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra4.numRecPac[1], resBlo.numRecPac[1]) annotation (Line(
      points={{34,37},{34,68},{-18,68},{-18,48},{-10,48}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(qRes.y[1], roa2.numPacSet) annotation (Line(
      points={{-89.5,37},{14.8,37},{14.8,11}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo.numSenPac[1], tra2.numSenPac[1]) annotation (Line(
      points={{-11,-44},{-26,-44},{-26,-39}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra2.numRecPac[1], roa1.numRecPac) annotation (Line(
      points={{-26,-17},{-26,-11}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa1.numSenPac, tra1.numSenPac[1]) annotation (Line(
      points={{-26,11},{-26,20},{-34,20},{-34,-17}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra1.numRecPac[1], comBlo.numRecPac[1]) annotation (Line(
      points={{-34,-39},{-34,-58},{20,-58},{20,-44},{12,-44}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(qCom.y[1], roa1.numPacSet) annotation (Line(
      points={{79.5,-57},{48,-57},{48,-20},{-14.8,-20},{-14.8,-11}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo.qOut[1], roa2.qIn) annotation (Line(
      points={{13,43},{18,43},{18,12}},
      color={28,108,200},
      thickness=1));
  connect(roa2.qOut, comBlo.qIn[1]) annotation (Line(
      points={{18,-11},{18,-49.2},{12,-49.2}},
      color={28,108,200},
      thickness=1));
  connect(comBlo.qOut[1], roa1.qIn) annotation (Line(
      points={{-11,-49},{-18,-49},{-18,-12}},
      color={28,108,200},
      thickness=1));
  connect(roa1.qOut, resBlo.qIn[1]) annotation (Line(
      points={{-18,11},{-18,42.8},{-10,42.8}},
      color={28,108,200},
      thickness=1));
  connect(proRes.y[1], resBlo.proEV) annotation (Line(
      points={{-89.5,23},{-32,23},{-32,40.6},{-10,40.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proCom.y[1], comBlo.proEV) annotation (Line(
      points={{79.5,-71},{38,-71},{38,-51.4},{12,-51.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qRes.y[1], resBlo.qOutSet[1]) annotation (Line(
      points={{-89.5,37},{-40,37},{-40,45.6},{-10,45.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qCom.y[1], comBlo.qOutSet[1]) annotation (Line(
      points={{79.5,-57},{42,-57},{42,-46.4},{12,-46.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file=
          "Resources/Scripts/CoupledSystem/ThreeCoupledSystemDelay/Examples/ThreeCoupledSystemDelay.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
     <p>This example demonstrates the coupling the three system.The EV charging power 
    consumption is calculated from the transportation system, which is related to the 
    traffic condition between the blocks.Also,the wireless communication power consumption is
    calculated from the communication system.  The wireless communication quality deteriorates
    with the increase of the vehicles on the road while the communication quality impacts on
    the road traffic conditions. The results from this example show the energy,
    transportation and communication conditions.</p>
</html>"));
end ThreeCoupledSystemDelay;
