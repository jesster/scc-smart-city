within MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDelay;
model BlockNew
  "Model that connects the energy,transportation and communication system under a new framework"
  parameter Real numEV(fixed=true, start=numEV)
    "Number of charging EV in the block"
    annotation(Dialog(tab="Transportation",group="Parameters"));
  parameter Integer number=2 "Number of ports for transportation and communication system";
  parameter Integer num "Number of ports for transportation and communication system";
  parameter Modelica.SIunits.Voltage V_nominal=V_nominal
    "Nominal voltage(V_nominal>=0)"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Frequency f=f "Nominal grid frequency"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Angle lat=lat "Latitude"
    annotation(Dialog(tab="Energy",group="Parameters"));
  BaseClasses.EnergyNew                                                        ene(
    V_nominal=V_nominal,
    f=f,
    lat=lat,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    A=600000,
    num=1)   annotation (Placement(transformation(extent={{-10,32},{10,52}})));
  MultiInfrastructure.Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui "Building power load"
    annotation (Placement(transformation(extent={{-140,20},{-100,60}}),
        iconTransformation(extent={{-140,20},{-100,60}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
    annotation (Placement(transformation(extent={{-120,52},{-100,72}}),
        iconTransformation(extent={{-120,52},{-100,72}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-120,76},{-100,96}}), iconTransformation(extent=
           {{-120,76},{-100,96}})));
  Modelica.Blocks.Interfaces.RealInput numRecPac[number]
    "Number of received packets" annotation (Placement(transformation(extent={{-140,
            -20},{-100,20}}), iconTransformation(extent={{-140,-20},{-100,20}})));
  BaseClasses.nPortBlockNew
    com(num=number + 2)
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  Modelica.Blocks.Interfaces.RealOutput numSenPac[number]
    "Number of sent packets"
    annotation (Placement(transformation(extent={{52,-20},{92,20}})));

  Modelica.Blocks.Interfaces.RealInput qOutSet[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,-50},{-100,-10}}),
        iconTransformation(extent={{-140,-50},{-100,-10}})));
  Modelica.Blocks.Interfaces.RealInput qIn[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=90,
        origin={-60,-100})));
  Modelica.Blocks.Interfaces.RealInput proEV
    "Probability of single EV to be charged"
    annotation (Placement(transformation(extent={{-140,-84},{-100,-44}})));
  BaseClasses.TransportationBlockNew                                                        tra(
  num=num, numEV(start=numEV, fixed=true))
    annotation (Placement(transformation(extent={{-20,-70},{0,-50}})));
  Modelica.Blocks.Interfaces.RealOutput qOut [num] "Connector of Real output signal"
    annotation (Placement(transformation(extent={{-19,-19},{19,19}},
        rotation=90,
        origin={41,-81}),
        iconTransformation(extent={{74,-116},{94,-96}})));
equation
  connect(term_p,term_p)  annotation (Line(points={{-110,62},{-110,62},{-110,62},
          {-110,62}}, color={0,120,120}));
  connect(ene.weaBus,weaBus)  annotation (Line(
      points={{-11,51.8},{-40,51.8},{-40,86},{-110,86}},
      color={255,204,51},
      thickness=0.5));
  connect(ene.term_p,term_p)  annotation (Line(points={{-11,42},{-60,42},{-60,
          62},{-110,62}},
                      color={0,120,120}));
  connect(ene.PBui,PBui)  annotation (Line(points={{-12,40},{-120,40}},
                     color={0,0,127}));
  connect(qOutSet,tra. qOutSet) annotation (Line(points={{-120,-30},{-40,-30},{
          -40,-56},{-22,-56}},
                          color={0,0,127}));
  connect(qIn,tra. qIn) annotation (Line(points={{-60,-100},{-60,-60},{-22,-60}},
                 color={0,0,127}));
  connect(proEV,tra. proEV) annotation (Line(points={{-120,-64},{-22,-64}},
                     color={0,0,127}));
  connect(tra.qOut,qOut)  annotation (Line(points={{1,-60},{41,-60},{41,-81}},
                     color={0,0,127}));
  connect(com.numTotPac, ene.numSenPac[1]) annotation (Line(points={{-49,-5},{
          -20,-5},{-20,34},{-12,34}},
                                color={0,0,127}));
  connect(ene.priConSig, com.numRecPac[number+1]) annotation (Line(points={{11,42},
          {20,42},{20,20},{-80,20},{-80,0},{-71,0}},     color={0,0,127}));
  connect(com.numSenPac[number+1], tra.priConSig) annotation (Line(points={{-49,0},
          {-32,0},{-32,-52},{-22,-52}},
                                      color={0,0,127}));
  connect(tra.numEVCha, com.numRecPac[number+2]) annotation (Line(points={{1,-68},
          {20,-68},{20,-20},{-80,-20},{-80,0},{-71,0}},     color={0,0,127}));
  connect(com.numSenPac[number+2], ene.numEV) annotation (Line(points={{-49,0},
          {-26,0},{-26,37},{-12,37}},
                            color={0,0,127}));
  for i in 1:number loop
  connect(com.numSenPac[i], numSenPac[i]);
  end for
    annotation (Line(points={{-5,0},{110,0}}, color={0,0,127}));
  for i in 1:number loop
  connect(com.numRecPac[i], numRecPac[i]);
  end for
    annotation (Line(points={{-5,0},{110,0}}, color={0,0,127}));
  connect(numRecPac, com.numRecPac)
    annotation (Line(points={{-120,0},{-71,0}}, color={0,0,127}));
  connect(com.numSenPac, numSenPac)
    annotation (Line(points={{-49,0},{72,0}}, color={0,0,127}));
   annotation(Dialog(tab="Energy",group="Parameters"),
            Dialog(tab="Energy",group="Parameters"),
              Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Line(points={{-96,-48},{-94,-54},{-92,-60},{-90,-64},{-88,-68},{-86,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-76,-48},{-78,-54},{-80,-60},{-82,-64},{-84,-68},{-86,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-76,-48},{-74,-42},{-72,-36},{-70,-32},{-68,-28},{-66,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-56,-48},{-58,-42},{-60,-36},{-62,-32},{-64,-28},{-66,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-56,-48},{-54,-54},{-52,-60},{-50,-64},{-48,-68},{-46,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-36,-48},{-38,-54},{-40,-60},{-42,-64},{-44,-68},{-46,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-36,-48},{-34,-42},{-32,-36},{-30,-32},{-28,-28},{-26,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-16,-48},{-18,-42},{-20,-36},{-22,-32},{-24,-28},{-26,
              -26}},                                                 color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{-16,-48},{-14,-54},{-12,-60},{-10,-64},{-8,-68},{-6,-70}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{4,-48},{2,-54},{0,-60},{-2,-64},{-4,-68},{-6,-70}}, color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{4,-48},{6,-42},{8,-36},{10,-32},{12,-28},{14,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{24,-48},{22,-42},{20,-36},{18,-32},{16,-28},{14,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name"),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
        Rectangle(
          extent={{-78,20},{72,-24}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={57,20},
          rotation=90),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={57,56},
          rotation=90),
        Line(points={{-62,62},{-36,62}},   color={0,0,0}),
        Line(points={{-50,62},{-50,32}},    color={0,0,0}),
        Line(points={{-62,62},{-62,60}},   color={0,0,0}),
        Line(points={{-36,62},{-36,60}},   color={0,0,0}),
        Line(
          points={{-72,32},{-70,34},{-66,38},{-62,52},{-62,60}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-72,32},{-60,34},{-54,36},{-38,48},{-36,60}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{-26,72},{0,72}},     color={0,0,0}),
        Line(points={{-26,72},{-26,70}},   color={0,0,0}),
        Line(points={{0,72},{0,70}},       color={0,0,0}),
        Line(
          points={{-62,60},{-46,64},{-36,66},{-30,68},{-26,70}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-36,60},{-24,62},{-14,64},{-6,66},{0,70}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-26,70},{-10,74},{0,76},{6,78},{10,80}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{0,70},{12,72},{22,74},{30,76},{36,80}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{10,82},{36,82}},   color={0,0,0}),
        Line(points={{-14,72},{-14,42}},   color={0,0,0}),
        Line(points={{22,82},{22,50}},   color={0,0,0}),
        Line(points={{10,82},{10,80}},     color={0,0,0}),
        Line(points={{36,82},{36,80}},     color={0,0,0}),
        Rectangle(
          extent={{46,60},{68,24}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,60},{62,50}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,42},{62,32}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,26},{62,16}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid), Line(
          points={{-78,-2},{72,-2}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),
        Ellipse(
          extent={{19.5,0.5},{91.5,-71.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{22.5,-2.5},{88.5,-68.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{34.5,-14.5},{76.5,-56.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{37.5,-17.5},{73.5,-53.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{43.5,-23.5},{67.5,-47.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{54,-34},{30,-96},{32,-96},{54,-38},{54,-34}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{48,-50},{62,-52}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{50.5,-30.5},{60,-40}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{56,-34},{80,-96},{78,-96},{56,-38},{56,-34}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{44,-62},{66,-64}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the three agents (energy, transportation and communication) in the block.</p>
</html>"));
end BlockNew;
