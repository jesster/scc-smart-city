within MultiInfrastructure.CoupledSystem;
package TransportationDelayAndCommunication

annotation (Documentation(info="<html>
<p>This package contains models for coupled systems of transportation and communication. </p>
</html>"));
end TransportationDelayAndCommunication;
