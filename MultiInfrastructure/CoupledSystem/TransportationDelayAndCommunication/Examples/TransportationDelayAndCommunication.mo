within MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.Examples;
model TransportationDelayAndCommunication "Example that demonstrates the use of models in 
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication"
  extends Modelica.Icons.Example;
  MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.Block resBlo(
    num=1, numEV=1500)
    annotation (Placement(transformation(extent={{-10,30},{10,50}})));
  MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.Block comBlo(numEV=300)
    annotation (Placement(transformation(extent={{10,-58},{-10,-38}})));
  Modelica.Blocks.Sources.CombiTimeTable qRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,15; 1,21; 2,6; 3,9; 4,24; 5,30; 6,240; 7,360; 8,580; 9,240; 10,150;
        11,88; 12,60; 13,60; 14,90; 15,90; 16,60; 17,90; 18,90; 19,60; 20,90;
        21,30; 22,15; 23,0; 24,15])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-80,32},{-60,52}})));
  Modelica.Blocks.Sources.CombiTimeTable qCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,15; 1,0; 2,0; 3,0; 4,24; 5,30; 6,60; 7,60; 8,90; 9,90; 10,45; 11,
        90; 12,57; 13,61; 14,90; 15,90; 16,150; 17,275; 18,601; 19,325; 20,180;
        21,85; 22,35; 23,45; 24,15])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{80,-66},{60,-46}})));
  MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.BaseClasses.Transmission
    tra4(
    num=1,
    kc={0.03},
    cc={400})
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={36,28})));
  MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.BaseClasses.Transmission
    tra1(kc={0.03}, cc={380})
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,-30})));
  MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.BaseClasses.Transmission
    tra2(
    num=1,
    kc={0.03},
    cc={380})
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-32,-28})));
  MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.BaseClasses.RoadVariableDelayCom roa1(l=1200, redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.CClassRoad30
      roaTyp)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-24,0})));
  MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.BaseClasses.RoadVariableDelayCom roa2(l=1300, redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={22,-4})));
  Modelica.Blocks.Sources.CombiTimeTable proRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.9; 1,0.9; 2,0.9; 3,0.9; 4,0.8; 5,0.7; 6,0.5; 7,0.5; 8,0.3; 9,0.4;
        10,0.35; 11,0.45; 12,0.5; 13,0.45; 14,0.4; 15,0.6; 16,0.55; 17,0.7; 18,
        0.65; 19,0.65; 20,0.75; 21,0.8; 22,0.8; 23,0.8; 24,0.9])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-80,0},{-60,20}})));
  Modelica.Blocks.Sources.CombiTimeTable proCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.6; 1,0.5; 2,0.55; 3,0.5; 4,0.5; 5,0.6; 6,0.6; 7,0.6; 8,0.7; 9,
        0.8; 10,0.8; 11,0.7; 12,0.8; 13,0.75; 14,0.8; 15,0.7; 16,0.65; 17,0.65;
        18,0.7; 19,0.7; 20,0.7; 21,0.75; 22,0.7; 23,0.65; 24,0.6])
    "Probability of charging for a single EV at different time in a commercial block"
    annotation (Placement(transformation(extent={{80,-96},{60,-76}})));
  MultiInfrastructure.CoupledSystem.TransportationDelayAndCommunication.BaseClasses.Transmission
    tra3(kc={0.03}, cc={400})
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={30,26})));
equation
  connect(qRes.y[1], roa2.numPacSet) annotation (Line(
      points={{-59,42},{-36,42},{-36,22},{20.6,22},{20.6,7},{18.8,7}},
      color={0,0,127},
      pattern=LinePattern.Dot));

  connect(roa1.numPacSet,qCom. y[1]) annotation (Line(
      points={{-20.8,-11},{-20.8,-20},{38,-20},{38,-56},{59,-56}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(resBlo.numPac[1], tra3.numSenPac[1]) annotation (Line(
      points={{11,44.2},{30,44.2},{30,37}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa2.numSenPac, tra4.numSenPac[1]) annotation (Line(
      points={{30,-15},{30,-16},{36,-16},{36,17}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra4.numRecPac[1], resBlo.numRecPac[1]) annotation (Line(
      points={{36,39},{36,60},{-22,60},{-22,44.2},{-12,44.2}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa2.numRecPac, tra3.numRecPac[1]) annotation (Line(
      points={{30,7},{30,15}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa1.numSenPac, tra1.numSenPac[1]) annotation (Line(
      points={{-32,11},{-32,20},{-40,20},{-40,-19}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra1.numRecPac[1], comBlo.numRecPac[1]) annotation (Line(
      points={{-40,-41},{-40,-60},{24,-60},{24,-43.8},{12,-43.8}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(comBlo.numPac[1], tra2.numSenPac[1]) annotation (Line(
      points={{-11,-43.8},{-32,-43.8},{-32,-39}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(tra2.numRecPac[1], roa1.numRecPac) annotation (Line(
      points={{-32,-17},{-32,-11}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(roa1.qIn, comBlo.qOut[1]) annotation (Line(
      points={{-24,-12},{-24,-53},{-11,-53}},
      color={28,108,200},
      thickness=1));
  connect(roa1.qOut, resBlo.qIn[1]) annotation (Line(
      points={{-24,11},{-24,35},{-12,35}},
      color={28,108,200},
      thickness=1));
  connect(roa2.qOut, comBlo.qIn[1]) annotation (Line(
      points={{22,-15},{22,-53},{12,-53}},
      color={28,108,200},
      thickness=1));
  connect(resBlo.qOut[1], roa2.qIn) annotation (Line(
      points={{11,35},{22,35},{22,8}},
      color={28,108,200},
      thickness=1));
  connect(qRes.y[1], resBlo.qOutSet[1]) annotation (Line(
      points={{-59,42},{-36,42},{-36,37.2},{-12,37.2}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qCom.y[1], comBlo.qOutSet[1]) annotation (Line(
      points={{59,-56},{30,-56},{30,-50.8},{12,-50.8}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proCom.y[1], comBlo.proEV) annotation (Line(
      points={{59,-86},{22,-86},{22,-55.2},{12,-55.2}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes.y[1], resBlo.proEV) annotation (Line(
      points={{-59,10},{-44,10},{-44,32.8},{-12,32.8}},
      color={0,0,127},
      pattern=LinePattern.Dot));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file=
          "Resources/Scripts/CoupledSystem/TransportationDelayAndCommunication/Examples/TransportationDelayAndCommunication.mos"
        "Simulate and Plot"),
    Documentation(info="<html>
    <p>This example demonstrates the interaction between the transportation system and communication system.
    The wireless communication quality deteriorates with the increase of the vehicles 
    on the road while the communication quality impacts on the road traffic conditions. 
    The results from this example show both the traffic and communication conditions. </p>
</html>"));
end TransportationDelayAndCommunication;
