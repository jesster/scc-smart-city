within MultiInfrastructure.CoupledSystem.EnergyDistributionAndTransportationDelay;
package BaseClasses
annotation (Documentation(info="<html>
<p>This package contains base classes that are used to construct the models in EnergyAndTransportation.</p>
</html>"));
end BaseClasses;
