within MultiInfrastructure.CoupledSystem.EnergyDistributionAndTransportationDelay.BaseClasses;
model TransportationBlock "Model of a transportation block"
  parameter Integer num(min=1)=1 "Number of ports in the transportation block";
  Real numEV(start = 300) "Number of charging EV in the block";
  Modelica.Blocks.Interfaces.RealInput qOutSet[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,30},{-100,70}})));
  Modelica.Blocks.Interfaces.RealInput qIn[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));

  Modelica.Blocks.Interfaces.RealOutput qOut [num] "Connector of Real output signal"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));

  Modelica.Blocks.Sources.RealExpression numCha(y=proEV*numEV)
    "number of charging EV in the block"
    annotation (Placement(transformation(extent={{70,-90},{90,-70}})));
  Modelica.Blocks.Interfaces.RealOutput numEVCha(quantity="Number", unit="1")
    "Number of EV charging in the block"
    annotation (Placement(transformation(extent={{100,-90},{120,-70}})));
  Modelica.Blocks.Interfaces.RealInput proEV
    "Probability of single EV to be charged"
    annotation (Placement(transformation(extent={{-140,-100},{-100,-60}})));
equation

  qOut[:]=qOutSet[:];
  der(numEV)= (sum(qIn[:])-sum(qOut[:]))/3600;

  connect(numCha.y, numEVCha)
    annotation (Line(points={{91,-80},{110,-80}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-100,100},{100,-100}},
          pattern=LinePattern.None,
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-38,62},{28,-58}},
          pattern=LinePattern.None,
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=3),
        Rectangle(
          extent={{-50,-54},{40,-62}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{20,26},{38,22}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{36,26},{40,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=4,
          lineColor={0,0,0}),
        Rectangle(
          extent={{36,-32},{54,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=4,
          lineColor={0,0,0}),
        Rectangle(
          extent={{50,4},{54,-36}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          radius=5,
          lineColor={0,0,0}),
        Ellipse(
          extent={{44,12},{60,-6}},
          pattern=LinePattern.None,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{44,20},{60,6}},
          pattern=LinePattern.None,
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-26,50},{16,18}},
          fillColor={153,230,76},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          radius=2,
          lineColor={0,0,0}),
        Rectangle(
          extent={{48,14},{50,0}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Rectangle(
          extent={{54,14},{56,0}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),Diagram(coordinateSystem(preserveAspectRatio=false),
        graphics={
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),
    Documentation(info="<html>
<p>This model shows a transportation block which serves as a charging station. The number of the interfaces can be determined by the users. The number of the EV parking in the block is calculated using the traffic flow balance.</p>
</html>"),
    __Dymola_Commands);
end TransportationBlock;
