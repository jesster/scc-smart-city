within MultiInfrastructure.CoupledSystem.EnergyDistributionAndTransportationDelay;
package Examples "Collection of models that demonstrate the use of models in 
  MultiInfrastructure.CoupledSystem.EnergyAndTransportation"
  extends Modelica.Icons.ExamplesPackage;


annotation (Documentation(info="<html>
<p>This package contains examples for the use of models in the package EnergyAndTransportation.</p>
</html>"));
end Examples;
