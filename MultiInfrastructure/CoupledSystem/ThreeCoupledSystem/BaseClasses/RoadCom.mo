within MultiInfrastructure.CoupledSystem.ThreeCoupledSystem.BaseClasses;
model RoadCom
  "Model of a road section linked to different blocks considering the communication factor"
 extends
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.PartialTwoPort;
  parameter
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types.TrafficFlow
    q_nominal "Nominal capacity at the road";
  parameter Modelica.SIunits.Time U_nominal=1000 "Reference travel cost at the free flow";
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types.TrafficImpedance
    R "Traffic impedance of the road";
  Real lamCom "Communication factor that impacts on the traffic road";
  Modelica.Blocks.Interfaces.RealInput numRecPac
    "Number of received packets" annotation (Placement(transformation(extent={{-140,50},
            {-100,90}}),      iconTransformation(extent={{-120,70},{-100,90}})));
  Modelica.Blocks.Interfaces.RealOutput numSenPac "Number of sent packets"
    annotation (Placement(transformation(extent={{100,70},{120,90}}),
        iconTransformation(extent={{100,70},{120,90}})));
  Modelica.Blocks.Interfaces.RealInput numPacSet
    "Number of sent packet setpoint" annotation (Placement(transformation(
          extent={{-140,-60},{-100,-20}}), iconTransformation(extent={{-120,-44},
            {-100,-24}})));

equation
  lamCom = if integer(numPacSet)==0 then 1 else numSenPac/numRecPac;
  port_b.U=0;
  numSenPac=numPacSet;
  R=lamCom*0.15*U_nominal/(q_nominal)^5;
  port_b.q^5=(port_a.U-port_b.U)/R;

  annotation (Icon(graphics={Rectangle(
          extent={{-100,20},{100,-20}},
          pattern=LinePattern.None,
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid), Line(
          points={{-100,0},{100,0}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),
        Text(
          extent={{-140,80},{144,28}},
          lineColor={0,0,255},
          textString="%name")}), Documentation(info="<html>
<p>This road model considers the impact from the commincation domain. The traffic impedance is calculated as <code>RCom=lamCom*R.</code></p>
<p>See model <a href=\"modelica://MultiInfrastructure.IndividualSystem.Transportation.Road\">MultiInfrastructure.IndividualSystem.Transportation.Road</a> for more information. </p>
</html>"));
end RoadCom;
