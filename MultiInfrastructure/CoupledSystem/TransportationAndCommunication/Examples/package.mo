within MultiInfrastructure.CoupledSystem.TransportationAndCommunication;
package Examples "Collection of models that demonstrate the use of models in 
  MultiInfrastructure.CoupledSystem.TransportationAndCommunication"
  extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>This package contains examples for the use of models in the package.</p>
</html>"));
end Examples;
