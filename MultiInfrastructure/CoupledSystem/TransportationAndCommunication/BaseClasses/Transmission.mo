within MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses;
model Transmission
  parameter Integer num(min=1)=1 "Number of ports in the communication block";
   parameter Real kc[num] "Proportinal coefficient for the wireless transmission";
  parameter Real cc[num] "Maximum package amount";
   Real r[num] "Package loss rate";
  Modelica.Blocks.Interfaces.RealInput numSenPac[num] "Number of sent packets"
    annotation (Placement(transformation(extent={{-140,-30},{-100,10}}),
        iconTransformation(extent={{-120,-10},{-100,10}})));
  Modelica.Blocks.Interfaces.RealOutput numRecPac[num]
    "Number of received packets"
    annotation (Placement(transformation(extent={{100,-10},{120,10}}),
        iconTransformation(extent={{100,-10},{120,10}})));

equation
 for i in 1:num loop
  r[i]= if noEvent(numSenPac[i]>cc[i]) then kc[i]*sqrt(numSenPac[i]-cc[i]) else 0;
  numRecPac[i]=numSenPac[i]*(1-r[i]);
  end for;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Line(points={{-80,0},{-78,-6},{-76,-12},{-74,-16},{-72,-20},{-70,-22}},
            color={0,0,0}),
        Line(points={{-60,0},{-62,-6},{-64,-12},{-66,-16},{-68,-20},{-70,-22}},
            color={0,0,0}),
        Line(points={{-60,0},{-58,6},{-56,12},{-54,16},{-52,20},{-50,22}},
            color={0,0,0}),
        Line(points={{-40,0},{-42,6},{-44,12},{-46,16},{-48,20},{-50,22}},
            color={0,0,0}),
        Line(points={{-40,0},{-38,-6},{-36,-12},{-34,-16},{-32,-20},{-30,-22}},
            color={0,0,0}),
        Line(points={{-20,0},{-22,-6},{-24,-12},{-26,-16},{-28,-20},{-30,-22}},
            color={0,0,0}),
        Line(points={{-20,0},{-18,6},{-16,12},{-14,16},{-12,20},{-10,22}},
            color={0,0,0}),
        Line(points={{0,0},{-2,6},{-4,12},{-6,16},{-8,20},{-10,22}}, color={0,0,
              0}),
        Line(points={{0,0},{2,-6},{4,-12},{6,-16},{8,-20},{10,-22}}, color={0,0,
              0}),
        Line(points={{20,0},{18,-6},{16,-12},{14,-16},{12,-20},{10,-22}}, color=
             {0,0,0}),
        Line(points={{20,0},{22,6},{24,12},{26,16},{28,20},{30,22}}, color={0,0,
              0}),
        Line(points={{40,0},{38,6},{36,12},{34,16},{32,20},{30,22}}, color={0,0,
              0}),
        Line(points={{40,0},{42,-6},{44,-12},{46,-16},{48,-20},{50,-22}}, color=
             {0,0,0}),
        Line(points={{60,0},{58,-6},{56,-12},{54,-16},{52,-20},{50,-22}}, color=
             {0,0,0}),
        Line(points={{60,0},{62,6},{64,12},{66,16},{68,20},{70,22}}, color={0,0,
              0}),
        Line(points={{80,0},{78,6},{76,12},{74,16},{72,20},{70,22}}, color={0,0,
              0}),
        Text(
          extent={{-150,142},{134,90}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This transmission model links two communication base stations together. It evaluates the tranmission quality from one station to another using the packet loss rate. In this model, we use the following relationship,</p>
<p align=\"center\"><i>r=k*sqrt(Q-C)</i></p>
<p>where<i> r</i> is the packet loss rate,<i> k</i> is the coefficient,<i> Q </i>is the packet volume, <i>C</i> is the threshold which means under this threshold, no packet loss happens.</p>
</html>"));
end Transmission;
