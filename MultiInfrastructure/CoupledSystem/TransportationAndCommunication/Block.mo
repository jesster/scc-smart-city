within MultiInfrastructure.CoupledSystem.TransportationAndCommunication;
model Block "Model that connects the transportation and communication system"
  parameter Integer num=1 "Number of communication sources"
  annotation(Dialog(tab="Communication",group="Parameters"));
  parameter
    MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.Types.TrafficFlow
    qb_nominal=1200 "Nominal capacity at the road linked with port_b"
    annotation (Dialog(tab="Transportation", group="Parameters"));
  parameter Modelica.SIunits.Time Ub_nominal=1000
    "Reference travel cost at the free flow road linked with port_b"
  annotation(Dialog(tab="Transportation",group="Parameters"));
  parameter Real numEV( fixed=true, start=600)
    "Number of charging EV in the block"
  annotation(Dialog(tab="Transportation",group="Parameters"));
   MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses.TwoPortBlock tra(
    qb_nominal=qb_nominal,
    Ub_nominal=Ub_nominal,
    numEV(fixed=true, start=numEV)) "Residential block" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={0,0})));
  Modelica.Blocks.Interfaces.RealInput qb "Traffic outflow at the port_b"
    annotation (Placement(transformation(extent={{-140,-60},{-100,-20}})));
  Modelica.Blocks.Interfaces.RealOutput numPac[ num] "Number of sent packtets"
    annotation (Placement(transformation(extent={{100,32},{120,52}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.TrafficPort_b
    port_b1
    "Traffic connector b (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{94,-10},{114,10}})));
  MultiInfrastructure.IndividualSystem.Transportation.FlowTheory.BaseClasses.TrafficPort_a
    port_a1
    "Traffic connector a (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-114,-10},{-94,10}})));

  MultiInfrastructure.CoupledSystem.TransportationAndCommunication.BaseClasses.nPortBlock nPortBlock
    annotation (Placement(transformation(extent={{-10,32},{10,52}})));
  Modelica.Blocks.Interfaces.RealInput numRecPac[num]
    "Number of received packets"
    annotation (Placement(transformation(extent={{-140,22},{-100,62}})));
equation
  connect(qb, tra.qb) annotation (Line(points={{-120,-40},{-40,-40},{-40,3},{
          -12,3}},
        color={0,0,127}));
  connect(tra.port_b, port_b1)
    annotation (Line(points={{10,0},{104,0}}, color={0,127,255},
      thickness=0.5));
  connect(tra.port_a, port_a1)
    annotation (Line(points={{-10,0},{-104,0}}, color={0,127,255},
      thickness=0.5));
  connect(nPortBlock.numRecPac, numRecPac)
    annotation (Line(points={{-11,42},{-120,42}}, color={0,0,127}));
  connect(nPortBlock.numSenPac, numPac)
    annotation (Line(points={{11,42},{110,42}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-76,34},{74,-10}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name"),
        Line(points={{-80,-48},{-82,-54},{-84,-60},{-86,-64},{-88,-68},{-90,-70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-80,-48},{-78,-42},{-76,-36},{-74,-32},{-72,-28},{-70,-26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-60,-48},{-62,-42},{-64,-36},{-66,-32},{-68,-28},{-70,-26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-60,-48},{-58,-54},{-56,-60},{-54,-64},{-52,-68},{-50,-70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-40,-48},{-42,-54},{-44,-60},{-46,-64},{-48,-68},{-50,-70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-40,-48},{-38,-42},{-36,-36},{-34,-32},{-32,-28},{-30,-26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-20,-48},{-22,-42},{-24,-36},{-26,-32},{-28,-28},{-30,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{-20,-48},{-18,-54},{-16,-60},{-14,-64},{-12,-68},{-10,-70}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{0,-48},{-2,-54},{-4,-60},{-6,-64},{-8,-68},{-10,-70}},
                                                                          color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{0,-48},{2,-42},{4,-36},{6,-32},{8,-28},{10,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{20,-48},{18,-42},{16,-36},{14,-32},{12,-28},{10,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Ellipse(
          extent={{15.5,0.5},{87.5,-71.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{18.5,-2.5},{84.5,-68.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{30.5,-14.5},{72.5,-56.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{33.5,-17.5},{69.5,-53.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{39.5,-23.5},{63.5,-47.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{50,-34},{26,-96},{28,-96},{50,-38},{50,-34}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{44,-50},{58,-52}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{46.5,-30.5},{56,-40}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{52,-34},{76,-96},{74,-96},{52,-38},{52,-34}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{40,-62},{62,-64}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-68,96},{70,50}},
          lineColor={28,108,200},
          pattern=LinePattern.Dash,
          lineThickness=0.5,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid,
          textString="T+C"),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={59,30},
          rotation=90),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={59,66},
          rotation=90),
        Rectangle(
          extent={{48,70},{70,34}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{54,70},{64,60}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{54,54},{64,44}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{54,36},{64,26}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid), Line(
          points={{-76,12},{74,12}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5)}),                                      Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the charging stations and communication base stations in the block.</p>
</html>"));
end Block;
