within MultiInfrastructure.CoupledSystem.EnergyAndTransportationDelay.BaseClasses;
model RoadVariableDelay
  "Model of a road with variable delay based on the traffic condition"

  parameter Modelica.SIunits.Length l=1000 "Length of the road";
  replaceable parameter MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.Generic roaTyp
    "Road type" annotation (choicesAllMatching=true, Placement(transformation(
          extent={{-40,-40},{-20,-20}})));
  Modelica.Blocks.Interfaces.RealOutput qOut "Connector of Real output signal"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Interfaces.RealInput qIn "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Nonlinear.VariableDelay varDel(delayMax=100000000)
    annotation (Placement(transformation(extent={{54,-10},{74,10}})));
  Modelica.Blocks.Continuous.Integrator int1(y_start=20)
    annotation (Placement(transformation(extent={{-60,20},{-40,40}})));
  Modelica.Blocks.Continuous.Integrator int2(y_start=0)
    annotation (Placement(transformation(extent={{-60,50},{-40,70}})));
  IndividualSystem.Transportation.TrafficTheory.TrafficCost traCos(
      redeclare
      IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad40
      roaTyp, l=l)
              annotation (Placement(transformation(extent={{4,30},{24,50}})));
  Modelica.Blocks.Sources.RealExpression numVeh(y=abs((int1.y - int2.y)/3600))
    "Number of vehicles on the road"
    annotation (Placement(transformation(extent={{-30,30},{-10,50}})));
equation
  connect(qIn, varDel.u)
    annotation (Line(points={{-120,0},{52,0}}, color={0,0,127}));
  connect(qIn, int1.u) annotation (Line(points={{-120,0},{-80,0},{-80,30},{-62,
          30}}, color={0,0,127}));
  connect(traCos.delTim, varDel.delayTime) annotation (Line(points={{25,40},{40,
          40},{40,-6},{52,-6}}, color={0,0,127}));
  connect(numVeh.y, traCos.qIn)
    annotation (Line(points={{-9,40},{2,40}}, color={0,0,127}));
  connect(varDel.y, qOut)
    annotation (Line(points={{75,0},{110,0}}, color={0,0,127}));
  connect(varDel.y, int2.u) annotation (Line(points={{75,0},{80,0},{80,80},{-80,
          80},{-80,60},{-62,60}}, color={0,0,127}));
    annotation (Placement(transformation(extent={{60,-10},{80,10}})),
              Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                             Rectangle(
          extent={{-100,24},{100,-24}},
          pattern=LinePattern.None,
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid), Line(
          points={{-100,0},{100,0}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name")}),
                            Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows a road model with a variable delay time, 
which is calculated using a speed-flow correlation based on the traffic condition. 
See model  <a href=\"modelica://MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.TrafficCost\">
MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.TrafficCost</a> for more information.</p> </p>
</html>"));
end RoadVariableDelay;
