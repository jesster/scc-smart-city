within MultiInfrastructure.CoupledSystem.EnergyAndTransportationDelay.Examples;
model EnergyAndTransportationDelay
  "Example that demonstrates the use of models in MultiInfrastructure.CoupledSystem.EnergyAndTransportationDelay"
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
  parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
  MultiInfrastructure.CoupledSystem.EnergyAndTransportationDelay.Block resBlo(
    lat=weaDat.lat,
    f=f,
    V_nominal=V_nominal,
    num=1,
    numEV=1500)
    annotation (Placement(transformation(extent={{-10,28},{10,48}})));
  MultiInfrastructure.CoupledSystem.EnergyAndTransportationDelay.Block comBlo(
    lat=weaDat.lat,
    f=f,
    V_nominal=V_nominal,
    num=1,
    numEV=200)
    annotation (Placement(transformation(extent={{10,-34},{-10,-14}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
    computeWetBulbTemperature=false,
    filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3.mos")
    "Weather data model"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Blocks.Sources.CombiTimeTable powRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000; 12,
        -200000; 14,-300000; 16,-600000; 18,-800000; 20,-1000000; 22,-800000; 24,
        -600000]) "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,62},{-90,72}})));
  Modelica.Blocks.Sources.CombiTimeTable numPacRes(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0; 2,0; 4,0; 6,40; 8,100; 10,80; 12,60; 14,40; 16,30; 18,20; 20,10;
        22,0; 24,0]) "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{-100,46},{-90,56}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(
    f=f,
    V=V_nominal,
    phiSou=0) "Grid model that provides power to the system"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=270,
        origin={-90,0})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
    V_nominal=V_nominal,
    l=2000,
    P_nominal=2000000,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.automatic)
    "Line from or to grid"
    annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,28})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
    V_nominal=V_nominal,
    redeclare replaceable
  MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1500
      commercialCable,
    l=2000,
    P_nominal=2000000,
    final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial)
    "Line from or to grid"
    annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,-20})));
  Modelica.Blocks.Sources.CombiTimeTable powCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,-50000; 2,-30000; 4,-50000; 6,-100000; 8,-500000; 10,-800000; 12,-1100000;
        14,-1500000; 16,-1400000; 18,-1000000; 20,-700000; 22,-300000; 24,-50000])
    "Power profile for a residential building block"
    annotation (Placement(transformation(extent={{90,-28},{80,-18}})));
  Modelica.Blocks.Sources.CombiTimeTable numPacCom(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    timeScale=3600,
    table=[0,0; 2,0; 4,0; 6,40; 8,100; 10,80; 12,60; 14,40; 16,30; 18,20; 20,10;
        22,0; 24,0]) "Number of packages sent for a residential building block"
    annotation (Placement(transformation(extent={{90,-52},{80,-42}})));
  MultiInfrastructure.CoupledSystem.EnergyAndTransportationDelay.BaseClasses.RoadVariableDelay roa1(redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=15000)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-20,4})));
  MultiInfrastructure.CoupledSystem.EnergyAndTransportationDelay.BaseClasses.RoadVariableDelay roa2(        redeclare
      MultiInfrastructure.IndividualSystem.Transportation.TrafficTheory.BaseClasses.Data.DClassRoad20
      roaTyp, l=20000)
    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={18,4})));
  Modelica.Blocks.Sources.CombiTimeTable proRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.9; 1,0.9; 2,0.9; 3,0.9; 4,0.8; 5,0.7; 6,0.5; 7,0.5; 8,0.3; 9,0.4;
        10,0.35; 11,0.45; 12,0.5; 13,0.45; 14,0.4; 15,0.6; 16,0.55; 17,0.7; 18,
        0.65; 19,0.65; 20,0.75; 21,0.8; 22,0.8; 23,0.8; 24,0.9])
    "Probability of charging for a single EV at different time in a residential block"
    annotation (Placement(transformation(extent={{-100,14},{-90,24}})));
  Modelica.Blocks.Sources.CombiTimeTable proCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,0.6; 1,0.5; 2,0.55; 3,0.5; 4,0.5; 5,0.6; 6,0.6; 7,0.6; 8,0.7; 9,
        0.8; 10,0.8; 11,0.7; 12,0.8; 13,0.75; 14,0.8; 15,0.7; 16,0.65; 17,0.65;
        18,0.7; 19,0.7; 20,0.7; 21,0.75; 22,0.7; 23,0.65; 24,0.6])
    "Probability of charging for a single EV at different time in a commercial block"
    annotation (Placement(transformation(extent={{90,-90},{80,-80}})));
  Modelica.Blocks.Sources.CombiTimeTable qRes(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,15; 1,21; 2,6; 3,9; 4,24; 5,30; 6,240; 7,360; 8,580; 9,240; 10,150;
        11,88; 12,60; 13,60; 14,90; 15,90; 16,60; 17,90; 18,90; 19,60; 20,90;
        21,30; 22,15; 23,0; 24,15])
    "Number of EV charging profile for a residential building block"
    annotation (Placement(transformation(extent={{-100,32},{-90,42}})));
  Modelica.Blocks.Sources.CombiTimeTable qCom(
    tableName="tab1",
    fileName="Resources/Inputs/Transportation/nevRes.txt",
    tableOnFile=false,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
    timeScale=3600,
    table=[0,15; 1,0; 2,0; 3,0; 4,24; 5,30; 6,60; 7,60; 8,90; 9,90; 10,45; 11,
        90; 12,57; 13,61; 14,90; 15,90; 16,150; 17,275; 18,601; 19,325; 20,180;
        21,85; 22,35; 23,45; 24,15])
    "Number of EV charging profile for a commercial building block"
    annotation (Placement(transformation(extent={{90,-72},{80,-62}})));
equation
  connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
      points={{-80,90},{-20,90},{-20,46.6},{-11,46.6}},
      color={255,204,51},
      thickness=0.5));
  connect(gri.terminal, lin1.terminal_p)
    annotation (Line(points={{-80,-1.77636e-015},{-40,-1.77636e-015},{-40,18}},
                                                        color={0,120,120},
    thickness=0.5));
  connect(gri.terminal, lin2.terminal_n) annotation (Line(
      points={{-80,-1.77636e-015},{-40,-1.77636e-015},{-40,-10}},
      color={0,120,120},
      thickness=0.5));
  connect(lin2.terminal_p, comBlo.term_p) annotation (Line(
      points={{-40,-30},{-40,-48},{30,-48},{30,-17.8},{11,-17.8}},
      color={0,120,120},
      thickness=0.5));
  connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
      points={{-80,90},{60,90},{60,-15.4},{11,-15.4}},
      color={255,204,51},
      thickness=0.5));
  connect(powRes.y[1], resBlo.PBui) annotation (Line(
      points={{-89.5,67},{-30,67},{-30,39},{-12,39}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(numPacRes.y[1], resBlo.numSenPac[1]) annotation (Line(
      points={{-89.5,51},{-32,51},{-32,36},{-12,36}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(comBlo.PBui, powCom.y[1]) annotation (Line(
      points={{12,-23},{79.5,-23}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(numPacCom.y[1], comBlo.numSenPac[1]) annotation (Line(
      points={{79.5,-47},{44,-47},{44,-26},{12,-26}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proRes.y[1], resBlo.proEV) annotation (Line(
      points={{-89.5,19},{-28,19},{-28,28.6},{-12,28.6}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(roa1.qOut, resBlo.qIn[1]) annotation (Line(
      points={{-20,15},{-20,30.8},{-12,30.8}},
      color={28,108,200},
      thickness=1));
  connect(roa1.qIn, comBlo.qOut[1]) annotation (Line(
      points={{-20,-8},{-20,-31},{-11,-31}},
      color={28,108,200},
      thickness=1));
  connect(resBlo.qOut[1], roa2.qIn) annotation (Line(
      points={{11,31},{18,31},{18,16}},
      color={28,108,200},
      thickness=1));
  connect(roa2.qOut, comBlo.qIn[1]) annotation (Line(
      points={{18,-7},{18,-31.2},{12,-31.2}},
      color={28,108,200},
      thickness=1));
  connect(lin1.terminal_n, resBlo.term_p) annotation (Line(
      points={{-40,38},{-40,44.2},{-11,44.2}},
      color={0,120,120},
      thickness=0.5));
  connect(proCom.y[1], comBlo.proEV) annotation (Line(
      points={{79.5,-85},{36,-85},{36,-33.4},{12,-33.4}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qRes.y[1], resBlo.qOutSet[1]) annotation (Line(
      points={{-89.5,37},{-34,37},{-34,33},{-12,33}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qCom.y[1], comBlo.qOutSet[1]) annotation (Line(
      points={{79.5,-67},{40,-67},{40,-29},{12,-29}},
      color={0,0,127},
      pattern=LinePattern.Dot));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
  Documentation(info="<html>
  <p>This example demonstrates the interaction between the energy system and transportation system.
  The EV charging power consumption is calculated from the transportation system,
  which is related to the traffic condition between the blocks. 
  The results from this example show both energy consumption profile and the traffic conditions.</p>
</html>"),
  __Dymola_Commands(file=
          "Resources/Scripts/CoupledSystem/EnergyAndTransportationDelay/Examples/EnergyAndTransportationDelay.mos"
        "Simulate and Plot"));
end EnergyAndTransportationDelay;
