within MultiInfrastructure.CoupledSystem;
package EnergyAndCommunication
  model Block  "Model that connects the energy and communication system"
    parameter Modelica.SIunits.Angle lat "Latitude"
    annotation(Dialog(tab="Energy",group="Parameters"));
    parameter Modelica.SIunits.Voltage V_nominal
      "Nominal voltage(V_nominal>=0)"
    annotation(Dialog(tab="Energy",group="Parameters"));
    parameter Modelica.SIunits.Frequency f=60 "Nominal grid frequency"
    annotation(Dialog(tab="Energy",group="Parameters"));
    parameter Integer num=1 "Number of communication sources"
    annotation(Dialog(tab="Communication",group="Parameters"));
    MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.Bus weaBus
      annotation (Placement(transformation(extent={{-116,70},{-96,90}})));
    MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p1
      annotation (Placement(transformation(extent={{-120,50},{-100,70}})));
    MultiInfrastructure.Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui "Building power load"
      annotation (Placement(transformation(extent={{-140,-10},{-100,30}})));
    Modelica.Blocks.Interfaces.RealInput numEV "Number of charging EV"
      annotation (Placement(transformation(extent={{-140,-40},{-100,0}})));

    MultiInfrastructure.CoupledSystem.EnergyAndCommunication.BaseClasses.nPortBlock com(
    num=num)
      annotation (Placement(transformation(extent={{-26,-50},{-6,-30}})));
    Modelica.Blocks.Interfaces.RealInput numRecPac[num]
      "Number of received packets"
      annotation (Placement(transformation(extent={{-140,-60},{-100,-20}})));
    Modelica.Blocks.Interfaces.RealOutput numSenPac[num]
      "Number of sent packets"
      annotation (Placement(transformation(extent={{100,-50},{120,-30}})));
    MultiInfrastructure.CoupledSystem.EnergyAndCommunication.BaseClasses.Energy ene(
      V_nominal=V_nominal,
      f=f,
      lat=lat,
      num=num,
      redeclare
        MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
        commercialCable)
      annotation (Placement(transformation(extent={{46,20},{66,40}})));
  equation
    connect(ene.weaBus, weaBus) annotation (Line(
        points={{45,39.8},{6,39.8},{6,80},{-106,80}},
        color={255,204,51},
        thickness=0.5));
    connect(ene.term_p, term_p1) annotation (Line(points={{45,30},{0,30},{0,60},
            {-110,60}}, color={0,120,120}));
    connect(ene.PBui, PBui) annotation (Line(points={{44,27},{-12,27},{-12,10},
            {-120,10}}, color={0,0,127}));
    connect(ene.numEV, numEV) annotation (Line(points={{44,25},{-8,25},{-8,-20},
            {-120,-20}}, color={0,0,127}));
    connect(com.numSenPac, numSenPac)
      annotation (Line(points={{-5,-40},{110,-40}}, color={0,0,127}));
    connect(numRecPac, com.numRecPac) annotation (Line(points={{-120,-40},{-73,
            -40},{-27,-40}}, color={0,0,127}));
    connect(com.numSenPac, ene.numSenPac) annotation (Line(points={{-5,-40},{0,
            -40},{0,-38},{0,22.8},{44,22.8}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Line(points={{-96,-48},{-94,-54},{-92,-60},{-90,-64},{-88,-68},{-86,
                -70}},
              color={28,108,200},
            pattern=LinePattern.Dash),
          Line(points={{-76,-48},{-78,-54},{-80,-60},{-82,-64},{-84,-68},{-86,
                -70}},
              color={28,108,200},
            pattern=LinePattern.Dash),
          Line(points={{-76,-48},{-74,-42},{-72,-36},{-70,-32},{-68,-28},{-66,
                -26}},
              color={28,108,200},
            pattern=LinePattern.Dash),
          Line(points={{-56,-48},{-58,-42},{-60,-36},{-62,-32},{-64,-28},{-66,
                -26}},
              color={28,108,200},
            pattern=LinePattern.Dash),
          Line(points={{-56,-48},{-54,-54},{-52,-60},{-50,-64},{-48,-68},{-46,
                -70}},
              color={28,108,200},
            pattern=LinePattern.Dash),
          Line(points={{-36,-48},{-38,-54},{-40,-60},{-42,-64},{-44,-68},{-46,
                -70}},
              color={28,108,200},
            pattern=LinePattern.Dash),
          Line(points={{-36,-48},{-34,-42},{-32,-36},{-30,-32},{-28,-28},{-26,
                -26}},
              color={28,108,200},
            pattern=LinePattern.Dash),
          Line(points={{-16,-48},{-18,-42},{-20,-36},{-22,-32},{-24,-28},{-26,
                -26}},                                                 color={28,
                108,200},
            pattern=LinePattern.Dash),
          Line(points={{-16,-48},{-14,-54},{-12,-60},{-10,-64},{-8,-68},{-6,-70}},
                                                                       color={28,
                108,200},
            pattern=LinePattern.Dash),
          Line(points={{4,-48},{2,-54},{0,-60},{-2,-64},{-4,-68},{-6,-70}}, color={28,
                108,200},
            pattern=LinePattern.Dash),
          Line(points={{4,-48},{6,-42},{8,-36},{10,-32},{12,-28},{14,-26}},
                                                                       color={28,
                108,200},
            pattern=LinePattern.Dash),
          Line(points={{24,-48},{22,-42},{20,-36},{18,-32},{16,-28},{14,-26}},
                                                                       color={28,
                108,200},
            pattern=LinePattern.Dash),
          Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
          Line(points={{-58,22},{-32,22}},   color={0,0,0}),
          Line(points={{-46,22},{-46,-8}},    color={0,0,0}),
          Line(points={{-58,22},{-58,20}},   color={0,0,0}),
          Line(points={{-32,22},{-32,20}},   color={0,0,0}),
          Line(
            points={{-68,-8},{-66,-6},{-62,-2},{-58,12},{-58,20}},
            color={175,175,175},
            smooth=Smooth.Bezier),
          Line(
            points={{-68,-8},{-56,-6},{-50,-4},{-34,8},{-32,20}},
            color={175,175,175},
            smooth=Smooth.Bezier),
          Line(points={{-22,32},{4,32}},     color={0,0,0}),
          Line(points={{-22,32},{-22,30}},   color={0,0,0}),
          Line(points={{4,32},{4,30}},       color={0,0,0}),
          Line(
            points={{-58,20},{-42,24},{-32,26},{-26,28},{-22,30}},
            color={175,175,175},
            smooth=Smooth.Bezier),
          Line(
            points={{-32,20},{-20,22},{-10,24},{-2,26},{4,30}},
            color={175,175,175},
            smooth=Smooth.Bezier),
          Line(
            points={{-22,30},{-6,34},{4,36},{10,38},{14,40}},
            color={175,175,175},
            smooth=Smooth.Bezier),
          Line(
            points={{4,30},{16,32},{26,34},{34,36},{40,40}},
            color={175,175,175},
            smooth=Smooth.Bezier),
          Line(points={{14,42},{40,42}},   color={0,0,0}),
          Line(points={{-10,32},{-10,2}},    color={0,0,0}),
          Line(points={{26,42},{26,10}},   color={0,0,0}),
          Line(points={{14,42},{14,40}},     color={0,0,0}),
          Line(points={{40,42},{40,40}},     color={0,0,0}),
          Ellipse(
            extent={{19.5,0.5},{91.5,-71.5}},
            lineColor={170,213,255},
            fillColor={170,213,255},
            fillPattern=FillPattern.Solid),
          Ellipse(
            extent={{22.5,-2.5},{88.5,-68.5}},
            lineColor={170,213,255},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Ellipse(
            extent={{34.5,-14.5},{76.5,-56.5}},
            lineColor={170,213,255},
            fillColor={170,213,255},
            fillPattern=FillPattern.Solid),
          Ellipse(
            extent={{37.5,-17.5},{73.5,-53.5}},
            lineColor={170,213,255},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Ellipse(
            extent={{43.5,-23.5},{67.5,-47.5}},
            lineColor={170,213,255},
            fillColor={170,213,255},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{54,-34},{30,-96},{32,-96},{54,-38},{54,-34}},
            lineColor={0,0,0},
            fillColor={0,0,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{48,-50},{62,-52}},
            lineColor={0,0,0},
            fillColor={0,0,0},
            fillPattern=FillPattern.Solid),
          Ellipse(
            extent={{50.5,-30.5},{60,-40}},
            pattern=LinePattern.None,
            fillColor={0,0,0},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0}),
          Polygon(
            points={{56,-34},{80,-96},{78,-96},{56,-38},{56,-34}},
            lineColor={0,0,0},
            fillColor={0,0,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{44,-62},{66,-64}},
            lineColor={0,0,0},
            fillColor={0,0,0},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-70,94},{68,48}},
            lineColor={28,108,200},
            pattern=LinePattern.Dash,
            lineThickness=0.5,
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid,
            textString="E+C"),
          Text(
            extent={{-150,158},{134,106}},
            lineColor={0,0,255},
            textString="%name")}),
                                 Diagram(coordinateSystem(preserveAspectRatio=
              false)),
      Documentation(info="<html>
<p>This model shows the energy provision and communication base stations in the block.The load from the communication base stations is calculated from the communication system.</p>
</html>"));
  end Block;

  package Examples "Collection of models that demonstrate the use of models in 
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication"
    extends Modelica.Icons.ExamplesPackage;

    model EnergyAndCommunication "Example that demonstrates the use of models in 
  MultiInfrastructure.CoupledSystem.EnergyAndCommunication"
      extends Modelica.Icons.Example;
      parameter Modelica.SIunits.Voltage V_nominal = 10000 "Nominal grid voltage";
      parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
      MultiInfrastructure.CoupledSystem.EnergyAndCommunication.Block comBlo(
        lat=weaDat.lat,
        num=2,
        V_nominal=V_nominal,
        f=f)
        annotation (Placement(transformation(extent={{12,-40},{-8,-20}})));
      MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
        computeWetBulbTemperature=false,
        filNam="modelica://MultiInfrastructure/Resources/WeatherData/USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3.mos")
        "Weather data model"
        annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
      MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid  gri(
        f=f,
        V=V_nominal,
        phiSou=0) "Grid model that provides power to the system"
        annotation (Placement(transformation(extent={{10,10},{-10,-10}},
            rotation=270,
            origin={-90,2})));
      MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
        V_nominal=V_nominal,
        l=2000,
        P_nominal=2000000,
        redeclare
          MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
          commercialCable,
        final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.automatic)
                           "Line from or to grid"      annotation (Placement(
            transformation(
            extent={{10,-10},{-10,10}},
            rotation=90,
            origin={-40,20})));
      MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
        V_nominal=V_nominal,
        redeclare replaceable
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_1500
          commercialCable,
        l=2000,
        P_nominal=2000000,
        final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial)
                           "Line from or to grid"      annotation (Placement(
            transformation(
            extent={{10,-10},{-10,10}},
            rotation=90,
            origin={-40,-20})));
      Modelica.Blocks.Sources.CombiTimeTable powRes(
        extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        timeScale=3600,
        table=[0,-600000; 2,-460000; 4,-380000; 6,-400000; 8,-400000; 10,-300000; 12,
            -200000; 14,-300000; 16,-600000; 18,-800000; 20,-1000000; 22,-800000; 24,
            -600000]) "Power profile for a residential building block"
        annotation (Placement(transformation(extent={{-100,60},{-90,70}})));
      Modelica.Blocks.Sources.CombiTimeTable powCom(
        extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        timeScale=3600,
        table=[0,-50000; 2,-30000; 4,-50000; 6,-100000; 8,-500000; 10,-800000; 12,-1100000;
            14,-1500000; 16,-1400000; 18,-1000000; 20,-700000; 22,-300000; 24,-50000])
        "Power profile for a residential building block"
        annotation (Placement(transformation(extent={{84,-34},{74,-24}})));
      Modelica.Blocks.Sources.CombiTimeTable nevRes(
        extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        timeScale=3600,
        table=[0,100; 2,80; 4,60; 6,50; 8,30; 10,8; 12,10; 14,20; 16,30; 18,40; 20,60;
            22,80; 24,100])
        "Number of EV charging profile for a residential building block"
        annotation (Placement(transformation(extent={{-100,40},{-90,50}})));
      Modelica.Blocks.Sources.CombiTimeTable nevCom(
        extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        timeScale=3600,
        table=[0,5; 2,5; 4,5; 6,10; 8,60; 10,80; 12,80; 14,70; 16,60; 18,30; 20,20;
            22,10; 24,5])
        "Number of EV charging profile for a residential building block"
        annotation (Placement(transformation(extent={{84,-50},{74,-40}})));
      MultiInfrastructure.CoupledSystem.EnergyAndCommunication.BaseClasses.Transmission tra1(
      num=2,
      kc={0.03,0.03},
      cc={105,100})
      annotation (Placement(transformation(extent={{-10,-10},{10,10}},
            rotation=90,
            origin={-18,12})));
      MultiInfrastructure.CoupledSystem.EnergyAndCommunication.BaseClasses.Transmission tra2(
      num=2,
      cc={100,105},
      kc={0.03,0.05})
      annotation (Placement(transformation(extent={{10,10},{-10,-10}},
            rotation=90,
            origin={18,12})));
      Modelica.Blocks.Sources.CombiTimeTable numPac1(
        tableName="tab1",
        fileName="Resources/Inputs/Transportation/nevRes.txt",
        tableOnFile=false,
        extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
        timeScale=3600,
        table=[0,5; 1,0; 2,0; 3,0; 4,8; 5,10; 6,20; 7,20; 8,30; 9,30; 10,15; 11,30; 12,
            20; 13,20; 14,30; 15,30; 16,50; 17,98; 18,119; 19,80; 20,60; 21,40; 22,10;
            23,15; 24,5]) "Number of vehicles for a commercial building block"
        annotation (Placement(transformation(extent={{-100,20},{-90,30}})));
      Modelica.Blocks.Sources.CombiTimeTable numPac2(
        tableName="tab1",
        fileName="Resources/Inputs/Transportation/nevRes.txt",
        tableOnFile=false,
        extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
        smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments,
        timeScale=3600,
        table=[0,5; 1,7; 2,2; 3,3; 4,8; 5,10; 6,80; 7,100; 8,120; 9,80; 10,50; 11,30; 12,
            20; 13,20; 14,30; 15,30; 16,20; 17,30; 18,30; 19,20; 20,30; 21,10; 22,5; 23,
            0; 24,5]) "Number of vehicle for a residential building block"
        annotation (Placement(transformation(extent={{84,-66},{74,-56}})));
      MultiInfrastructure.CoupledSystem.EnergyAndCommunication.Block resBlo(
        num=2,
        lat=weaDat.lat,
        V_nominal=V_nominal,
        f=f) annotation (Placement(transformation(extent={{-6,44},{14,64}})));
    equation
      connect(gri.terminal, lin1.terminal_p) annotation (Line(
          points={{-80,2},{-40,2},{-40,10}},
          color={0,120,120},
          thickness=0.5));
      connect(gri.terminal, lin2.terminal_n) annotation (Line(
          points={{-80,2},{-40,2},{-40,-10}},
          color={0,120,120},
          thickness=0.5));
      connect(lin2.terminal_p, comBlo.term_p1) annotation (Line(
          points={{-40,-30},{-40,-52},{40,-52},{40,-24},{13,-24}},
          color={0,120,120},
          thickness=0.5));
      connect(weaDat.weaBus, comBlo.weaBus) annotation (Line(
          points={{-80,90},{40,90},{40,-22},{12.6,-22}},
          color={255,204,51},
          thickness=0.5));
      connect(powCom.y[1], comBlo.PBui) annotation (Line(points={{73.5,-29},{
              44.75,-29},{14,-29}},             color={0,0,127},
          pattern=LinePattern.Dot));
      connect(nevCom.y[1], comBlo.numEV) annotation (Line(points={{73.5,-45},{
              26,-45},{26,-32},{14,-32}}, color={0,0,127},
          pattern=LinePattern.Dot));
      connect(tra2.numRecPac[1], comBlo.numRecPac[1]) annotation (Line(points={{17.5,1},
              {18,1},{18,-35},{14,-35}},          color={0,0,127},
          pattern=LinePattern.Dash));
      connect(numPac1.y[1], resBlo.numRecPac[1]) annotation (Line(points={{-89.5,25},
              {-58,25},{-34,25},{-34,26},{-34,49},{-8,49}},           color={0,0,127},
          pattern=LinePattern.Dot));

      connect(nevRes.y[1], resBlo.numEV) annotation (Line(points={{-89.5,45},{
              -60,45},{-36,45},{-36,52},{-30,52},{-8,52}}, color={0,0,127},
          pattern=LinePattern.Dot));
      connect(lin1.terminal_n, resBlo.term_p1) annotation (Line(
          points={{-40,30},{-40,60},{-7,60}},
          color={0,120,120},
          thickness=0.5));
      connect(weaDat.weaBus, resBlo.weaBus) annotation (Line(
          points={{-80,90},{-30,90},{-30,62},{-6.6,62}},
          color={255,204,51},
          thickness=0.5));
      connect(powRes.y[1], resBlo.PBui) annotation (Line(points={{-89.5,65},{
              -36,65},{-36,55},{-8,55}}, color={0,0,127},
          pattern=LinePattern.Dot));
      connect(resBlo.numSenPac, tra2.numSenPac)
        annotation (Line(points={{15,50},{18,50},{18,23}}, color={0,0,127},
          pattern=LinePattern.Dash));
      connect(numPac2.y[1], comBlo.numRecPac[2]) annotation (Line(points={{73.5,
              -61},{24,-61},{24,-33},{14,-33}}, color={0,0,127},
          pattern=LinePattern.Dot));
      connect(comBlo.numSenPac, tra1.numSenPac) annotation (Line(points={{-9,
              -34},{-18,-34},{-18,1}}, color={0,0,127},
          pattern=LinePattern.Dash));
      connect(tra1.numRecPac[2], resBlo.numRecPac[2]) annotation (Line(points={{-18.5,
              23},{-18.5,22},{-18,22},{-18,36},{-18,51},{-8,51}},
            color={0,0,127},
          pattern=LinePattern.Dash));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
            coordinateSystem(preserveAspectRatio=false)),
        __Dymola_Commands(file=
              "Resources/Scripts/CoupledSystem/EnergyAndCommunication/Examples/EnergyAndCommunication.mos"
            "Simulate and Plot"),
        Documentation(info="<html>
    <p>This example demonstrates the interaction between the energy system and communication system. 
    The wireless communication power consumption is calculated from the communication system. 
    The results from this example show both energy consumption profile and the communciation conditions.</p>
</html>"));
    end EnergyAndCommunication;
    annotation (Documentation(info="<html>
<p>This package contains examples for the use of models in the package.</p>
</html>"));
  end Examples;

  package BaseClasses

    model nPortBlock "Model of a communication block with n ports"

      parameter Integer num(min=1)=1 "Number of ports in the communication block";
      Modelica.Blocks.Interfaces.RealOutput numSenPac[num]
        "Number of sent packets"
        annotation (Placement(transformation(extent={{100,-10},{120,10}})));
      Modelica.Blocks.Interfaces.RealInput numRecPac[num]
        "Number of received packets" annotation (Placement(transformation(extent={{-140,
                -30},{-100,10}}), iconTransformation(extent={{-120,-10},{-100,10}})));
    equation
       for i in 1:num loop
      numSenPac[i]=numRecPac[i];
       end for
      annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
            Ellipse(
              extent={{-57,87},{60,-30}},
              lineColor={170,213,255},
              fillColor={170,213,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{-53,83},{56,-26}},
              lineColor={170,213,255},
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{-34.5,64.5},{37.5,-7.5}},
              lineColor={170,213,255},
              fillColor={170,213,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{-31.5,61.5},{34.5,-4.5}},
              lineColor={170,213,255},
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{-19.5,49.5},{22.5,7.5}},
              lineColor={170,213,255},
              fillColor={170,213,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{-16.5,46.5},{19.5,10.5}},
              lineColor={170,213,255},
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{-10.5,40.5},{13.5,16.5}},
              lineColor={170,213,255},
              fillColor={170,213,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{-7.5,37.5},{10.5,19.5}},
              pattern=LinePattern.None,
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid,
              lineColor={0,0,0}),
            Polygon(
              points={{6,26},{50,-92},{42,-92},{0,26},{6,26}},
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Rectangle(
              extent={{-24,-32},{26,-36}},
              pattern=LinePattern.None,
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-14,-8},{18,-12}},
              pattern=LinePattern.None,
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid),
            Polygon(
              points={{-2,26},{-46,-92},{-38,-92},{4,26},{-2,26}},
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Rectangle(
              extent={{-30,-58},{34,-62}},
              pattern=LinePattern.None,
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid),
            Text(
              extent={{-150,158},{134,106}},
              lineColor={0,0,255},
              textString="%name")}),            Diagram(coordinateSystem(
              preserveAspectRatio=false)),
        Documentation(info="<html>
<p>This model shows a n-port communication block. Its input is the number of the received packets and its output is the number of the sent packets, which is determined by the traffic outflow. The number of the ports can be set by the users.</p>
</html>"));

      annotation (Documentation(info="<html>
<p>This model shows an n-port communication block. Its input is the number of the received packets and its output is the number of the sent packets, which is determined by the traffic outflow. The number of the ports can be set by the users.</p>
</html>"));
    end nPortBlock;

    model Energy "Model of power supply and demand"
      parameter Modelica.SIunits.Area A=60000 "Net surface area of PV"
        annotation(Dialog(tab="Supply",group="PVs"));
      parameter Modelica.SIunits.Voltage VPV_nominal=480
        "Nominal voltage of PV (VPV_nominal >= 0)"
        annotation (Dialog(tab="Supply",group="PVs"));
      parameter Modelica.SIunits.Voltage VWin_nominal=480
        "Nominal voltage of wind turbine (VWin_nominal >= 0)"
        annotation (Dialog(tab="Supply",group="Wind Turbines"));
      parameter Modelica.SIunits.Power PV_nominal=-PLoa_nominal "Nominal power of PV panels"
        annotation(Dialog(tab="Supply",group="PVs"));
      parameter Modelica.SIunits.Power PWin_nominal=-PLoa_nominal
        "Nominal power pf the wind turbine"
        annotation(Dialog(tab="Supply",group="Wind Turbines"));
      parameter Modelica.SIunits.Voltage V_nominal=10000 "Nominal voltage(V_nominal>=0)";
      parameter Modelica.SIunits.Frequency f = 60 "Nominal grid frequency";
      parameter Modelica.SIunits.Power PLoa_nominal=PBb_nominal+PEv_nominal+PCt_nominal
        "Nominal power of demand load (negative if consumed, positive if generated)"
        annotation(Dialog(tab="Demand",group="Parameters"));
      parameter Modelica.SIunits.Angle lat "Latitude";
      parameter Modelica.SIunits.Power PBb_nominal=-1500000
        "Nominal power of building blocks (negative if consumed, positive if generated)"
        annotation(Dialog(tab="Demand",group="Parameters"));
      parameter Modelica.SIunits.Power PEv_nominal=-1500000
        "Nominal power of EV charging (negative if consumed, positive if generated)"
        annotation(Dialog(tab="Demand",group="Parameters"));
      parameter Modelica.SIunits.Power PCt_nominal=-70000
        "Nominal power of communication towers (negative if consumed, positive if generated)"
        annotation(Dialog(tab="Demand",group="Parameters"));
      parameter Modelica.SIunits.Length l=2000 "Length of the main line in the energy model"
        annotation (Dialog(tab="Line",group="Parameters"));
      parameter Modelica.SIunits.Length l1=2000 "Length of the line 1 in the energy model"
        annotation (Dialog(tab="Line",group="Parameters"));
      parameter Modelica.SIunits.Length l2=2000 "Length of the line 2 in the energy model"
        annotation (Dialog(tab="Line",group="Parameters"));
     parameter Integer num "Number of ports in the communication block"
        annotation (Dialog(tab="Demand"));
      replaceable parameter
        MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable
        constrainedby
        MultiInfrastructure.Buildings.Electrical.Transmission.BaseClasses.BaseCable
        "Commercial cables options"
        annotation (
        Evaluate=true,Dialog(
        tab="Line",group="Manual mode",
        enable=mode == MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial),
        choicesAllMatching=true);
      MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
        annotation (Placement(transformation(extent={{-120,-10},{-100,10}})));
      MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
            transformation(extent={{-130,60},{-90,100}}),iconTransformation(extent={{-120,88},
                {-100,108}})));
      Modelica.Blocks.Interfaces.RealInput numEV
        "Number of EV charging in the block"
        annotation (Placement(transformation(extent={{-140,-70},{-100,-30}})));
      MultiInfrastructure.Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui
        "Building power load"
        annotation (Placement(transformation(extent={{-140,-50},{-100,-10}})));
      MultiInfrastructure.IndividualSystem.Energy.SupplySide.Supply sup(
        PV_nominal=-PLoa_nominal,
        lat=lat,
        use_C=false,
        A=A,
        l=10000,
        l1=2000,
        l2=2000,
        V_nominal=V_nominal,
        redeclare
          MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
          commercialCable,
        PWin_nominal=PWin_nominal,
        VABase=PLoa_nominal,
        VHigh=10000,
        VLow=480,
        XoverR=8,
        Zperc=0.03,
        VPV_nominal=VPV_nominal,
        VWin_nominal=VWin_nominal) "Supply side"
        annotation (Placement(transformation(extent={{40,20},{60,40}})));
      MultiInfrastructure.IndividualSystem.Energy.DemandSide.Demand dem(
        PBb_nominal=PBb_nominal,
        PEv_nominal=PEv_nominal,
        PCt_nominal=PCt_nominal,
        l1=2000,
        l2=3000,
        l3=3000,
        V_nominal=V_nominal,
        redeclare
          MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
          commercialCable,
        num=num)           "Demand side"
        annotation (Placement(transformation(extent={{40,-60},{60,-40}})));
      MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin2(
        V_nominal=V_nominal,
        P_nominal=PLoa_nominal,
        final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
        l=l2,
        redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable)
                                "Line from supply to demand"
                                               annotation (Placement(transformation(
            extent={{10,-10},{-10,10}},
            rotation=180,
            origin={1.77636e-015,-20})));
      MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin1(
        V_nominal=V_nominal,
        P_nominal=PLoa_nominal,
        final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
        l=l1,
        redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable)
                                "Line from supply to demand"
                                               annotation (Placement(transformation(
            extent={{10,-10},{-10,10}},
            rotation=180,
            origin={0,20})));
      MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Lines.Line lin(
        V_nominal=V_nominal,
        P_nominal=PLoa_nominal,
        final mode=MultiInfrastructure.Buildings.Electrical.Types.CableMode.commercial,
        l=l,
        redeclare replaceable MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Generic commercialCable=commercialCable)
                                "Line from or to grid" annotation (Placement(
            transformation(
            extent={{10,-10},{-10,10}},
            rotation=180,
            origin={-70,0})));

      Modelica.Blocks.Interfaces.RealInput numSenPac[num] "Number of packages sent"
        annotation (Placement(transformation(extent={{-140,-92},{-100,-52}})));
    equation
      connect(sup.weaBus, weaBus) annotation (Line(
          points={{39,38.4},{-20,38.4},{-20,80},{-110,80}},
          color={255,204,51},
          thickness=0.5), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(dem.PBui, PBui) annotation (Line(points={{38,-45},{-50,-45},{-50,-30},
              {-120,-30}},      color={0,0,127}));
      connect(numEV, dem.numEV) annotation (Line(points={{-120,-50},{-120,-50},{38,
              -50}}, color={0,0,127}));
      connect(lin2.terminal_p, dem.term_p)
        annotation (Line(points={{10,-20},{20,-20},{20,-42},{30,-42},{40,-42},{39.2,
              -42}},                                           color={0,120,120},
          thickness=0.5));
      connect(lin1.terminal_p, sup.term_p)
        annotation (Line(points={{10,20},{10,20},{20,20},{20,30},{42,30},{42,30.2},
              {39,30.2}},                                   color={0,120,120},
          thickness=0.5));
      connect(lin.terminal_n, term_p)
        annotation (Line(points={{-80,0},{-110,0}}, color={0,120,120},
          thickness=0.5));
      connect(lin.terminal_p, lin1.terminal_n) annotation (Line(points={{-60,
              -1.33227e-015},{-40,-1.33227e-015},{-40,20},{-10,20}},
                                     color={0,120,120},
          thickness=0.5));
      connect(lin.terminal_p, lin2.terminal_n) annotation (Line(points={{-60,0},{
              -60,0},{-40,0},{-40,-20},{-10,-20}},
                                               color={0,120,120},
          thickness=0.5));
      connect(dem.numSenPac, numSenPac) annotation (Line(points={{38,-55},{-50,-55},
              {-50,-72},{-120,-72}}, color={0,0,127}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
            Rectangle(
              extent={{70,-64},{10,-70}},
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None,
              lineColor={0,0,0}),
            Rectangle(
              extent={{-66,76},{-68,6}},
              pattern=LinePattern.None,
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),
            Polygon(
              points={{7,-1},{-21,-9},{-17,-13},{7,-1}},
              pattern=LinePattern.None,
              origin={-55,57},
              rotation=-90,
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid,
              lineColor={0,0,0}),
            Polygon(
              points={{-94,92},{-68,80},{-70,76},{-94,92}},
              pattern=LinePattern.None,
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),
            Polygon(
              points={{-38,92},{-66,80},{-64,76},{-38,92}},
              pattern=LinePattern.None,
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{21.5,96.5},{93.5,24.5}},
              lineColor={170,213,255},
              fillColor={170,213,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{24.5,93.5},{90.5,27.5}},
              lineColor={170,213,255},
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{36.5,81.5},{78.5,39.5}},
              lineColor={170,213,255},
              fillColor={170,213,255},
              fillPattern=FillPattern.Solid),
            Polygon(
              points={{-28,4},{-46,-48},{2,-48},{20,4},{-28,4}},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Ellipse(
              extent={{39.5,78.5},{75.5,42.5}},
              lineColor={170,213,255},
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{45.5,72.5},{69.5,48.5}},
              lineColor={170,213,255},
              fillColor={170,213,255},
              fillPattern=FillPattern.Solid),
            Polygon(
              points={{56,62},{32,0},{34,0},{56,58},{56,62}},
              lineColor={0,0,0},
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{50,46},{64,44}},
              lineColor={0,0,0},
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid),
            Ellipse(
              extent={{52.5,65.5},{62,56}},
              pattern=LinePattern.None,
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid,
              lineColor={0,0,0}),
            Polygon(
              points={{58,62},{82,0},{80,0},{58,58},{58,62}},
              lineColor={0,0,0},
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-50,64},{14,4}},
            lineColor={150,150,150},
            fillPattern=FillPattern.Solid,
            fillColor={150,150,150}),
          Polygon(
            points={{-18,88},{-60,64},{24,64},{-18,88}},
            lineColor={95,95,95},
            smooth=Smooth.None,
            fillPattern=FillPattern.Solid,
            fillColor={95,95,95}),
          Rectangle(
            extent={{-38,40},{-24,54}},
            lineColor={255,255,255},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-12,40},{2,54}},
            lineColor={255,255,255},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-38,18},{-24,32}},
            lineColor={255,255,255},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-12,18},{2,32}},
            lineColor={255,255,255},
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{16,16},{64,-68}},
              pattern=LinePattern.None,
              lineColor={0,0,0},
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              radius=3),
            Rectangle(
              extent={{64,2},{74,-2}},
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None,
              lineColor={0,0,0}),
            Rectangle(
              extent={{72,2},{76,-52}},
              pattern=LinePattern.None,
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              radius=4,
              lineColor={0,0,0}),
            Rectangle(
              extent={{72,-48},{86,-52}},
              pattern=LinePattern.None,
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              radius=4,
              lineColor={0,0,0}),
            Rectangle(
              extent={{82,-22},{86,-52}},
              pattern=LinePattern.None,
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              radius=5,
              lineColor={0,0,0}),
            Ellipse(
              extent={{78,-16},{90,-32}},
              pattern=LinePattern.None,
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              lineColor={0,0,0}),
            Rectangle(
              extent={{78,-12},{90,-22}},
              pattern=LinePattern.None,
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid,
              lineColor={0,0,0}),
            Rectangle(
              extent={{22,8},{58,-16}},
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None,
              radius=2,
              lineColor={0,0,0}),
            Rectangle(
              extent={{80,-16},{82,-22}},
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None,
              lineColor={0,0,0}),
            Rectangle(
              extent={{86,-16},{88,-22}},
              fillColor={150,225,75},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None,
              lineColor={0,0,0}),
            Rectangle(
              extent={{46,34},{68,32}},
              lineColor={0,0,0},
              fillColor={0,0,0},
              fillPattern=FillPattern.Solid),
            Polygon(
              points={{-72,10},{-90,-42},{-42,-42},{-24,10},{-72,10}},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-68,6},{-72,-6},{-62,-6},{-58,6},{-68,6}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-54,6},{-58,-6},{-48,-6},{-44,6},{-54,6}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-40,6},{-44,-6},{-34,-6},{-30,6},{-40,6}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-60,-10},{-64,-22},{-54,-22},{-50,-10},{-60,-10}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-46,-10},{-50,-22},{-40,-22},{-36,-10},{-46,-10}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-74,-10},{-78,-22},{-68,-22},{-64,-10},{-74,-10}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-66,-26},{-70,-38},{-60,-38},{-56,-26},{-66,-26}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-52,-26},{-56,-38},{-46,-38},{-42,-26},{-52,-26}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-80,-26},{-84,-38},{-74,-38},{-70,-26},{-80,-26}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-24,0},{-28,-12},{-18,-12},{-14,0},{-24,0}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-10,0},{-14,-12},{-4,-12},{0,0},{-10,0}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{4,0},{0,-12},{10,-12},{14,0},{4,0}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-16,-16},{-20,-28},{-10,-28},{-6,-16},{-16,-16}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-2,-16},{-6,-28},{4,-28},{8,-16},{-2,-16}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-30,-16},{-34,-28},{-24,-28},{-20,-16},{-30,-16}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-22,-32},{-26,-44},{-16,-44},{-12,-32},{-22,-32}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-8,-32},{-12,-44},{-2,-44},{2,-32},{-8,-32}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Polygon(
              points={{-36,-32},{-40,-44},{-30,-44},{-26,-32},{-36,-32}},
              fillColor={0,0,255},
              fillPattern=FillPattern.Solid,
              pattern=LinePattern.None),
            Ellipse(
              extent={{-70,80},{-64,74}},
              pattern=LinePattern.None,
              fillColor={95,95,95},
              fillPattern=FillPattern.Solid,
              lineColor={0,0,0}),
            Line(points={{-90,-70},{-64,-70}}, color={0,0,0}),
            Line(points={{-78,-70},{-78,-100}}, color={0,0,0}),
            Line(points={{-90,-70},{-90,-72}}, color={0,0,0}),
            Line(points={{-64,-70},{-64,-72}}, color={0,0,0}),
            Line(
              points={{-100,-100},{-98,-98},{-94,-94},{-90,-80},{-90,-72}},
              color={175,175,175},
              smooth=Smooth.Bezier),
            Line(
              points={{-100,-100},{-88,-98},{-82,-96},{-66,-84},{-64,-72}},
              color={175,175,175},
              smooth=Smooth.Bezier),
            Line(points={{-54,-60},{-28,-60}}, color={0,0,0}),
            Line(points={{-42,-60},{-42,-90}}, color={0,0,0}),
            Line(points={{-54,-60},{-54,-62}}, color={0,0,0}),
            Line(points={{-28,-60},{-28,-62}}, color={0,0,0}),
            Line(
              points={{-90,-72},{-74,-68},{-64,-66},{-58,-64},{-54,-62}},
              color={175,175,175},
              smooth=Smooth.Bezier),
            Line(
              points={{-64,-72},{-52,-70},{-42,-68},{-34,-66},{-28,-62}},
              color={175,175,175},
              smooth=Smooth.Bezier),
            Line(points={{-18,-50},{8,-50}}, color={0,0,0}),
            Line(points={{-6,-50},{-6,-80}}, color={0,0,0}),
            Line(points={{-18,-50},{-18,-52}}, color={0,0,0}),
            Line(points={{8,-50},{8,-52}}, color={0,0,0}),
            Line(
              points={{-54,-62},{-38,-58},{-28,-56},{-22,-54},{-18,-52}},
              color={175,175,175},
              smooth=Smooth.Bezier),
            Line(
              points={{-28,-62},{-16,-60},{-6,-58},{2,-56},{8,-52}},
              color={175,175,175},
              smooth=Smooth.Bezier),
            Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
            Text(
              extent={{-150,158},{134,106}},
              lineColor={0,0,255},
              textString="%name")}),                                        Diagram(
            coordinateSystem(preserveAspectRatio=false)),
        __Dymola_Commands,
        Documentation(info="<html>
    <p>This model shows the energy provision and use in the block. As the schematics shows, 
    the power supply from the PV and wind turbine is consumed by the building blocks, 
    EV charging stations, and the communication towers. If it is oversupply, 
    the power will flow from the block to other blocks or the grid. Conversely, 
    if the supply is not sufficient, the grid or other blocks will provide the power.</p>
<p><br><img src=\"modelica://MultiInfrastructure/Resources/Images/IndividualSystem/Energy/Energy.png\"/></p>
</html>"));
    end Energy;

    model Transmission
      parameter Integer num(min=1)=1 "Number of ports in the communication block";
       parameter Real kc[num] "Proportinal coefficient for the wireless transmission";
      parameter Real cc[num] "Maximum package amount";
       Real r[num] "Package loss rate";
      Modelica.Blocks.Interfaces.RealInput numSenPac[num] "Number of sent packets"
        annotation (Placement(transformation(extent={{-140,-30},{-100,10}}),
            iconTransformation(extent={{-120,-10},{-100,10}})));
      Modelica.Blocks.Interfaces.RealOutput numRecPac[num]
        "Number of received packets"
        annotation (Placement(transformation(extent={{100,-10},{120,10}}),
            iconTransformation(extent={{100,-10},{120,10}})));

    equation
     for i in 1:num loop
      r[i]= if noEvent(numSenPac[i]>cc[i]) then kc[i]*sqrt(numSenPac[i]-cc[i]) else 0;
      numRecPac[i]=numSenPac[i]*(1-r[i]);
      end for;
      annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
            Line(points={{-80,0},{-78,-6},{-76,-12},{-74,-16},{-72,-20},{-70,-22}},
                color={0,0,0}),
            Line(points={{-60,0},{-62,-6},{-64,-12},{-66,-16},{-68,-20},{-70,-22}},
                color={0,0,0}),
            Line(points={{-60,0},{-58,6},{-56,12},{-54,16},{-52,20},{-50,22}},
                color={0,0,0}),
            Line(points={{-40,0},{-42,6},{-44,12},{-46,16},{-48,20},{-50,22}},
                color={0,0,0}),
            Line(points={{-40,0},{-38,-6},{-36,-12},{-34,-16},{-32,-20},{-30,-22}},
                color={0,0,0}),
            Line(points={{-20,0},{-22,-6},{-24,-12},{-26,-16},{-28,-20},{-30,-22}},
                color={0,0,0}),
            Line(points={{-20,0},{-18,6},{-16,12},{-14,16},{-12,20},{-10,22}},
                color={0,0,0}),
            Line(points={{0,0},{-2,6},{-4,12},{-6,16},{-8,20},{-10,22}}, color={0,0,
                  0}),
            Line(points={{0,0},{2,-6},{4,-12},{6,-16},{8,-20},{10,-22}}, color={0,0,
                  0}),
            Line(points={{20,0},{18,-6},{16,-12},{14,-16},{12,-20},{10,-22}}, color=
                 {0,0,0}),
            Line(points={{20,0},{22,6},{24,12},{26,16},{28,20},{30,22}}, color={0,0,
                  0}),
            Line(points={{40,0},{38,6},{36,12},{34,16},{32,20},{30,22}}, color={0,0,
                  0}),
            Line(points={{40,0},{42,-6},{44,-12},{46,-16},{48,-20},{50,-22}}, color=
                 {0,0,0}),
            Line(points={{60,0},{58,-6},{56,-12},{54,-16},{52,-20},{50,-22}}, color=
                 {0,0,0}),
            Line(points={{60,0},{62,6},{64,12},{66,16},{68,20},{70,22}}, color={0,0,
                  0}),
            Line(points={{80,0},{78,6},{76,12},{74,16},{72,20},{70,22}}, color={0,0,
                  0}),
            Text(
              extent={{-146,142},{138,90}},
              lineColor={0,0,255},
              textString="%name")}),                                 Diagram(
            coordinateSystem(preserveAspectRatio=false)),
        Documentation(info="<html>
<p>This transmission model links two communication base stations together. It evaluates the tranmission quality from one station to another using the packet loss rate. In this model, we use the following relationship,</p>
<p align=\"center\"><i>r=k*sqrt(Q-C)</i></p>
<p>where<i> r</i> is the packet loss rate,<i> k</i> is the coefficient,<i> Q </i>is the packet volume, <i>C</i> is the threshold which means under this threshold, no packet loss happens.</p>
</html>"));
    end Transmission;
    annotation (Documentation(info="<html>
<p>This package contains base classes that are used to construct the models in EnergyAndCommunication.</p>
</html>"));
  end BaseClasses;
  annotation (Documentation(info="<html>
<p>This package contains models for coupled systems of energy and communication. </p>
</html>"));
end EnergyAndCommunication;
