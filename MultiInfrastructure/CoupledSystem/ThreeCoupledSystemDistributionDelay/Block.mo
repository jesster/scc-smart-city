within MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay;
model Block "Model that connects the energy,transportation and communication system"
  parameter Real numEV(fixed=true, start=numEV)
    "Number of charging EV in the block"
    annotation(Dialog(tab="Transportation",group="Parameters"));
  parameter Integer num=num "Number of ports for transportation and communication system";
  parameter Integer n=12 "Number of buildings in the community";
  parameter Modelica.SIunits.Voltage V_nominal=V_nominal
    "Nominal voltage(V_nominal>=0)"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Frequency f=f "Nominal grid frequency"
    annotation(Dialog(tab="Energy",group="Parameters"));
  parameter Modelica.SIunits.Angle lat=lat "Latitude"
    annotation(Dialog(tab="Energy",group="Parameters"));
  BaseClasses.EnergyDistribution ene(
    V_nominal=V_nominal,
    f=f,
    lat=lat,
    redeclare
      MultiInfrastructure.Buildings.Electrical.Transmission.MediumVoltageCables.Annealed_Al_500
      commercialCable,
    A=600000,
    num=num) annotation (Placement(transformation(extent={{34,36},{54,56}})));
  MultiInfrastructure.Buildings.Electrical.AC.ThreePhasesBalanced.Interfaces.Terminal_p term_p
    annotation (Placement(transformation(extent={{-120,52},{-100,72}}),
        iconTransformation(extent={{-120,52},{-100,72}})));
  MultiInfrastructure.Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-120,76},{-100,96}}), iconTransformation(extent=
           {{-120,76},{-100,96}})));
  Modelica.Blocks.Interfaces.RealInput numRecPac[num]
    "Number of received packets"
    annotation (Placement(transformation(extent={{-140,-40},{-100,0}})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.nPortBlock
    com(num=num)
    annotation (Placement(transformation(extent={{-26,-30},{-6,-10}})));
  Modelica.Blocks.Interfaces.RealOutput numSenPac[num]
    "Number of sent packets"
    annotation (Placement(transformation(extent={{100,-30},{120,-10}})));

  Modelica.Blocks.Interfaces.RealInput qOutSet[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-140,-64},{-100,-24}})));
  Modelica.Blocks.Interfaces.RealInput qIn[num] "Connector of Real input signal"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=90,
        origin={-60,-120})));
  Modelica.Blocks.Interfaces.RealInput proEV
    "Probability of single EV to be charged"
    annotation (Placement(transformation(extent={{-140,-114},{-100,-74}})));
  MultiInfrastructure.CoupledSystem.ThreeCoupledSystemDistributionDelay.BaseClasses.TransportationBlock
    tra(num=num, numEV(start=numEV, fixed=true))
    annotation (Placement(transformation(extent={{20,-66},{40,-46}})));
  Modelica.Blocks.Interfaces.RealOutput qOut [num] "Connector of Real output signal"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={60,-120}),
        iconTransformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={60,-120})));
  Buildings.Controls.OBC.CDL.Interfaces.RealInput PBui [n](
    quantity="Power",
    unit="W",
    max=0)
    "Building power load(negative means consumption, positive means generation)"
    annotation (Placement(transformation(extent={{-140,0},{-100,40}}),
        iconTransformation(extent={{-140,0},{-100,40}})));
equation
  connect(term_p,term_p)  annotation (Line(points={{-110,62},{-110,62},{-110,62},
          {-110,62}}, color={0,120,120}));
  connect(ene.weaBus,weaBus)  annotation (Line(
      points={{33,55.8},{0,55.8},{0,86},{-110,86}},
      color={255,204,51},
      thickness=0.5));
  connect(ene.term_p,term_p)  annotation (Line(points={{33,49},{-20,49},{-20,62},
          {-110,62}}, color={0,120,120},
      thickness=0.5));
  connect(com.numSenPac,numSenPac)
    annotation (Line(points={{-5,-20},{110,-20}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(numRecPac,com. numRecPac) annotation (Line(points={{-120,-20},{-27,-20}},
                           color={0,0,127},
      pattern=LinePattern.Dot));
  connect(com.numSenPac, ene.numSenPac) annotation (Line(points={{-5,-20},{10,
          -20},{10,37.4},{32,37.4}},
                                color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qOutSet,tra. qOutSet) annotation (Line(points={{-120,-44},{0,-44},{0,-51},
          {18,-51}},      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(qIn,tra. qIn) annotation (Line(points={{-60,-120},{-60,-56},{18,-56}},
                 color={0,0,127},
      pattern=LinePattern.Dot));
  connect(proEV,tra. proEV) annotation (Line(points={{-120,-94},{12,-94},{12,-64},
          {18,-64}}, color={0,0,127},
      pattern=LinePattern.Dot));
  connect(tra.qOut,qOut)  annotation (Line(points={{41,-56},{60,-56},{60,-120}},
                     color={0,0,127},
      pattern=LinePattern.Dot));
  connect(tra.numEVCha, ene.numEV) annotation (Line(points={{41,-64},{50,-64},{
          50,0},{6,0},{6,41.4},{32,41.4}},
                                      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[1], PBui[1]) annotation (Line(
      points={{32,43.5667},{0,43.5667},{0,1.66667},{-120,1.66667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[2], PBui[2]) annotation (Line(
      points={{32,43.9},{0,43.9},{0,5},{-120,5}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[3], PBui[3]) annotation (Line(
      points={{32,44.2333},{0,44.2333},{0,8.33333},{-120,8.33333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[4], PBui[4]) annotation (Line(
      points={{32,44.5667},{0,44.5667},{0,11.6667},{-120,11.6667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[5], PBui[5]) annotation (Line(
      points={{32,44.9},{0,44.9},{0,15},{-120,15}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[6], PBui[6]) annotation (Line(
      points={{32,45.2333},{0,45.2333},{0,18.3333},{-120,18.3333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[7], PBui[7]) annotation (Line(
      points={{32,45.5667},{0,45.5667},{0,21.6667},{-120,21.6667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[8], PBui[8]) annotation (Line(
      points={{32,45.9},{0,45.9},{0,25},{-120,25}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[9], PBui[9]) annotation (Line(
      points={{32,46.2333},{0,46.2333},{0,28.3333},{-120,28.3333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[10], PBui[10]) annotation (Line(
      points={{32,46.5667},{0,46.5667},{0,31.6667},{-120,31.6667}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(ene.PBui[11], PBui[11]) annotation (Line(
      points={{32,46.9},{0,46.9},{0,35},{-120,35}},
      color={0,0,127},
      pattern=LinePattern.Dot));
  connect(PBui[12], ene.PBui[12]) annotation (Line(
      points={{-120,38.3333},{0,38.3333},{0,47.2333},{32,47.2333}},
      color={0,0,127},
      pattern=LinePattern.Dot));
   annotation(Dialog(tab="Energy",group="Parameters"),
            Dialog(tab="Energy",group="Parameters"),
              Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Line(points={{-96,-48},{-94,-54},{-92,-60},{-90,-64},{-88,-68},{-86,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-76,-48},{-78,-54},{-80,-60},{-82,-64},{-84,-68},{-86,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-76,-48},{-74,-42},{-72,-36},{-70,-32},{-68,-28},{-66,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-56,-48},{-58,-42},{-60,-36},{-62,-32},{-64,-28},{-66,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-56,-48},{-54,-54},{-52,-60},{-50,-64},{-48,-68},{-46,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-36,-48},{-38,-54},{-40,-60},{-42,-64},{-44,-68},{-46,
              -70}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-36,-48},{-34,-42},{-32,-36},{-30,-32},{-28,-28},{-26,
              -26}},
            color={28,108,200},
          pattern=LinePattern.Dash),
        Line(points={{-16,-48},{-18,-42},{-20,-36},{-22,-32},{-24,-28},{-26,
              -26}},                                                 color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{-16,-48},{-14,-54},{-12,-60},{-10,-64},{-8,-68},{-6,-70}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{4,-48},{2,-54},{0,-60},{-2,-64},{-4,-68},{-6,-70}}, color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{4,-48},{6,-42},{8,-36},{10,-32},{12,-28},{14,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Line(points={{24,-48},{22,-42},{20,-36},{18,-32},{16,-28},{14,-26}},
                                                                     color={28,
              108,200},
          pattern=LinePattern.Dash),
        Text(
          extent={{-150,158},{134,106}},
          lineColor={0,0,255},
          textString="%name"),
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={0,0,0}),
        Rectangle(
          extent={{-78,20},{72,-24}},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={57,20},
          rotation=90),
        Rectangle(
          extent={{12,11},{-12,-11}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          radius=10,
          origin={57,56},
          rotation=90),
        Line(points={{-62,62},{-36,62}},   color={0,0,0}),
        Line(points={{-50,62},{-50,32}},    color={0,0,0}),
        Line(points={{-62,62},{-62,60}},   color={0,0,0}),
        Line(points={{-36,62},{-36,60}},   color={0,0,0}),
        Line(
          points={{-72,32},{-70,34},{-66,38},{-62,52},{-62,60}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-72,32},{-60,34},{-54,36},{-38,48},{-36,60}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{-26,72},{0,72}},     color={0,0,0}),
        Line(points={{-26,72},{-26,70}},   color={0,0,0}),
        Line(points={{0,72},{0,70}},       color={0,0,0}),
        Line(
          points={{-62,60},{-46,64},{-36,66},{-30,68},{-26,70}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-36,60},{-24,62},{-14,64},{-6,66},{0,70}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{-26,70},{-10,74},{0,76},{6,78},{10,80}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(
          points={{0,70},{12,72},{22,74},{30,76},{36,80}},
          color={175,175,175},
          smooth=Smooth.Bezier),
        Line(points={{10,82},{36,82}},   color={0,0,0}),
        Line(points={{-14,72},{-14,42}},   color={0,0,0}),
        Line(points={{22,82},{22,50}},   color={0,0,0}),
        Line(points={{10,82},{10,80}},     color={0,0,0}),
        Line(points={{36,82},{36,80}},     color={0,0,0}),
        Rectangle(
          extent={{46,60},{68,24}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,60},{62,50}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,42},{62,32}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,26},{62,16}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid), Line(
          points={{-78,-2},{72,-2}},
          color={255,255,255},
          pattern=LinePattern.Dash,
          thickness=0.5),
        Ellipse(
          extent={{19.5,0.5},{91.5,-71.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{22.5,-2.5},{88.5,-68.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{34.5,-14.5},{76.5,-56.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{37.5,-17.5},{73.5,-53.5}},
          lineColor={170,213,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{43.5,-23.5},{67.5,-47.5}},
          lineColor={170,213,255},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{54,-34},{30,-96},{32,-96},{54,-38},{54,-34}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{48,-50},{62,-52}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{50.5,-30.5},{60,-40}},
          pattern=LinePattern.None,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{56,-34},{80,-96},{78,-96},{56,-38},{56,-34}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{44,-62},{66,-64}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model shows the three agents (energy, transportation and communication) in the block.</p>
</html>"));
end Block;
